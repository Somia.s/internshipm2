#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import itertools
import matplotlib.pyplot as plt


class Family:

    def __init__(self, phylum_name, family_name):
      '''
      A family is defined by:
      - a phylum name
      - a family name
      '''

      # Retrieve the phylum name and the family name of the current family
      self.phylum_name = phylum_name
      self.family_name = family_name

      # Dataframe to contain data
      self.df_allPdist = pd.DataFrame() # P-distances
      self.df_criterion0_CS_piWithin = pd.DataFrame() # Species based on criterion 0
      self.df_SeqPerBINs = pd.DataFrame() # Sequence numbers per BINs
      self.df_output = pd.DataFrame()



    def filePathways(self):
      '''
         Pathway of files
      '''

      # Input: P-distances
      self.input_filenameFamilies = "../../Output/intraFamily/{}/{}/pdistances.csv".format(self.phylum_name, self.family_name)
      
      # Outputs: a CSV file and histograms
      self.output_filename = "../../Output/intraFamily/{}/{}/compare_pi_within_interBINs/compare_pi_within_interBINs.csv".format(self.phylum_name, self.family_name)
      self.output_filenameHist = "../../Output/intraFamily/{}/{}/compare_pi_within_interBINs/".format(self.phylum_name, self.family_name)



    def createDirectory(self, nameDirectory):
      '''
          Create directory if it hasn't been created yet
      '''
      if not os.path.exists(nameDirectory):
          os.mkdir(nameDirectory)



    def retrieveAllPdist(self):
      '''
        Load p-distance values
      '''

      # Read CSV file
      self.df_allPdist = pd.read_csv(self.input_filenameFamilies, sep = "\t")

      # Convert column with sequence IDs into string
      self.df_allPdist['sequenceID1'] = self.df_allPdist['sequenceID1'].astype(str)
      self.df_allPdist['sequenceID2'] = self.df_allPdist['sequenceID2'].astype(str)



    def selectPdistIntraBINs(self):
      '''
        Select the p-distances only within a BIN
        By default, the BIN with only 1 sequence are removed
      '''

      self.df_criterion0_CS_piWithin = self.df_allPdist[self.df_allPdist.bin_uri1 == self.df_allPdist.bin_uri2]



    def selectCandidateCScriterion0(self):
      '''
        Select only criterion 0 candidate CS 
      '''

      # Select "species name 1" and "species name 2" columns with the same species name: select intra-species p-distances
      self.df_criterion0_CS_piWithin = self.df_criterion0_CS_piWithin[self.df_criterion0_CS_piWithin.species_name1 == self.df_criterion0_CS_piWithin.species_name2]

      # Select the nominal species with at least 2 different BINs <=> the nominal species is a criterion 0 candidates CS
      df_criterion0_CS = self.df_allPdist[self.df_allPdist.bin_uri1 != self.df_allPdist.bin_uri2]
      # The name of the criterion 0 CSs
      species_names_criterion0CS = df_criterion0_CS.species_name1

      # Select the criterion 0 candidates CS
      self.df_criterion0_CS_piWithin = self.df_criterion0_CS_piWithin[self.df_criterion0_CS_piWithin.species_name1.isin(species_names_criterion0CS)]



    def sequencesPerBINs(self):
      '''
        For each BIN of a criterion 0 candidate CS, count the number of sequences
      '''

      # BIN 1
      df_SeqBINs1 = self.df_criterion0_CS_piWithin[['species_name1', 'bin_uri1', 'sequenceID1']].drop_duplicates()
      df_SeqBINs1.columns = ['species_name', 'bin_uri', 'sequenceID']
      # BIN 2
      df_SeqBINs2 = self.df_criterion0_CS_piWithin[['species_name1', 'bin_uri2', 'sequenceID2']].drop_duplicates()
      df_SeqBINs2.columns = ['species_name', 'bin_uri', 'sequenceID']

      # Merge the two dataframes, drop duplicated IDs
      df_SeqBINs = pd.concat([df_SeqBINs1, df_SeqBINs2]).drop_duplicates().reset_index(drop=True)

      # Count the number of sequences per BIN
      self.df_SeqPerBINs = df_SeqBINs.groupby(by=['species_name', 'bin_uri']).agg([('nb_sequences', 'size'), ('sequencesID', lambda x: ','.join(sorted(x)))]).reset_index()
      self.df_SeqPerBINs.columns = ['species_name', 'bin_uri', 'nb_sequences', 'sequencesID']

      # Remove the criterion 0 candidate CS with only one BIN after removing its other BIN(s) with only 1 sequence
      mask = self.df_SeqPerBINs.species_name.duplicated(keep=False)
      self.df_SeqPerBINs = self.df_SeqPerBINs[mask]



    def plotHist(self, nullDist, punctual_value, id_BIN1, id_BIN2, nb_seq_BIN1, nb_seq_BIN2, species_name, pvalue):
      '''
        Plot a histogram for each pair comparison: the null distribution and the punctual value to compare
        BIN 1: BIN with most sequence
        BIN 2: BIN with fewest sequence
      '''

      sns.set_theme()
      x = nullDist
      fig = sns.displot(x, rug=True, label="{}: {} sequences".format(id_BIN1, nb_seq_BIN1))
      fig.fig.set_size_inches(30,15)
      plt.title('Histogram: comparison of the pi-within between {} and {} of {}. P-value = {}'.format(id_BIN1, id_BIN2, species_name, pvalue), fontsize=20)
      plt.xlabel('Pi-within of the BIN with the most sequences and after a sampling of the size of the BIN with the fewest sequences', fontsize=19)
      plt.ylabel('Frequency', fontsize=19)

      plt.axvline(x=punctual_value, color='r', linewidth=5, label="{}: {} sequences".format(id_BIN2, nb_seq_BIN2)) # Add a vertical line
      plt.legend(prop={'size': 20}) # Add a legend
      plt.savefig(str(self.output_filenameHist + id_BIN1 + "_" + id_BIN2 + ".png")) # Save the figure
      


    def compare_pi_within_interBINs(self):
      '''
        Within a family: for all species, compare its all BINS pairwise.
      '''

      self.filePathways()
      self.retrieveAllPdist()
      self.selectPdistIntraBINs()
      self.selectCandidateCScriterion0()
      self.sequencesPerBINs()
      self.createDirectory("../../Output/intraFamily/{}/{}/compare_pi_within_interBINs".format(self.phylum_name, self.family_name))


      # Structure to contain lines
      rows = []

      # Number of simulations
      nb_simulations = 1000

      # All criterion 0 nominal species within family
      species_names = self.df_SeqPerBINs.species_name.unique()
      
      # For each species within the current family
      for species_name in species_names:

        # Dataframe of the current species
        df_currentSpecies = self.df_SeqPerBINs[self.df_SeqPerBINs.species_name == species_name]
        df_currentSpeciesPI = self.df_criterion0_CS_piWithin[self.df_criterion0_CS_piWithin.species_name1 == species_name]
        
        # Retrieve the BINs of the current species
        bins_uris = df_currentSpecies.bin_uri

        # All possible combinaisons between the pairwise BINs
        pairwiseCombinaisons = list(itertools.combinations(bins_uris, 2))

        # Compare the diversity by pair of BINs
        for pair in pairwiseCombinaisons:
          
          # BIN 1
          bin_uri1 = pair[0]
          nb_seq_BIN1 = df_currentSpecies[df_currentSpecies.bin_uri == bin_uri1].nb_sequences.iloc[0]
          
          # BIN 2
          bin_uri2 = pair[1]
          nb_seq_BIN2 = df_currentSpecies[df_currentSpecies.bin_uri == bin_uri2].nb_sequences.iloc[0]

          # A null distribution is based on data
          if nb_seq_BIN1 >= nb_seq_BIN2: # BIN 1 is used for the null distribution
            ""
          else:
            # BIN 1 becomes BIN 2
            bin_uri1 = pair[1]
            nb_seq_BIN1_tmp = nb_seq_BIN1
            nb_seq_BIN1 = nb_seq_BIN2
            
            # BIN 2 becomes BIN 1
            bin_uri2 = pair[0]
            nb_seq_BIN2 = nb_seq_BIN1_tmp

          # Consider the comparison only if the smallest BIN contains at least 5 sequences
          #if nb_seq_BIN2 >= 5:

          # Calculate one pi-within mean for the BIN which has the fewest sequences
          piBIN2 = df_currentSpeciesPI[df_currentSpeciesPI.bin_uri2 == bin_uri2].pdist.mean()

          # Pre-calculate pi within BIN 1
          piBIN1 = df_currentSpeciesPI[df_currentSpeciesPI.bin_uri1 == bin_uri1].pdist.mean()

          list_piBIN1 = []
          nb_times_inf = 0
          nb_times_sup = 0

          # Calculate different pi-within for the BIN which has the most sequences
          for s in range(nb_simulations):

            piBIN1 = df_currentSpeciesPI[df_currentSpeciesPI.bin_uri1 == bin_uri1].sample(n=nb_seq_BIN2, replace=True).pdist.mean()

            list_piBIN1.append(piBIN1)

            if piBIN1 > piBIN2: # The null distribution is greater than the punctual value
              nb_times_sup+=1

            elif piBIN1 < piBIN2: # The null distribution is smaller than the punctual value
              nb_times_inf+=1

          proportion_nullDist_sup = nb_times_sup/nb_simulations
          proportion_nullDist_inf = nb_times_inf/nb_simulations

          # Null distribution is greater or smaller than the punctual value ?
          nullDistribution = None

          if proportion_nullDist_sup > proportion_nullDist_inf:
            nullDistribution = ">"
          elif proportion_nullDist_inf > proportion_nullDist_sup:
            nullDistribution = "<"
          elif proportion_nullDist_inf == proportion_nullDist_sup:
            nullDistribution = "="

          pvalue = min(proportion_nullDist_inf, proportion_nullDist_sup)

          # Plot result for 2 BINS comparison
          #self.plotHist(list_piBIN1, piBIN2, bin_uri1, bin_uri2, nb_seq_BIN1, nb_seq_BIN2, species_name, pvalue)
          
          # Write appended results
          for piBIN1_resample in list_piBIN1:
            rows.append([self.phylum_name, self.family_name, species_name, bin_uri2, nb_seq_BIN2, piBIN2, bin_uri1, nb_seq_BIN1, piBIN1, piBIN1_resample, nb_times_inf, nb_times_sup, nullDistribution, pvalue])

      # Convert to panda format
      all_rows = [x for x in rows if x != []]
      header = ["phylum_name", "family_name", "species_name", "bin_uri1", "nb_seq_bin_uri1", "piwithin_BIN1", "bin_uri2", "nb_seq_bin_uri2", "piwithin_BIN2", "piwithin_BIN2_resample", "nb_times_inf", "nb_times_sup", "nullDistribution", "pvalue"]
      df_output = pd.DataFrame(all_rows, columns=header)

      # Export the dataframe into CSV file
      df_output.to_csv(self.output_filename, sep='\t', index=False)