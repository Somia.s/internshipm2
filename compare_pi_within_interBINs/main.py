#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../')
from commonFunctions import *

from compare_pi_within_interBINs import *
from compare_pi_within_interBINs_allFamilies import *
from criterion3 import *

import argparse


program_description = ''' ************ PROGRAM DESCRIPTION ************
    Compare polymorphism between BINs.
    The program can be performed for a specific phylum, a specific family, or for all data.
   '''


def create_parser(): 
    '''
        Retrieve arguments before start the program
    '''

    parser = argparse.ArgumentParser(add_help=True,
                                     description=program_description)

    ## The phylum we want to study
    # Via a string
    parser.add_argument('-Phylum','--PhylumName',
                        help="Phylum name",
                        type=str,
                        default=None,
                        required=False) # Not necessary to indicate phylum name

    # Via a file
    parser.add_argument('-PhylumFile','--PhylumFileName',
                        help="Phylum filename containing the names",
                        type=argparse.FileType('r'),
                        default=None,
                        required=False) # Not necessary to indicate the phylum name

    ## The family we want to study
    # Via a string
    parser.add_argument('-Family','--FamilyName',
                        help="Name of the family",
                        type=str,
                        default=None,
                        required=False) # Not necessary to indicate the phylum name

    # Via a file
    parser.add_argument('-FamilyFile','--FamilyFileName',
                        help="Family filename containing the names",
                        type=argparse.FileType('r'),
                        default=None,
                        required=False) # Not necessary to indicate the phylum name
         
    return parser


def parse_arguments(): # Return a dictionary of arguments
    # Example: {"Phylum": "Mollusca"}

    parser = create_parser() # Parser creation
    args = parser.parse_args() # Parse user's arguments
    dict_args = dict(args.__dict__)

    return dict_args


def main(PhylumName, PhylumFileName, FamilyName, FamilyFileName):

    print("----- START COMPARE PI WITHIN INTERBINS -----\n")

    # Create directories for the results
    createDirectory("../../Output")
    createDirectory("../../Output/compare_pi_within_interBINs")
    createDirectory("../../Log")

    # Create the log file
    f = open("../../Log/compare_pi_within_interBINs.log", "w")
    f.close()

    # Create (or clean) the output file
    #f = open("../../Output/compare_pi_within_interBINs/compare_pi_within_interBINs.csv", "w")
    #header = ["phylum_name", "family_name", ""species_name", "bin_uri1", "nb_seq_bin_uri1", "bin_uri2", "nb_seq_bin_uri2", "nullDistribution", "pvalue"]
    #f.write('\t'.join(header)+'\n')
    #f.close()

    # Retrieve available phylum names
    available_phylum = retrieveDirectories("../../Output/intraFamily")

    # Retrieve phylum name(s)
    if PhylumName == None and PhylumFileName == None:
        # 1. Retrieve all phylum inside the sequences folder
        list_phylum = available_phylum

    elif PhylumFileName == None:
        # 2. Retrieve the phylum name by a string
        list_phylum = [PhylumName]

    elif PhylumName == None:
        # 3. Retrieve the phylum name by a file
        list_phylum = []
        for phylum_name in PhylumFileName.readlines():
            list_phylum.append(phylum_name.strip())

    # Read the file of family names
    if FamilyFileName != None:
        families_from_FILE = [] # inizialisation
        for family_name in FamilyFileName.readlines():
            families_from_FILE.append(family_name.strip())

    #----------------------#
    #    For each family   #
    #----------------------#    
    for phylum_name in list_phylum:

        if phylum_name not in available_phylum:
            
            print("\nThe phylum name {} is not available.".format(phylum_name))

        else:
            
            print("\nThe phylum name {} is available.".format(phylum_name))

            #----------------------#
            #    For each family   #
            #----------------------#

            # Retrieve avalaible family names for this current phylum
            available_families_tmp = retrieveFilenames("../../Data/sequences/corrected_and_fitted_Sequences/{}".format(phylum_name))
            available_families = []
            for available_family in available_families_tmp:
                available_families.append(available_family.replace(".fas", ""))

            # Retrieve family name(s)
            if FamilyName == None and FamilyFileName == None:
                # 1. Retrieve all families inside the sequence folder
                list_families = available_families

            elif FamilyFileName == None:
                # 2. Retrieve family name by a string
                list_families = [FamilyName]

            elif FamilyName == None:
                # 3. Retrieve family name by a file
                list_families = families_from_FILE


            # Retrieve all families inside the sequences files
            for family_name in list_families:

                if family_name not in available_families:
                    
                    print("The family name {} is unavailable.".format(family_name))

                else:
                    
                    # Retrieve p-distances from a family
                    pdist_filename = "../../Output/intraFamily/{}/{}/pdistances.csv".format(phylum_name, family_name)
                    
                    # Get p-distances file size
                    filesizePdist= None
                    if os.path.exists(pdist_filename):
                        filesizePdist = os.path.getsize(pdist_filename)

                    # Ensure that pdistances have been calculated
                    if os.path.exists(pdist_filename) and filesizePdist != 0:

                        if os.path.exists("../../Output/intraFamily/{}/{}/compare_pi_within_interBINs/compare_pi_within_interBINs.csv".format(phylum_name, family_name)):
                            
                            print("\nPHYLUM: ", phylum_name, " FAMILY:", family_name)
                            print("Compare pi-within interBINs.")
                            family = Family(phylum_name, family_name)
                            family.compare_pi_within_interBINs()
                        
    # Consider all families
    all_families = Families()
    all_families.getResults()
    all_families.plotResults()

    # Criterion 3
    CS_Results = CSResults()
    CS_Results.checkCriterion3()
    CS_Results.proportionCriterion3("criterion1")
    CS_Results.proportionCriterion3("criterion2")


    print("\n----- END COMPARE PI WITHIN INTERBINS  ------\n")


if __name__ == '__main__':
    arguments = parse_arguments()
    main(**arguments)