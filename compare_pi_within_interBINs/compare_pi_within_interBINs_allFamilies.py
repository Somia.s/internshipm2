#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import matplotlib.pyplot as plt
import os
import seaborn as sns

class Families:

    def __init__(self):
      '''
        The results of comparisons between BINs, for all families.
      '''

      # Inputs
      self.intraFamily_directory = "../../Output/intraFamily/"

      # Ouputs
      self.output_filename = "../../Output/compare_pi_within_interBINs/compare_pi_within_interBINs.csv"
      self.output_plot = "../../Output/compare_pi_within_interBINs/compare_pi_within_interBINs.png"
      self.output_pieChart = "../../Output/compare_pi_within_interBINs/proportionsPvalue.png"
      self.output_pieChartNullDistSupOfInf = "../../Output/compare_pi_within_interBINs/proportionsNullDistSupOrInf.png"


    def retrieveDirectories(self, directory):
      '''
          Retrieve all files in a directory
          @param directory: Directory with the files
          @return: List of filenames, directory are not considered
      '''

      list_filenames = [f for f in os.listdir(directory) if not os.path.isfile(os.path.join(directory, f))]

      return list_filenames


    def getResults(self):
      '''
        Retrieve the results of comparisons, for all families.
      '''

      # Retrieve the phylum
      list_phylum = self.retrieveDirectories(self.intraFamily_directory)

      for phylum_name in list_phylum:

        # Retrieve the family
        list_families = self.retrieveDirectories("../../Output/intraFamily/{}".format(phylum_name))

        for family_name in list_families:

          filename_oneFamily = "../../Output/intraFamily/{}/{}/compare_pi_within_interBINs/compare_pi_within_interBINs.csv".format(phylum_name, family_name)

          # If comparisons have been performed
          if os.path.exists(filename_oneFamily):

            # Import the dataframe
            df_result_oneFamily = pd.read_csv(filename_oneFamily, sep="\t")

            # Group by comparisons of BINs pairwise
            if not df_result_oneFamily.empty:

              df_result_oneFamily = df_result_oneFamily.groupby(by=["phylum_name", "family_name", "species_name", "bin_uri1", "nb_seq_bin_uri1", "bin_uri2", "nb_seq_bin_uri2", "nullDistribution"]).pvalue.mean().reset_index()

              # Save all families into a unique CSV file
              df_result_oneFamily.to_csv(self.output_filename, sep = "\t", index=False, mode='a', header=False)


    def plotPvalueAccordingToNbSeq(self, df_result):
      '''
        Plot p-value results, according to the number of sequences in the smallest BIN and in the largest BIN
      '''
      
      sns.set(rc={'figure.figsize':(50,50)}) # Figure size
      g = sns.relplot(
          data=df_result,
          x="nb_seq_bin_uri1", y="nb_seq_bin_uri2",
          col="Result"
      )

      g.set(xlabel='Number of sequences in the smallest BIN', 
            ylabel='Number of sequences in the largest BIN')
      #plt.title("Test the equality of the genetic diversity between two BINs of a nominal species")
      #plt.show()
      g.savefig(self.output_plot)


    def plotProportionsPvalue(self, df_result):
      '''
        Plot a pie chart: with the proportion of significant p-value and not significant p-value
      '''

      fig = plt.figure()
      df_resultProp = df_result.groupby(['Result']).count().reset_index()
      colors = ["skyblue", "lightskyblue", "salmon", "tomato"] # Choose colors
      plt.pie(df_resultProp['pvalue'], labels=df_resultProp['Result'], colors=colors, autopct='%1.1f%%')
      plt.title('Comparison pairwise of pi-within between BINs of nominal species: proportions of BINs per pair with equal or unequal intra-BIN diversities')
      plt.show()
      fig.savefig(self.output_pieChart, dpi=fig.dpi)


    def plotProportionsNullDistSup(self, df_result):
      '''
        Plot a pie chart with the proportions:
        1. Null distribution is greater than the punctual value
        2. Null distribution is smaller than the punctual value
        i.e.the proportion of times the larger BIN has a lower or higher polymorphism than the smallest BIN
      '''

      fig = plt.figure()
      df_resultProp = df_result.groupby(['nullDistribution']).count().reset_index()
      colors = ["skyblue", "white", "tomato"] # Choose colors
      plt.pie(df_resultProp['pvalue'], labels=df_resultProp['nullDistribution'], colors=colors, autopct='%1.1f%%')
      plt.title('Comparison of pi-within between BINs: proportion of times the largest BIN has a lower or higher polymorphism than the smallest BIN')
      plt.show()
      fig.savefig(self.output_pieChartNullDistSupOfInf, dpi=fig.dpi)

  
    def plotResults(self):
      '''
        Plot p-value results
      '''

      # Load data
      df_result = pd.read_csv(self.output_filename, sep="\t")

      # Add column "pvalue2"
      df_result.loc[df_result['pvalue'] == 0.0, 'Result'] = "Inequality (p = 0)"
      df_result.loc[(df_result['pvalue'] > 0.0) & (df_result['pvalue'] <= 0.025), 'Result'] = "Inequality (p <= 0.025)"
      df_result.loc[(df_result['pvalue'] > 0.025) & (df_result['pvalue'] <= 0.05), 'Result'] = "Equality (0.025 < p <= 0.05)"
      df_result.loc[df_result['pvalue'] > 0.05, 'Result'] = "Equality (p > 0.05)"

      df_result = df_result.sort_values(by=['pvalue'])

      #self.plotPvalueAccordingToNbSeq(df_result)
      self.plotProportionsPvalue(df_result)
      self.plotProportionsNullDistSup(df_result)