#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import os

class CSResults:

    def __init__(self):
        '''
            The CS results and polymorphism comparisons between each pair BINs inside a criterion 0 nominal species
        '''

        # Input
        self.filename_compareDivInterBINs = "../../Output/criterion3/criterion3.csv"
        self.filename_CSresults = "../../Output/detectCandidateCS/detectCandidateCS_withoutBiblio.csv"

        # Ouput
        self.filename_output = "../../Output/criterion3/CSresults_criterion3.csv"
    
        # Structure to contain data
        self.df_compareDivInterBINs = None
        self.df_CSresults = None
        self.df_output = None

        # Load data
        self.loadData()


    def loadData(self):
        '''
            Load data into pandas dataframes then preprocessing data
        '''

        # Load comparisons of pi-within between BINs
        self.df_compareDivInterBINs = pd.read_csv(self.filename_compareDivInterBINs, sep="\t")
        # Label equal or inequal pi-within between BINs
        self.df_compareDivInterBINs.loc[self.df_compareDivInterBINs['pvalue'] <= 0.025, 'criterion3'] = "Inequality"
        self.df_compareDivInterBINs.loc[self.df_compareDivInterBINs['pvalue'] > 0.025, 'criterion3'] = "Equality"

        # Load CS results from criteria 1 and 2
        self.df_CSresults = pd.read_csv(self.filename_CSresults, sep="\t")
        # Keep only species which have checked criterion 0
        self.df_CSresults = self.df_CSresults[self.df_CSresults["test0"] == True]
        # Keep only at family level
        self.df_CSresults = self.df_CSresults[self.df_CSresults["genusLevel"] == False]
        #self.df_CSresults = self.df_CSresults[self.df_CSresults["number_of_sequences"] >= 5]
        self.df_CSresults = self.df_CSresults[["phylum_name", "family_name", "species_name", "test0", "test1", "test2"]]


    def retrieveBINshaveCheckedC1orC2(self):
        '''
            For species which have > 2 BINs, retrieve pair(s) BINs which have validated criterion 1 (or 2)
        '''

        ''


    def checkCriterion3(self):
        '''
            Merge CS results and polymorphism comparison between pairwise BINs (merge according to phylum name and species name)
            Check criterion 3
        '''

        print(self.df_compareDivInterBINs)
        print(self.df_compareDivInterBINs[self.df_compareDivInterBINs.species_name == "Evadne nordmanni"])
        print(len(self.df_compareDivInterBINs.species_name.unique()))
        print(self.df_CSresults)

        # Merge CS results with criterion 3
        df_merged = pd.merge(self.df_compareDivInterBINs, self.df_CSresults, how="left", on=["phylum_name", "family_name", "species_name"])

        # Rows to contain data
        rows = []

        # Analyse each species
        species_names = df_merged.species_name.unique()

        for species_name in species_names[0:1]:
                
                df_current_species = df_merged[df_merged.species_name == "Evadne nordmanni"].reset_index()

                if len(df_current_species) == 1:
                    
                    row = df_current_species.iloc[0].values.tolist()[1:]
                    row.append(df_current_species.test1[0])
                    row.append(df_current_species.test2[0])
                    rows.append(row)

                else:

                    #---------------------------------------------------------#
                    #        For species which have more than 2 BINS          #
                    #---------------------------------------------------------#

                    # Retrieve phylum name and family name
                    phylum_name = df_current_species.phylum_name.iloc[0]
                    family_name = df_current_species.family_name.iloc[0]

                    # Get Pdistance comparisons file
                    filename_compPdist = "../../Output/intraFamily/{}/{}/comparisonsPdist.csv".format(phylum_name, family_name)

                    if os.path.exists(filename_compPdist):
                        
                        df_comparisonsPdist = pd.read_csv(filename_compPdist, sep="\t")

                        if not df_comparisonsPdist.empty:
                            
                            # Rename columns: to test1 from test1_old, same for test 2
                            df_current_species = df_current_species.rename(columns={'test1':'test1_old'})
                            df_current_species = df_current_species.rename(columns={'test2':'test2_old'})

                            for i in range(len(df_current_species)):
                                BIN1 = df_current_species.bin_uri1[i]
                                BIN2 = df_current_species.bin_uri2[i]
                                df_current_2BINs = df_comparisonsPdist[((df_comparisonsPdist["bin_uri1"] == BIN1) & (df_comparisonsPdist["bin_uri2"] == BIN2)) | (df_comparisonsPdist["bin_uri2"] == BIN1) & (df_comparisonsPdist["bin_uri1"] == BIN2)]

                                # Check at least a ratio > 1 for criterion1
                                count_ratioSup1 = df_current_2BINs[df_current_2BINs['ratio_test1'] > 1].count().iloc[0]

                                if count_ratioSup1 >= 1:
                                    df_current_species.loc[((df_current_species['bin_uri1'] == BIN1) & (df_current_species['bin_uri2'] == BIN2)), 'test1'] = True
                                else:
                                    df_current_species.loc[((df_current_species['bin_uri1'] == BIN1) & (df_current_species['bin_uri2'] == BIN2)), 'test1'] = False

                                # Check at least a ratio <1 for criterion2
                                count_ratioInf1 = 0
                                ratios_criterion2 = list(df_current_2BINs['ratio_test2'])

                                for ratio in ratios_criterion2:
                                    if isinstance(ratio, float): # If all values inside the column contain a float value
                                        if float(ratio) < 1:
                                            count_ratioInf1 +=1
                                    elif ratio.find("A") == -1 and ratio.find("B") == -1 and ratio.find("C") == -1 and ratio.find("D") == -1 and ratio.find("E") == -1 and ratio != None and ratio != "":
                                        if float(ratio) < 1:
                                            count_ratioInf1 +=1

                                if count_ratioInf1 >= 1:
                                    df_current_species.loc[((df_current_species['bin_uri1'] == BIN1) & (df_current_species['bin_uri2'] == BIN2)), 'test2'] = True
                                else:
                                    df_current_species.loc[((df_current_species['bin_uri1'] == BIN1) & (df_current_species['bin_uri2'] == BIN2)), 'test2'] = False

                            for row in df_current_species.values.tolist():
                                rows.append(row[1:])

                # Convert to panda format
                all_rows = [x for x in rows if x != []]
                self.df_output = pd.DataFrame(all_rows, columns=['phylum_name', 'family_name', 'species_name', 'bin_uri1', 'nb_seq_bin_uri1', 'bin_uri2', 'nb_seq_bin_uri2', 'nullDistribution', 'pvalue', 'criterion3', 'test0', 'test1_old', 'test2_old', 'test1', 'test2'])
    
                self.df_output.to_csv(self.filename_output, sep = "\t", index=False)


    def proportionCriterion3(self, criterion):
        '''
            Proportion of species which have successed criterion 1 (or 2) and which have their BINs with inequal diversity

            C3 means criterion 3

            parameter criterion: Criterion 3 is checked after criterion 1 or 2
        '''

        if criterion == "criterion1":
            test_old = "test1_old"
            test = "test1"
        elif criterion == "criterion2":
            test_old = "test2_old"
            test = "test2"

        # Load C3 results (Criterion 0 species and their BINs, pi-within comparisons between BINs have been performed)
        df_C3results = pd.read_csv(self.filename_output, sep="\t")
        print("********************* Species of criterion 0 and the pi-within comparisons between their BINs *********************\n")
        print(df_C3results)
        print("Total number of species: ", len((df_C3results.species_name.unique())))
        print("\n\n")

        ###################################################

        #---------------------------------------------------------#
        #       Species which have checked criterion 1 or 2       #
        #---------------------------------------------------------#

        df_testChecked = df_C3results[df_C3results[test_old] == True]
        print("********************* Species that have successed {} and the pi-within comparisons between their BINs *********************\n".format(criterion))
        print(df_testChecked)
        print("Total number of species that have successed {}: ".format(criterion), len((df_testChecked.species_name.unique())))
        print("\n\n")

        # Keep only BINs that have checked C1
        # Warning: Some species have their BINs with only 1 sequence and pi-within can't be performed. The BINs involved in criterion 1 check can be not appear.
        df_testChecked = df_testChecked[df_testChecked[test] != False]
        print("********************* Keep only BINs that have checked C1 *********************\n")
        print(df_testChecked)
        print("Total number of species that have successed {}: ".format(criterion), len((df_testChecked.species_name.unique())))

        # For each species, join all results from criterion 3
        df_testChecked = df_testChecked.groupby(by="species_name").agg({'criterion3': lambda x: str(set(x))}).reset_index()

        # For each species, determine result from criterion 3
        df_testChecked.loc[df_testChecked['criterion3'].str.contains('Equality'), 'criterion3Result'] = False
        df_testChecked.loc[df_testChecked['criterion3'].str.contains('Inequality'), 'criterion3Result'] = True
        df_testChecked.loc[df_testChecked['criterion3'].str.contains(','), 'criterion3Result'] = "Both"

        # Display results
        print("Inequal BINs:", len(df_testChecked[df_testChecked['criterion3Result'] == True]))
        print("Inequal and equal BINs:", len(df_testChecked[df_testChecked['criterion3Result'] == "Both"]))
        print("Equal BINs:", len(df_testChecked[df_testChecked['criterion3Result'] == False]))
        print("\n*****************************************************************************************************************************")
        print("*****************************************************************************************************************************\n\n\n")

        ###################################################

        #-------------------------------------------------------------#
        #       Species which have not checked criterion 1 or 2       #
        #-------------------------------------------------------------#

        df_testnotChecked = df_C3results[df_C3results[test_old] == False]
        print("********************* Species that have not successed {} and the pi-within comparisons between their BINs *********************\n".format(criterion))
        print(df_testnotChecked)
        print("Total number of species that have not successed {}: ".format(criterion), len((df_testnotChecked.species_name.unique())))
        print("\n\n")

        # For each species, join all results from criterion 3
        df_testnotChecked = df_testnotChecked.groupby(by="species_name").agg({'criterion3': lambda x: str(set(x))}).reset_index()
        print(df_testnotChecked)

        # For each species, determine result from criterion 3
        df_testnotChecked.loc[df_testnotChecked['criterion3'].str.contains('Equality'), 'criterion3Result'] = False
        df_testnotChecked.loc[df_testnotChecked['criterion3'].str.contains('Inequality'), 'criterion3Result'] = True
        df_testnotChecked.loc[df_testnotChecked['criterion3'].str.contains(','), 'criterion3Result'] = "Both"
        print(df_testnotChecked)

        # Display results
        print("Inequal BINs:", len(df_testnotChecked[df_testnotChecked['criterion3Result'] == True]))
        print("Inequal and equal BINs:", len(df_testnotChecked[df_testnotChecked['criterion3Result'] == "Both"]))
        print("Equal BINs:", len(df_testnotChecked[df_testnotChecked['criterion3Result'] == False]))
        print("\n*****************************************************************************************************************************")
        print("*****************************************************************************************************************************\n\n\n")

        ###################################################
