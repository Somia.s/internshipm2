#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from commonFunctions import *

import pandas as pd
import itertools # Retrieve the pairwise combinaisons
from Bio import SeqIO
from pysam import FastaFile
import os
import re

class Family_AllPdist():

    def __init__(self, phylum_name, family_name):
        '''
           A family is defined by:
           - A phylum name
           - A family name
           - An input: the multiple sequence alignment (MSA) performs thanks to MAFFT software
           - An output: the p-distances results

           Aim:
           Perform calculations of the p-distances inside a family of interest
        '''

        # Retrieve the phylum name and the family name of the current family
        self.phylum_name = phylum_name
        self.family_name = family_name
        self.genus_name = ""

        # Inputs
        self.input_filenameMSA = ""
        self.input_filename_sequencesInformations = ""

        # Output
        self.output_filenameAllPdist = ""


    def inputPathways(self):
        '''
            Pathway of the input
            Input from the MSA multiple alignment
        '''

        # Retrieve the multiple alignment result
        self.input_filenameMSA = "../../../Output/intraFamily/{}/{}/MSA.fas".format(self.phylum_name, self.family_name)

        # Retrieve other informations about those sequences
        self.input_filename_sequencesInformations = "../../../Output/descriptiveStatistics/sequences.csv"


    def outputPathways(self):
        '''
            Pathway of the output file
        '''

        # Save all p-distances inside a family
        self.output_filenamePerFamily = "../../../Output/intraFamily/{}/{}/pdistances.csv".format(self.phylum_name, self.family_name)

        # Save all p-distances inside a genus
        self.output_filenamePerGenus = "../../../Output/intraFamily/{}/{}/{}/pdistances.csv".format(self.phylum_name, self.family_name, self.genus_name)

        # Log file
        self.output_filenameLog = "../../../Log/calPdist.log"


    def loadSequences(self):
        '''
            Load the sequences for one family
            Read a fasta file
            Return a sequences object
        '''

        # Read the FASTA file
        try:
            sequences_object = FastaFile(self.input_filenameMSA)
            os.remove("../../../Output/intraFamily/{}/{}/MSA.fas.fai".format(self.phylum_name, self.family_name))
            return sequences_object

        except IOError as e: # If file could not be opened
            f = open(self.output_filenameLog, "a")
            f.write("{} file could not be opened by the FASTA parser.".format(self.input_filenameMSA))
            f.close()


    def load_SequenceInformations(self, list_sequenceIDs):
        '''
            Load the file containing the informations about the sequences
        '''
        # Read the data
        df_sequences = pd.read_csv(self.input_filename_sequencesInformations, sep='\t')

        # Creating a bool series from isin()
        df_current_sequences = df_sequences.sequenceID.isin(list_sequenceIDs)
  
        # Displaying data with those sequences IDs only 
        df_sequences = df_sequences[df_current_sequences]

        return df_sequences


    def removeNominalSpeciesWithOneSequence(self, df_sequences):
        '''
            Remove the nomimal species having only 1 sequence (so having 1 BIN)
            Return the same cleaning dataframe
        '''

        df_sequences_oneSequence = df_sequences.groupby(["species_name"]).filter(lambda x: x["sequenceID"].nunique() == 1)

        df_sequences_oneSequence.reset_index() # Convert into dataframe

        # Remove the rows of the nomimal species having only 1 sequence
        return df_sequences.append(df_sequences_oneSequence).drop_duplicates(keep=False)


    def getSpeciesName(self, df_sequences, id_seq):
        '''
            Return the species name for an identifiant sequence given
        '''

        species_name = df_sequences[df_sequences.sequenceID == int(id_seq)].species_name.iloc[0]

        return species_name


    def getBIN(self, df_sequences, id_seq):
        '''
            Return the BIN number for an identifiant sequence given
        '''

        bin_uri = df_sequences[df_sequences.sequenceID == int(id_seq)].bin_uri.iloc[0]

        return bin_uri


    def calculatePdist(self, df_sequences, sequences_object, output):
        '''
            Calculate the all p-distance between the all sequences inside a family
        '''

        # Remove the nomimal species having only 1 sequence
        df_sequences = self.removeNominalSpeciesWithOneSequence(df_sequences)

        # Update the list of the sequence IDs and remove eventually the duplicated ID
        list_sequencesID = list(set(df_sequences.sequenceID.to_list()))

        # All possible combinaisons between the pairwise sequences
        pairwiseCombinaisons = list(itertools.combinations(list_sequencesID, 2))

        ## Retrieve the position where a GAP is the latest at the 5', and where a GAP is the first at the 3'
        # Initialization of the maximum
        max_latest_gap_5P = 0
        len_MSA = len(sequences_object.fetch(sequences_object.references[0]))

        max_first_gap_3P = len_MSA
        # Aim: remove the GAP at extremeties
        for i in sequences_object.references:
            seq = sequences_object.fetch(i)
            latest_gap_5P = [m.end(0) for m in re.finditer('^-*', seq)][0] # at 5', is the position of a "-" character
            first_gap_3P = [m.start(0) for m in re.finditer('-*$', seq)][0] # 3'n is the position of a nucleotidic character
            # At 5P, select the GAP the latest
            if latest_gap_5P > max_latest_gap_5P:
                max_latest_gap_5P = latest_gap_5P
            # At 3P, select the first GAP
            if first_gap_3P < max_first_gap_3P:
                max_first_gap_3P = first_gap_3P

        # List to contain data
        rows = []

        # For each combinaison of sequences
        for pair in pairwiseCombinaisons:

            # Identifiants of the sequences
            id1 = pair[0]
            id2 = pair[1]

            # Get the species names
            species_name1 = self.getSpeciesName(df_sequences, id1)
            species_name2 = self.getSpeciesName(df_sequences, id2)

            # Get the BIN name
            bin_uri1 = self.getBIN(df_sequences, id1)
            bin_uri2 = self.getBIN(df_sequences, id2)

            # Get the nucleotides of the sequences
            seq1 = sequences_object.fetch(str(id1))
            seq2 = sequences_object.fetch(str(id2))

            # Get the lengths of the sequences
            len_seq1 = len(str(seq1).replace("-",""))
            len_seq2 = len(str(seq2).replace("-",""))

            # Cut the sequences for not considering the positions with GAP at extremities for at least one sequence
            seq1 = seq1[max_latest_gap_5P:max_first_gap_3P]
            seq2 = seq2[max_latest_gap_5P:max_first_gap_3P]

            # Length of the alignment affer removing the GAPs at the extremities
            len_MSA2 = len(seq1)

            # Initialization of the number of the mismatches between the 2 sequences
            mismatches = 0
            # Initilization of the number of the nucleotides using during the calculation of the p-distance pairwise
            len_pairwise_aln = 0

            # Perform the p-distance calculation
            for i in range(len_MSA2):
                if seq1[i] == "-" or seq2[i] == "-" or seq1[i] == "n" or seq2[i] == "n":
                    "" # Ignore extremities not overlapping, here delete complete pairwise = FALSE
                else:
                    len_pairwise_aln+=1
                    if seq1[i] != seq2[i]:
                        mismatches+=1

            pdist = mismatches/len_pairwise_aln

            # Write a line
            row = [id1, id2, species_name1, species_name2, bin_uri1, bin_uri2, pdist, len_seq1, len_seq2, len_MSA, len_MSA2, len_pairwise_aln]

            rows.append(row)

        # Convert to panda format
        all_rows = [x for x in rows if x != []]
        header = ['sequenceID1', 'sequenceID2', 'species_name1', 'species_name2', 'bin_uri1', 'bin_uri2', 'pdist', 'len_sequence1', 'len_sequence2', 'len_MSA', 'len_MSA_cutGapsExtremities', 'len_pairwise_aln']
        df_allPdist = pd.DataFrame(all_rows, columns=header)

        # Export the dataframe into CSV file
        df_allPdist.to_csv(output, sep='\t', index=False)    


    def perGenus(self, isBigFamily):
        '''
            For the big family (number of sequences > 1000), consider the genus taxonomic level
        '''

        # Name input and output filenames, according to the current family
        self.inputPathways()
        self.outputPathways()
        

        if isBigFamily == False:
            # Load the all sequences inside this family
            sequences_object = self.loadSequences()
            list_sequencesID = sequences_object.references

            # Load the informations about the sequences: retrieve their BIN(s), their nominal species name, genus name and family name
            df_sequences = self.load_SequenceInformations(list_sequencesID)

            self.calculatePdist(df_sequences, sequences_object, list_sequencesID, self.output_filenamePerFamily)

        else:

            # Load the informations about the sequences: retrieve their BIN(s), their nominal species name, genus name and family name
            df_Sequences = pd.read_csv(self.input_filename_sequencesInformations, sep='\t')

            # Load the genus from the family
            df_currentFamily = df_Sequences[df_Sequences.family_name == self.family_name]
            genus_names = df_currentFamily.genus_name.unique()

            for genus_name in genus_names:

                # Create a folder for the genus
                createDirectory("../../../Output/intraFamily/{}/{}/{}".format(self.phylum_name, self.family_name, genus_name))

                if not os.path.exists("../../../Output/intraFamily/{}/{}/{}/pdistances.csv".format(self.phylum_name, self.family_name, genus_name)):

                    # Update the name of the output
                    self.genus_name = genus_name
                    self.outputPathways()

                    # Load the all sequences inside a family
                    sequences_object_MSA = self.loadSequences()

                    # Retrieve the sequences in the MSA
                    list_sequencesInMSA = sequences_object_MSA.references

                    # Select only the sequences of the current genus of this family
                    seqID_currentGenus = list(df_currentFamily[df_currentFamily.genus_name == genus_name].sequenceID)

                    # Convert the strings into integers
                    list_sequencesInMSA = list(map(int, list_sequencesInMSA))
                    seqID_currentGenus = list(map(int, seqID_currentGenus))

                    # Remove the sequences not in the MSA (i.e. removing sequences after correction and adjustment, and sequences with more 10 ambigous nucleotids)
                    seqID_currentGenus1 = list(itertools.filterfalse(lambda x:x not in list_sequencesInMSA, seqID_currentGenus))

                    # Load the informations about the sequences: retrieve their BIN(s), their nominal species name, genus name and family name
                    df_sequences = self.load_SequenceInformations(seqID_currentGenus1)
                  
                    # Remove the sequences not in the MSA
                    MSA_tmp = "../../../Output/intraFamily/{}/{}/{}/MSA_tmp.fas".format(self.phylum_name, self.family_name, genus_name)
                    sequences_update = []
                    with open(MSA_tmp, 'w'):
                        for record in SeqIO.parse(self.input_filenameMSA, 'fasta'):
                            if int(record.id) in seqID_currentGenus:
                                sequences_update.append(record)
                    # Write the updated FASTA file
                    SeqIO.write(sequences_update, MSA_tmp, 'fasta')

                    if os.path.getsize(MSA_tmp) != 0:
                        # Update the sequences object, i.e. remove sequences not in the MSA
                        sequences_object_update = FastaFile(MSA_tmp)
                        os.remove(MSA_tmp)
                        os.remove("../../../Output/intraFamily/{}/{}/{}/MSA_tmp.fas.fai".format(self.phylum_name, self.family_name, genus_name))
                        
                        # Calculate the p-distances for the current genus
                        self.calculatePdist(df_sequences, sequences_object_update, self.output_filenamePerGenus)
