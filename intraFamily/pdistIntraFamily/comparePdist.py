#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import itertools # Retrieve the pairwise combinaisons
import pandas as pd
import numpy as np
import math
import os

class Family_comparisons():

    def __init__(self, phylum_name, family_name, genus_name):
        '''
           A family is defined by:
           - A phylum name
           - A family name
           - An input: the all p-distances
           - An output: the comparisons between the p-distances
        '''

        # Retrieve the phylum name and the family name of the current family
        self.phylum_name = phylum_name
        self.family_name = family_name
        self.genus_name = genus_name

        # Input
        self.input_filenameAllPdist  = ""

        # Output
        self.output_filename = ""

        # Structure to contain data
        self.df_allPdist = pd.DataFrame()


    def outputPathway(self):
        '''
            Name the output filename according to the phylum and the family
        '''

        self.output_filenameLog = "../../../Log/comparePdist.log" # Log file


    def perGenus(self, isBigFamily):
        '''
            For the big family (number of sequences > 1000), consider the genus taxonomic level
            Name the input and the output filenames according to the phylum and the family
        '''

        if isBigFamily == False:
            self.input_filenameAllPdist = "../../../Output/intraFamily/{}/{}/{}/pdistances.csv".format(self.phylum_name, self.family_name, self.genus_name)
            self.output_filename = "../../../Output/intraFamily/{}/{}/{}/comparisonsPdist.csv".format(self.phylum_name, self.family_name, self.genus_name)
        else:
            self.input_filenameAllPdist = "../../../Output/intraFamily/{}/{}/{}/pdistances.csv".format(self.phylum_name, self.family_name, self.genus_name)
            self.output_filename = "../../../Output/intraFamily/{}/{}/{}/comparisonsPdist.csv".format(self.phylum_name, self.family_name, self.genus_name)


    def get_allPdist(self):
        '''
            Get the all p-distances
        '''

        # Read data
        if os.path.exists(self.input_filenameAllPdist):
            self.df_allPdist = pd.read_csv(self.input_filenameAllPdist, sep="\t")
        else:
            self.df_allPdist = pd.DataFrame()


    def get_CS_level0(self):
        '''
            Get the names of all candidate CS level 0, inside a family
        '''
        
        # Select the nominal species with at least 2 sequences
        df_CS_level0 = self.df_allPdist[self.df_allPdist.species_name1 == self.df_allPdist.species_name2]

        # Select the nominal species with 2 different BINs
        df_CS_level0 = df_CS_level0[df_CS_level0.bin_uri1 != df_CS_level0.bin_uri2]

        # Convert into list
        df_CS_level0 = set(df_CS_level0["species_name1"].to_list())

        return df_CS_level0



    #--------------#
    #  DIVERGENCE  #
    #--------------#

    def get_df_interBINs(self, candidateCS):
        '''
            Only inside one candidate CS
            Calculate the pi-within between all BINs (pairwise) of a candidate CS
        '''

        # Select only the candidate CS (level 0) of interest
        df_interBINs = self.df_allPdist[(self.df_allPdist.species_name1 == candidateCS) & (self.df_allPdist.species_name2 == candidateCS)]

        # Select the pairwise sequences between the different BINs
        df_interBINs = df_interBINs[df_interBINs.bin_uri1 != df_interBINs.bin_uri2]

        # Considering the pairwise tuples (A,B), it's possible to find (B,A), need to transform all (B,A) into (A,B)
        # Warning: this not changes the final mean of the p-distances
        df_interBINs[['bin_uri1', 'bin_uri2']] = np.sort(df_interBINs[['bin_uri1', 'bin_uri2']], axis=1)

        # Group by each pairwise of BINs
        df_interBINs = df_interBINs.groupby(by=['bin_uri1', 'bin_uri2'])
        df_interBINs = df_interBINs.aggregate({"pdist": "mean"}).reset_index() # Mean of p-distances between 2 BINs

        return df_interBINs


    def get_df_pdist_interSpecies(self, one_candidateCS):
        '''
            Retrieve the p-distances between two nominal species: their all sequences pairwise
            For all nominal species of a family without the current candidate CS
        '''

        # Remove the current candidate CS
        df_interSpecies = self.df_allPdist[(self.df_allPdist.species_name1 != one_candidateCS) & (self.df_allPdist.species_name2 != one_candidateCS)]

        # Select only the p-distances between two different nominal species
        df_interSpecies = df_interSpecies[df_interSpecies.species_name1 != df_interSpecies.species_name2]

        # Mean between 2 nominal species
        if not df_interSpecies.empty: # If inside a family, there are only 2 species or only 1 species (possible if the species have > 1 BINs): comparisons not possible

            # Considering the pairwise tuples
            # Warning: the sequence IDs can be reverse with the species name, but this not changes the final mean of the p-distances
            df_interSpecies[['species_name1', 'species_name2']] = np.sort(df_interSpecies[['species_name1', 'species_name2']], axis=1)
            df_interSpecies = df_interSpecies.groupby(by=['species_name1', 'species_name2']) # Group by 2 nominal species

            df_interSpecies = df_interSpecies.aggregate({"pdist": "mean"}).reset_index()

            return df_interSpecies

        else:

            return pd.DataFrame()




    #--------------#
    # POLYMORPHISM #
    #--------------#

    def get_polymorphism_intraCandidateCS(self, candidateCS, BIN1, BIN2):
        '''
            Polymorphism within a candidate Cryptic Species
            Weighted mean of the p-distances

            Warning: We can't calculate the p-dist INTRAbin inside a candidate CS level 0 containing only one sequence in the BIN1 and in the BIN2.
            In this case, the function returns a empty dataframe.
        '''

        # Select the current level 0 candidate CS
        df_intraBINs = self.df_allPdist[(self.df_allPdist.species_name1 == candidateCS) & (self.df_allPdist.species_name2 == candidateCS)]

        # Select the pairwise sequences inside the same BIN
        df_intraBINs = df_intraBINs[df_intraBINs.bin_uri1 == df_intraBINs.bin_uri2]

        # Select the 2 current BINs
        filter1 = df_intraBINs['bin_uri1'] == BIN1
        filter2 = df_intraBINs['bin_uri1'] == BIN2
        df_intraBINs = df_intraBINs[filter1 | filter2]

        if not df_intraBINs.empty:
            # Inside a nominal species: calculate the weighted mean between 2 BINs
            weightedMean = df_intraBINs.aggregate({"pdist": "mean"}).pdist
            return weightedMean

        else:
            return None
        


    def get_polymorphism_intraNominalSpecies(self, species_name1, species_name2):
        '''
            Polymorphism within a pair of nominal species:
            1. Mean inside the nominal species 1
            2. Mean inside the nominal species 2
            3. Mean of those means
            BINs are not considered, all sequences of the nominal species are compared in pairs.
            Each species have at least 2 sequences (pre-selection).
        '''

        # Select only the nominal species 1 of interest
        df_intraSpecies1 = self.df_allPdist[(self.df_allPdist.species_name1 == species_name1) & (self.df_allPdist.species_name2 == species_name1)]

        # Select only the nominal species 2 of interest
        df_intraSpecies2 = self.df_allPdist[(self.df_allPdist.species_name1 == species_name2) & (self.df_allPdist.species_name2 == species_name2)]

        # Remove the nominal species with only one sequence (a pre-selection have been already done)
        if not df_intraSpecies1.empty and not df_intraSpecies2.empty:

            # Mean of the p-distances, inside the nominal species 1
            mean_intraSpecies1 = df_intraSpecies1.aggregate({"pdist": "mean"}).pdist

            # Mean of the p-distances, inside the nominal species 2
            mean_intraSpecies2 = df_intraSpecies2.aggregate({"pdist": "mean"}).pdist

            # Mean between those two nominal species
            mean_intraSpecies = (mean_intraSpecies1 + mean_intraSpecies2)/2

            return mean_intraSpecies

        else:
            return None


    #--------------#
    #  COMPARISONS #
    #--------------#

    def comparisonsPdist(self):
        '''
            Compare the p-distances:
            1. the p-distances between 2 BINs of a candidate level 0 cryptic species.
            VS
            2. The p-distances between 2 other nominal species inside the same family.
        '''
        print(self.genus_name)
        # Output pathways
        self.outputPathway()

        # Load data
        self.get_allPdist()

        if not os.path.exists(self.output_filename):

            if not self.df_allPdist.empty:

                # Retrieve the name of the candidate CS level 0
                potentialCS = self.get_CS_level0()

                # List to contain the lines
                all_rows = []

                #--------------------------------------------------------------------#
                # Inside one nominal species designated as a cryptic species level 0 #
                #--------------------------------------------------------------------#
                for one_potentialCS in potentialCS:

                    # Retrieve the all p-distances BETWEEN the BINs inside the current potential CS
                    df_interBINs = self.get_df_interBINs(one_potentialCS)            

                    #--------------------------------------------#
                    # For each pair of the other nominal species #
                    #--------------------------------------------#
                    # All p-distances BETWEEN nominal species, without the current candidate CS
                    df_pdist_otherSpecies = self.get_df_pdist_interSpecies(one_potentialCS)

                    # If there is enought nominal species to compare with the current candidate CS 
                    if not df_pdist_otherSpecies.empty:
                        # Select the pairwise of p-distances from the other species
                        for index, a_row in df_pdist_otherSpecies.iterrows():

                            otherSpecies1 = a_row.species_name1 # The name of the other nominal species 1
                            otherSpecies2 = a_row.species_name2 # The name of the other nominal species 2

                            pdist_interSpecies = a_row.pdist # The p-distance between the two other nominal species

                            #------------------------#
                            # For each pairwise BINs #
                            #------------------------#
                            if not df_interBINs.empty:
                                for index, row in df_interBINs.iterrows():

                                    bin_uri1 = row.bin_uri1 # The name of the BIN 1
                                    bin_uri2 = row.bin_uri2 # The name of the BIN 2
                                    
                                    pdist_interBIN = row.pdist # The p-distance between those two BINs

                                    # Retrieve the p-distances INTRA BINs inside those 2 BINs of this current potential CS
                                    pdist_intraBIN = self.get_polymorphism_intraCandidateCS(one_potentialCS, bin_uri1, bin_uri2)

                                    #--------#
                                    # Test 1 #
                                    #--------#
                                    
                                    try:
                                        ratio_test1 = pdist_interBIN/pdist_interSpecies
                                    except ZeroDivisionError:
                                        #Sometimes, the divergence between two other nominal species from the same family can be equal to 0 
                                        #due to the fact the sequences become identical after the precedent cutting. 
                                        ratio_test1 = float("inf")

                                    #--------------------------------#
                                    # Test 2: Including polymorphism #
                                    #--------------------------------#
                                    # Polymorphism inside the two other nominal species
                                    pdist_intraSpecies = self.get_polymorphism_intraNominalSpecies(otherSpecies1, otherSpecies2)

                                    if pdist_intraSpecies != None: # If the nominal species with only one sequence (but a pre-selection have been already done)

                                        a = pdist_intraBIN
                                        b = pdist_interBIN
                                        c = pdist_intraSpecies
                                        d = pdist_interSpecies

                                        ratio_test2 = ""

                                        if pdist_intraBIN == None: # Insufficient sequences: only 1 sequence in the BIN1, only 1 sequence in the other one
                                            ratio_test2 = ratio_test2 + "A"

                                        if pdist_intraBIN == 0.0: # Sequences inside BINs are identical
                                            ratio_test2 = ratio_test2 + "B"

                                        if pdist_intraSpecies == 0.0: # Insufficient sequences inside the two nominal species to determine polymorphism
                                            ratio_test2 = ratio_test2 + "C"

                                        if pdist_interBIN == 0.0: # The 2 BINs have same sequences (rarely !)
                                            ratio_test2 = ratio_test2 + "D"

                                        if pdist_interSpecies == 0.0: # The 2 nominal species have same sequences (rarely !)
                                            ratio_test2 = ratio_test2 + "E"

                                        if ratio_test2 == "":
                                            ratio_test2 = (a*d) / (b*c)
    
                                    else:
                                        ratio_test2 = None                                


                                    # Save data
                                    one_row = [one_potentialCS, bin_uri1, bin_uri2, otherSpecies1, otherSpecies2, ratio_test1, ratio_test2]

                                    all_rows.append(one_row)


                # Convert to panda format
                all_rows = [x for x in all_rows if x != []]
                df_output= pd.DataFrame(all_rows, columns=['candidateCS_test0', 'bin_uri1', 'bin_uri2', 'otherSpecies1', 'otherSpecies2', 'ratio_test1', 'ratio_test2'])
                df_output = df_output.sort_values(by=['candidateCS_test0', 'bin_uri1', 'bin_uri2', 'otherSpecies1', 'otherSpecies2'])

                # Export dataframe into CSV file
                df_output.to_csv(self.output_filename, sep='\t', index=False)

            else:

                f = open(self.output_filenameLog, "a")
                f.write("PHYLUM: {} FAMILY: {} : The p-distance file is empty.\n".format(self.phylum_name, self.family_name))
                f.close()