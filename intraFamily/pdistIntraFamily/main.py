#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from calPdist import *
from comparePdist import *
import argparse


program_description = ''' ************ PROGRAM DESCRIPTION ************\n
    Inside a phylum, for each family:\n
    Calculate the p-distances between the sequences.\n
    Compare:\n
    1. The p-distances intra-BINs inside a candidat cryptic species level 0 (i.e. containing severals BINs).\n
    with\n
    2. The p-distances inter-species between two other nominal species inside the same family.
   '''


def create_parser(): # Retrieve the arguments before start the program

    parser = argparse.ArgumentParser(add_help=True,
                                     description=program_description)

    ## Phylum we want to study
    # Via a string
    parser.add_argument('-Phylum','--PhylumName',
                        help="Name of the phylum",
                        type=str,
                        default=None,
                        required=False) # Not necessary to indicate the phylum name

    # Via a file
    parser.add_argument('-PhylumFile','--PhylumFileName',
                        help="Name of the phylum file containing the names",
                        type=argparse.FileType('r'),
                        default=None,
                        required=False) # Not necessary to indicate the phylum name

    ## The family we want to study
    # Via a string
    parser.add_argument('-Family','--FamilyName',
                        help="Name of the family",
                        type=str,
                        default=None,
                        required=False) # Not necessary to indicate the phylum name

    # Via a file
    parser.add_argument('-FamilyFile','--FamilyFileName',
                        help="Name of the family file containing the names",
                        type=argparse.FileType('r'),
                        default=None,
                        required=False) # Not necessary to indicate the phylum name
         
    return parser


def parse_arguments(): # Return a dictionary of arguments
    # Example: {"Phylum": "Mollusca"}

    parser = create_parser() # Creation of the parser
    args = parser.parse_args() # Parse the arguments supplied by the user
    dict_args = dict(args.__dict__)

    return dict_args


def main(PhylumName, PhylumFileName, FamilyName, FamilyFileName):

    print("----- START P-DISTANCE PAIRWISE INTRA-FAMILIES -----\n")

    # Create directories for the results
    createDirectory("../../../Output")
    createDirectory("../../../Log")
    createDirectory("../../../Output/intraFamily")

    # Create log file
    f1 = open("../../../Log/calPdist.log", "w")
    f1.close()
    # Create log file
    f2 = open("../../../Log/comparePdist.log", "w")
    f1.close()

    # Retrieve phylum name avalaible
    available_phylum = retrieveDirectories("../../../Output/intraFamily")

    # Retrieve phylum name(s)
    if PhylumName == None and PhylumFileName == None:
        # 1. Retrieve all phylum inside sequence folder
        list_phylum = available_phylum

    elif PhylumFileName == None:
        # 2. Retrieve phylum name by string
        list_phylum = [PhylumName]

    elif PhylumName == None:
        # 3. Retrieve phylum name by file
        list_phylum = []
        for phylum_name in PhylumFileName.readlines():
            list_phylum.append(phylum_name.strip())

    # Read family names file
    if FamilyFileName != None:
        families_from_FILE = [] # inizialisation
        for family_name in FamilyFileName.readlines():
            families_from_FILE.append(family_name.strip())

    for phylum_name in list_phylum:

        if phylum_name not in available_phylum:
            
            print("\nThe phylum name {} is not available.".format(phylum_name))

        else:
            
            print("\nThe phylum name {} is available.".format(phylum_name))

            #-----------------#
            # For each family #
            #-----------------#

            # Retrieve the family names avalaible for this current phylum
            available_families_tmp = retrieveFilenames("../../../Data/sequences/corrected_and_fitted_Sequences/{}".format(phylum_name))
            available_families = []
            for available_family in available_families_tmp:
                available_families.append(available_family.replace(".fas", ""))


            # Retrieve familiy name(s)
            if FamilyName == None and FamilyFileName == None:
                # 1. Retrieve all families inside sequence folder
                list_families = available_families

            elif FamilyFileName == None:
                # 2. Retrieve family name by string
                list_families = [FamilyName]

            elif FamilyName == None:
                # 3. Retrieve family name by file
                list_families = families_from_FILE


            # Retrieve all families inside the sequences files
            for family_name in list_families:

                if family_name not in available_families:
                    
                    ""
                    print("The family name {} is not available.".format(family_name))

                else:

                    print("\nPHYLUM: ", phylum_name, " FAMILY:", family_name)

                    # Create an output folder for one family
                    createDirectory("../../../Output/intraFamily/{}/{}".format(phylum_name, family_name))

                    MSA_filename = "../../../Output/intraFamily/{}/{}/MSA.fas".format(phylum_name, family_name)

                    filesizeMSA = None
                    if os.path.exists(MSA_filename):
                        filesizeMSA = os.path.getsize(MSA_filename)

                    if os.path.exists(MSA_filename) and filesizeMSA != 0:

                        # Count number of sequences in the family
                        nbSeqFamily = 0
                        f = open(MSA_filename)
                        for i in f.readlines():
                            if ">" in i:
                                nbSeqFamily+=1
                        f.close()

                        if nbSeqFamily <= 1000:
                        
                            if not os.path.exists("../../../Output/intraFamily/{}/{}/pdistances.csv".format(phylum_name, family_name)):
                                
                                # Calculate p-distance parwise
                                print("\nPHYLUM: ", phylum_name, " FAMILY:", family_name)
                                print("1. All p-distances")
                                family_allPdist = Family_AllPdist(phylum_name, family_name)
                                family_allPdist.perGenus(False)

                            if os.path.exists("../../../Output/intraFamily/{}/{}/pdistances.csv".format(phylum_name, family_name)) and not os.path.exists("../../../Output/intraFamily/{}/{}/comparisonsPdist.csv".format(phylum_name, family_name)):
                                
                                # Comparisons of p-distances
                                print("\nPHYLUM: ", phylum_name, " FAMILY:", family_name)
                                print("2. Comparisons")
                                family_comparisons = Family_comparisons(phylum_name, family_name, "")
                                family_comparisons.perGenus(False)
                                family_comparisons.comparisonsPdist()

                        else:

                            # For the big families, use the genus level
                            print("\nPHYLUM: ", phylum_name, " FAMILY:", family_name, "NUMBER OF SEQ = {}".format(nbSeqFamily))
                            
                            family_allPdist = Family_AllPdist(phylum_name, family_name)
                            family_allPdist.perGenus(True)
                            
                            genus_names = retrieveDirectories("../../../Output/intraFamily/{}/{}".format(phylum_name, family_name))

                            if 'compare_pi_within_interBINs' in genus_names: # Remove files from criterion 3
                                genus_names.remove('compare_pi_within_interBINs')

                            for genus_name in genus_names:

                                pdist_filename = "../../../Output/intraFamily/{}/{}/{}/pdistances.csv".format(phylum_name, family_name, genus_name)

                                if os.path.exists(pdist_filename):
                                        
                                    print("**")
                                    print("\nPHYLUM: ", phylum_name, " FAMILY:", family_name, "NUMBER OF SEQ = {}".format(nbSeqFamily))

                                    family_comparisons = Family_comparisons(phylum_name, family_name, genus_name)
                                    family_comparisons.perGenus(False)
                                    family_comparisons.comparisonsPdist()

    print("\n----- END P-DISTANCE PAIRWISE INTRA-FAMILIES ------\n")


if __name__ == '__main__':
    arguments = parse_arguments()
    main(**arguments)
