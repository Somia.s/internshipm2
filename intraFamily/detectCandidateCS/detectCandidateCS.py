#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np

class all_Species():

    def __init__(self):
        '''
            All species are analysis to detect CS.
        '''

        #---------------#
        #    Inputs     #
        #---------------#             
        # All nominal species and their number of BINs
        self.input_filenameSpecies_and_nb_BINs = "../../../Output/descriptiveStatistics/species_and_nb_BINs.csv"
        # All nominal species and complete taxonomic informations
        self.input_filenameSpecies_taxonomicRank = "../../../Output/descriptiveStatistics/all_nominalSpecies.csv"

        #--------------------------------#
        #    Struture to contain data    #
        #--------------------------------#        
        self.df_species_and_nb_BINs = pd.DataFrame()
        self.df_nbOtherSpeciesInFamily = pd.DataFrame()
        self.list_candidateCS_criterion0 = []
        self.list_species_failing_criterion0 = []
        self.df_infoTaxonomicRank = pd.DataFrame()

        #----------------#
        #     Outputs    #
        #----------------#
        self.output_filename_withoutBiblio_failedCriterion0 = "../../../Output/detectCandidateCS/detectSpecies_withoutBiblio_failedCriterion0.csv"
        self.output_filename_withoutBiblio = "../../../Output/detectCandidateCS/detectCandidateCS_withoutBiblio.csv"

        # Load species and their BINs
        self.loadSpeciesAndTheirBINs()



    def loadSpeciesAndTheirBINs(self):

        # All nominal species of the study and their number of BINs
        self.df_species_and_nb_BINs = pd.read_csv(self.input_filenameSpecies_and_nb_BINs, sep="\t")

        # Number of other species in family
        self.df_nbOtherSpeciesInFamily = self.df_species_and_nb_BINs['family_name'].value_counts() - 1

        # Fetch the name of the nominal species having failed the criterion 0
        self.list_species_failing_criterion0 = self.df_species_and_nb_BINs[self.df_species_and_nb_BINs.number_of_BINs == 1].species_name.unique()

        # Fetch the name of the nominal species having successed the criterion 0
        self.list_candidateCS_criterion0 = self.df_species_and_nb_BINs[self.df_species_and_nb_BINs.number_of_BINs > 1].species_name.unique()

        # Retrieve the taxonomic ranks of the species
        self.df_taxonomicRanks = pd.read_csv(self.input_filenameSpecies_taxonomicRank, sep="\t")



    def detectSpeciesFailedCriterion0(self):

        #-----------------------------------------------------------------------------------------------#
        #                                                                                               #
        #                         The species having failed the criterion 0                             #
        #                                                                                               #
        #-----------------------------------------------------------------------------------------------#  
        
        # Structure to contain data
        all_rows = []

        for current_species_failing_criterion0 in self.list_species_failing_criterion0:
        
            test0 = False
            # The test 1 and 2 can't be performed without at least 2 BINs in the nominal species
            test1 = None 
            test2 = None

            # Fetch the informations about the taxonomic rank for the current nominal species
            df_taxonomicRank = self.df_taxonomicRanks[self.df_taxonomicRanks.species_name == current_species_failing_criterion0]
            phylum_name = df_taxonomicRank.phylum_name.iloc[0]
            class_name = df_taxonomicRank.class_name.iloc[0]
            order_name = df_taxonomicRank.order_name.iloc[0]
            family_name = df_taxonomicRank.family_name.iloc[0]
            genus_name = df_taxonomicRank.genus_name.iloc[0]

            # Fetch the number of sequences
            nb_seq = self.df_species_and_nb_BINs[self.df_species_and_nb_BINs.species_name == current_species_failing_criterion0].number_of_sequences.iloc[0]

            # Fetch the number of other species in the family
            nb_otherSpeciesInFamily = self.df_nbOtherSpeciesInFamily[family_name]
            
            # Save the species result
            row = [phylum_name, class_name, order_name, family_name, genus_name, current_species_failing_criterion0, nb_seq, nb_otherSpeciesInFamily, test0, test1, test2, False]
            all_rows.append(row)

        #-----------------------------------#
        #          Save the data            #
        #-----------------------------------#

        # Convert to panda format
        all_rows = [x for x in all_rows if x != []]
        df_output = pd.DataFrame(all_rows, columns=['phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name', 'number_of_sequences', 'number_otherSpecies_inFamily', 'test0', 'test1', 'test2', 'genusLevel'])
        
        df_output.to_csv(self.output_filename_withoutBiblio_failedCriterion0, sep = "\t", index=False)



    def detectCS(self, siblingSisters, threshold):
        '''
            This function analyses each species.
            @param siblingSisters: consider only the sibling sisters or not
            @param threshold: thresholds
        '''

        # Structure to contain data
        all_rows = []


        #------------------------------------------------------------------------------------------------#
        #                                                                                                #
        #         The species having successed the criterion 0 i.e. having 2 or more BINs                #
        #                                                                                                #
        #------------------------------------------------------------------------------------------------#  

        for current_candidateCS in self.list_candidateCS_criterion0:
        
            # Test 0 is approved, the others not yet
            test0 = True
            test1 = False
            test2 = False

            # Fetch the informations about the taxonomic rank for the current nominal species
            df_taxonomicRank = self.df_taxonomicRanks[self.df_taxonomicRanks.species_name == current_candidateCS]
            phylum_name = df_taxonomicRank.phylum_name.iloc[0]
            class_name = df_taxonomicRank.class_name.iloc[0]
            order_name = df_taxonomicRank.order_name.iloc[0]
            family_name = df_taxonomicRank.family_name.iloc[0]
            genus_name = df_taxonomicRank.genus_name.iloc[0]

            # Fetch the number of sequences
            nb_seq = self.df_species_and_nb_BINs[self.df_species_and_nb_BINs.species_name == current_candidateCS].number_of_sequences.iloc[0]

            # Fetch the number of other species in the family
            nb_otherSpeciesInFamily = self.df_nbOtherSpeciesInFamily[family_name]

            # Fetch all p-distance comparisons
            input_filenameComparisonsPdist = "../../../Output/intraFamily/{}/{}/comparisonsPdist.csv".format(phylum_name, family_name)
            input_filenameComparisonsPdist_genus = "../../../Output/intraFamily/{}/{}/{}/comparisonsPdist.csv".format(phylum_name, family_name, genus_name)

            if not os.path.exists(input_filenameComparisonsPdist) and not os.path.exists(input_filenameComparisonsPdist_genus):
                
                # The result is not already runned
                test1 = None
                test2 = None
                
                # Save the species result
                row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, nb_seq, nb_otherSpeciesInFamily, test0, test1, test2, False]
                all_rows.append(row)

            else:
                
                if siblingSisters == False:

                    ########################################################################
                    #                              Per Genus                               #
                    ########################################################################
                    if os.path.exists(input_filenameComparisonsPdist_genus):

                        # Read the input file
                        df_myFamilyComparisonsPdist = pd.read_csv(input_filenameComparisonsPdist_genus, sep='\t')

                        if df_myFamilyComparisonsPdist.empty:

                            # Not enough other species in the family to test the criteria 1 and 2
                            test1 = None
                            test2 = None

                            # Save the species result
                            row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, nb_seq, nb_otherSpeciesInFamily, test0, test1, test2, True]
                            all_rows.append(row)

                        else:
                            # Select the result inside the family for only the current species
                            df_one_candidateCS_criterion0_compPdist = df_myFamilyComparisonsPdist[df_myFamilyComparisonsPdist.candidateCS_test0 == current_candidateCS]

                            # After fitting and cutting the sequences, some sequences have been removed and so some nominal species have losed BINs and have losed the criterion 0
                            if df_one_candidateCS_criterion0_compPdist.empty:
                                test1 = None
                                test2 = None

                            else:
                                
                                #--------------------#
                                #     Criterion 1    #
                                #--------------------#
                                ratios_criterion1 = df_one_candidateCS_criterion0_compPdist["ratio_test1"].to_list() # Fetch all ratio results

                                for ratio in ratios_criterion1:
                                    if ratio > 1:
                                        test1 = True

                                #--------------------#
                                #     Criterion 2    #
                                #--------------------#                        
                                ratios_criterion2 = df_one_candidateCS_criterion0_compPdist["ratio_test2"].to_list() # Fetch all ratio results

                                # If no ratio has been calculated for a nominal species, don't consider this nominal species
                                at_least_one_float = False 

                                for ratio in ratios_criterion2:
                                    if isinstance(ratio, float): # If all values inside the column contain a float value
                                        at_least_one_float = True
                                        if float(ratio) < 1:
                                            test2 = True
                                    elif ratio.find("A") == -1 and ratio.find("B") == -1 and ratio.find("C") == -1 and ratio.find("D") == -1 and ratio.find("E") == -1 and ratio != None and ratio != "":
                                        at_least_one_float = True
                                        if float(ratio) < 1:
                                            test2 = True

                                if at_least_one_float == False:
                                    test2 = None

                            # Save the species result
                            row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, nb_seq, nb_otherSpeciesInFamily, test0, test1, test2, True]
                            all_rows.append(row)



                ##########################################################################
                #                            Per family                                  #
                ##########################################################################

                if os.path.exists(input_filenameComparisonsPdist):

                    # Read the input file
                    df_myFamilyComparisonsPdist = pd.read_csv(input_filenameComparisonsPdist, sep='\t')

                    if df_myFamilyComparisonsPdist.empty:

                        # Not enough other species in the family to test the criteria 1 and 2
                        test1 = None
                        test2 = None

                        # Save the species result
                        row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, nb_seq, nb_otherSpeciesInFamily, test0, test1, test2, False]
                        all_rows.append(row)

                    else:
                        # Select the result inside the family for only the current species
                        df_one_candidateCS_criterion0_compPdist = df_myFamilyComparisonsPdist[df_myFamilyComparisonsPdist.candidateCS_test0 == current_candidateCS]

                        # After fitting and cutting the sequences, some sequences have been removed and so some nominal species have losed BINs and have losed the criterion 0
                        if df_one_candidateCS_criterion0_compPdist.empty:
                            test1 = None
                            test2 = None


                        else:

                            #------------------------#
                            #     Sibling sisters    #
                            #------------------------#

                            # Select only the sibling sisters for the comparisons
                            if siblingSisters == True:

                                grouped = df_one_candidateCS_criterion0_compPdist.groupby(["bin_uri1", "bin_uri2"])

                                bins_tuples = df_one_candidateCS_criterion0_compPdist[["bin_uri1", "bin_uri2"]].apply(tuple, axis=1).unique()

                                # The dataframe to contain the sibling sisters
                                df_siblingSisters = pd.DataFrame()

                                # Retrieve the sibling sisters
                                for bins_pair in bins_tuples:
                                    
                                    df_bins_pair = grouped.get_group(bins_pair)

                                    # Fetch the nominal species without the current criterion0 candidate CS
                                    all_otherSpecies = set(df_one_candidateCS_criterion0_compPdist[["otherSpecies1", "otherSpecies2"]].values.ravel().tolist())

                                    for one_otherSpecies in all_otherSpecies:
                                        
                                        df_one_tupleOtherSpecies = df_bins_pair[(df_bins_pair["otherSpecies1"] == one_otherSpecies) | (df_bins_pair["otherSpecies2"] == one_otherSpecies)]
                                        
                                        df_one_tupleOtherSpecies = df_one_tupleOtherSpecies[df_one_tupleOtherSpecies["ratio_test1"] != np.inf]
                                        df_one_tupleOtherSpecies = df_one_tupleOtherSpecies[df_one_tupleOtherSpecies["ratio_test1"] == df_one_tupleOtherSpecies["ratio_test1"].max()]
                                        
                                        speciesPair = df_one_tupleOtherSpecies[["otherSpecies1", "otherSpecies2"]].values.ravel().tolist()
                                        speciesPair.remove(one_otherSpecies)

                                        df_siblingSisters = pd.concat([df_siblingSisters, df_one_tupleOtherSpecies])

                                        df_siblingSisters = df_siblingSisters.drop_duplicates(subset=["bin_uri1", "bin_uri2", "otherSpecies1", "otherSpecies2"])

                                
                                df_one_candidateCS_criterion0_compPdist = df_siblingSisters

                            else:
                                ""
                                # No nothing, no filter


                            #--------------------#
                            #     Criterion 1    #
                            #--------------------#

                            if threshold == None:
                                ratios_criterion1 = df_one_candidateCS_criterion0_compPdist["ratio_test1"].to_list() # Fetch all ratio results

                                for ratio in ratios_criterion1:
                                    if ratio > 1:
                                        test1 = True

                            else:
                                count_ratioSup1 = df_one_candidateCS_criterion0_compPdist[df_one_candidateCS_criterion0_compPdist['ratio_test1'] > 1].count().iloc[0]
                                proportion_ratios = count_ratioSup1/len(df_one_candidateCS_criterion0_compPdist)

                                if proportion_ratios > threshold:
                                    test1 = True

                            #--------------------#
                            #     Criterion 2    #
                            #--------------------#

                            ratios_criterion2 = df_one_candidateCS_criterion0_compPdist["ratio_test2"].to_list() # Fetch all ratio results

                            # Count the number of floats
                            nb_floats = 0
                            # Count the number of ratio < to 1
                            ratioInf1 = 0
                            
                            for ratio in ratios_criterion2:
                                if isinstance(ratio, float): # If all values inside the column contain a float value
                                    nb_floats+= 1
                                    if float(ratio) < 1:
                                        if threshold == None:
                                            test2 = True
                                        else:
                                            ratioInf1 = 0
                                elif ratio.find("A") == -1 and ratio.find("B") == -1 and ratio.find("C") == -1 and ratio.find("D") == -1 and ratio.find("E") == -1 and ratio != None and ratio != "":
                                    nb_floats+= 1
                                    if float(ratio) < 1:
                                        if threshold == None:
                                            test2 = True
                                        else:
                                            ratioInf1+= 1

                            if nb_floats == 0:
                                test2 = None # If no ratio has been calculated for a nominal species, don't consider this nominal species
                            else:
                                proportion = ratioInf1/nb_floats
                                if proportion > threshold:
                                    test2 = True

                        # Save the species results
                        row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, nb_seq, nb_otherSpeciesInFamily, test0, test1, test2, False]
                        all_rows.append(row)


        #-----------------------------------#
        #          Save the data            #
        #-----------------------------------#

        # Convert to panda format
        all_rows = [x for x in all_rows if x != []]
        df_output = pd.DataFrame(all_rows, columns=['phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name', 'number_of_sequences', 'number_otherSpecies_inFamily', 'test0', 'test1', 'test2', 'genusLevel'])
        
        # Import into CSV file
        if siblingSisters == True and threshold != None:
            self.output_filename_withoutBiblio = "../../../Output/detectCandidateCS/siblingSisters_thresholds/detectCandidateCS_withoutBiblio_siblingSisters_{}.csv".format(threshold)
        elif siblingSisters == True:
            self.output_filename_withoutBiblio = "../../../Output/detectCandidateCS/siblingSisters/detectCandidateCS_withoutBiblio_siblingSisters.csv".format()
        elif threshold != None:
            self.output_filename_withoutBiblio = "../../../Output/detectCandidateCS/ratiosThresholds/detectCandidateCS_withoutBiblio_{}.csv".format(threshold)
                      
        df_output.to_csv(self.output_filename_withoutBiblio, sep = "\t", index=False)
