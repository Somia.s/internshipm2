#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import plotly.express as px
import numpy as np
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from commonFunctions import *


class CS_Results():

    def __init__(self):


        #-------------#
        #    Input    #
        #-------------#        
        self.input_filename = "../../../Output/detectCandidateCS/detectCandidateCS.csv"

        #-------------#
        #   Outputs   #
        #-------------#

        self.outputConfusionMatrix_filename = "../../../Output/detectCandidateCS/confusionMatrix.txt"
        self.output_filenameNumberCSPerFamily = "../../../Output/detectCandidateCS/detectCandidateCS_numbers.csv"

        # With biblio : Number of sequences
        self.output_criterion0_nbSeq_biblio = "../../../Output/detectCandidateCS/test0/nb_sequences_biblio.html"
        self.output_criterion1_nbSeq_biblio = "../../../Output/detectCandidateCS/test1/nb_sequences_biblio.html"
        self.output_criterion2_nbSeq_biblio = "../../../Output/detectCandidateCS/test2/nb_sequences_biblio.html"
       
        # With biblio : Number of nominal species
        self.output_criterion0_nbSpecies_biblio = "../../../Output/detectCandidateCS/test0/familySize_biblio.html"
        self.output_criterion1_nbSpecies_biblio = "../../../Output/detectCandidateCS/test1/familySize_biblio.html"
        self.output_criterion2_nbSpecies_biblio = "../../../Output/detectCandidateCS/test2/familySize_biblio.html"
        
        # Without biblio
        self.output_criterion0_withoutBiblio = "../../../Output/detectCandidateCS/test0/withoutBiblio.html"
        self.output_criterion1_withoutBiblio = "../../../Output/detectCandidateCS/test1/withoutBiblio.html"
        self.output_criterion2_withoutBiblio = "../../../Output/detectCandidateCS/test2/withoutBiblio.html"

        # Struture to contain data
        self.df_CSResults = pd.DataFrame()
        self.df_withoutBiblio = pd.DataFrame()

        # Load data
        self.loadResults()



    def loadResults(self):
        '''
            Load the results of tests
        '''

        self.df_CSResults = pd.read_csv(self.input_filename, sep="\t")
        self.df_CSResults = self.df_CSResults[self.df_CSResults.genusLevel == False]



    def countCS_perFamily(self):
        '''
            For each family, count the number of total nominal species, then those having successed the test0, 1 and 2
        '''

        # Group by the family name
        df_nb_nominal_species = self.df_finalOutput.groupby(by=['phylum_name', 'family_name'])

        # Aggregate the column of species name, this gives the number of nominal species inside a given family
        df_nb_nominal_species = df_nb_nominal_species.aggregate({"species_name": "count"}).reset_index()

        # Count the number of nominal species having successed the test 0, the test 1 and the test 2
        df_nb_success_test0 = self.df_finalOutput.set_index(['family_name']).test0.eq(True).sum(level=0).astype(int).reset_index()
        df_nb_success_test1 = self.df_finalOutput.set_index(['family_name']).test1.eq(True).sum(level=0).astype(int).reset_index()
        df_nb_success_test2 = self.df_finalOutput.set_index(['family_name']).test2.eq(True).sum(level=0).astype(int).reset_index()

        # Concatenate the dataframes
        df_detectCS_numbers = pd.concat([df_nb_nominal_species, df_nb_success_test0, df_nb_success_test1, df_nb_success_test2], axis=1)
        # Remove the duplicated columns after concatenating
        df_detectCS_numbers = df_detectCS_numbers.loc[:,~df_detectCS_numbers.columns.duplicated()]
        
        # Import into CSV file
        df_detectCS_numbers.to_csv(self.output_filenameNumberCSPerFamily, sep = "\t", index=False, header=['phylum_name', 'family_name', 'nb_nominal_species', 'nb_CS_test0', 'nb_CS_test1', 'nb_CS_test2'])



    def plotHistogram(self, df_results, x, color, level, label_x, pathHist):
        '''
            
        '''

        df_results = df_results.dropna(subset=[color])
        fig = px.histogram(df_results, 
                            x=x, # x
                            color=color, # y
                            marginal="rug", 
                            nbins=200,
                            title = "Test {} : The number of nominal species in terms of the number of {}".format(level, label_x))
        #fig.show()
        # Overlay both histograms
        fig.update_layout(barmode='overlay')
        # Reduce opacity to see both histograms
        fig.update_traces(opacity=0.75)

        fig.write_html(pathHist)


    def plotLineChart(self, df, x, y1, y2, pathway):
        '''
            Linechart
            x: 
            y: 
        '''

        fig1 = px.line(df, x=x, y=y1, title = "TP/TP+FN")
        fig2 = px.line(df, x=x, y=y2, title = "TN/TN+FP")
        
        fig = make_subplots(rows=2, cols=1, shared_xaxes=False)
        fig.add_trace(fig1['data'][0], row=1, col=1)
        fig.add_trace(fig2['data'][0], row=2, col=1)

        #fig.show()
        fig.write_html(pathway)


    def addTypeResultsBiblio(self):
        '''
            CS results with an available biblio
            Add a label for the TP, FN, FP and TN
        '''

        #  Results from test 0
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == True) & (self.df_CSResults['test0'] == True), 'Criterion 0'] = "TP"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == True) & (self.df_CSResults['test0'] == False), 'Criterion 0'] = "FN"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == False) & (self.df_CSResults['test0'] == True), 'Criterion 0'] = "FP"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == False) & (self.df_CSResults['test0'] == False), 'Criterion 0'] = "TN"

        #  Results from test 1
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == True) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test1'] == True), 'Criterion 1'] = "TP"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == True) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test1'] == False), 'Criterion 1'] = "FN"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == False) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test1'] == True), 'Criterion 1'] = "FP"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == False) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test1'] == False), 'Criterion 1'] = "TN"

        #  Results from test 2
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == True) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test2'] == True), 'Criterion 2'] = "TP"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == True) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test2'] == False), 'Criterion 2'] = "FN"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == False) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test2'] == True), 'Criterion 2'] = "FP"
        self.df_CSResults.loc[(self.df_CSResults['CS_biblio'] == False) & (self.df_CSResults['test0'] == True) & (self.df_CSResults['test2'] == False), 'Criterion 2'] = "TN"


    def checkTestBiblio(self, criterion, factor, outputFilename):
        '''
            Retrieve and compare:
            1. The true positives (TP) and false negatives (FN)
            2. The false positives (FP) and true negatives (TN)
        '''

        # For each label, count the number of occurences according to the number of sequences
        df_CSResults_TP = self.df_CSResults[self.df_CSResults[criterion] == 'TP'].groupby(by=[factor]).agg({criterion : 'count'}).reset_index()
        df_CSResults_FN = self.df_CSResults[self.df_CSResults[criterion] == 'FN'].groupby(by=[factor]).agg({criterion : 'count'}).reset_index()
        df_CSResults_FP = self.df_CSResults[self.df_CSResults[criterion] == 'FP'].groupby(by=[factor]).agg({criterion : 'count'}).reset_index()
        df_CSResults_TN = self.df_CSResults[self.df_CSResults[criterion] == 'TN'].groupby(by=[factor]).agg({criterion : 'count'}).reset_index()
        df_CSResults_total = self.df_CSResults.groupby(by=[factor]).agg({criterion : 'count'}).reset_index()

        # Rename the columns
        df_CSResults_TP = df_CSResults_TP.rename(columns={criterion: "TP"})
        df_CSResults_FN = df_CSResults_FN.rename(columns={criterion: "FN"})
        df_CSResults_FP = df_CSResults_FP.rename(columns={criterion: "FP"})
        df_CSResults_TN = df_CSResults_TN.rename(columns={criterion: "TN"})

        # Merge all the tables into only one
        df_CSResults = df_CSResults_total.merge(df_CSResults_TP, how = 'outer')
        df_CSResults = df_CSResults.merge(df_CSResults_FN, how = 'outer')
        df_CSResults = df_CSResults.merge(df_CSResults_FP, how = 'outer')
        df_CSResults = df_CSResults.merge(df_CSResults_TN, how = 'outer')

        # The max number of sequences or max number of other nominal species in the family
        max_nb_factor = max(df_CSResults[factor])

        # Cut the number of sequences into bins per 5 sequences
        bins = list(range(0, max_nb_factor, 5))
        labels = list(range(5, max_nb_factor, 5))
        df_CSResults['binned'] = pd.cut(df_CSResults[factor], bins, labels=labels)

        # Count the number of TP, FN, FP and TN in each bin
        df_CSResults = df_CSResults.groupby(by=['binned']).agg({'TP':'sum', 'FN':'sum', 'FP':'sum', 'TN':'sum'}).reset_index()
        # Calculate the ratios TP/TP+FP
        df_CSResults['TP/TP+FN'] = df_CSResults['TP']/ (df_CSResults['TP'] + df_CSResults['FN'])

        # Calculate the ratios FP/FP+TN
        df_CSResults['FP/FP+TN'] = df_CSResults['FP']/ (df_CSResults['TN'] + df_CSResults['FP'])

        # Plot the graph to compare the ratios
        self.plotBubbleChart1(df_CSResults, criterion, factor, outputFilename)


    def plotBubbleChart1(self, df, criterion, label_x, pathway):
        '''
            Bubblechart
            x: Number of sequences or number of other species in the family
            y: Ratio
            size: Number of TP+FN or TN+FP
        '''

        if label_x == 'number_of_sequences':
            label_x = 'sequences.'
        if label_x == 'familySize':
            label_x = 'familySize'

        df_CSResults1 = df.dropna(subset=['TP/TP+FN'])
        df_CSResults1['TP+FN'] = df_CSResults1['TP'] + df_CSResults1['FN']
        fig1 = px.scatter(df_CSResults1, x='binned', y='TP/TP+FN', size='TP+FN')
        fig1.update_traces(marker={'sizemin': 2})
        title1 = "{} : Sensitivity (the ratio TP/TP+FN) in terms of the number of {}".format(criterion, label_x)

        df_CSResults2 = df.dropna(subset=['FP/FP+TN'])
        df_CSResults2['TN+FP'] = df_CSResults2['TN'] + df_CSResults2['FP']
        fig2 = px.scatter(df_CSResults2, x='binned', y='FP/FP+TN', size='TN+FP')
        fig2.update_traces(marker={'sizemin': 2})
        title2 = "{} : FPR (the ratio FP/FP+TN) in terms of the number of {}".format(criterion, label_x)

        fig = make_subplots(rows=2, cols=1, shared_xaxes=False, subplot_titles=[title1, title2], 
                            x_title="Number of {}".format(label_x), y_title="Ratio")
        fig.add_trace(fig1['data'][0], row=1, col=1)
        fig.add_trace(fig2['data'][0], row=2, col=1)

        #fig.show()
        fig.write_html(pathway)


    def addTypeResultsWithoutBiblio(self):
        '''
            Add a label for the successes and the failures for the results without biblio.
        '''

        # Remove the species with a biblio
        self.df_withoutBiblio = self.df_CSResults[self.df_CSResults["CS_biblio"].isnull()]

        #  Results from test 0
        self.df_withoutBiblio.loc[(self.df_withoutBiblio['test0'] == True), 'Criterion 0 without biblio'] = "Success"
        self.df_withoutBiblio.loc[(self.df_withoutBiblio['test0'] == False), 'Criterion 0 without biblio'] = "Fail"

        #  Results from test 1
        self.df_withoutBiblio.loc[(self.df_withoutBiblio['test0'] == True) & (self.df_withoutBiblio['test1'] == True), 'Criterion 1 without biblio'] = "Success"
        self.df_withoutBiblio.loc[(self.df_withoutBiblio['test0'] == True) & (self.df_withoutBiblio['test1'] == False), 'Criterion 1 without biblio'] = "Fail"

        #  Results from test 2
        self.df_withoutBiblio.loc[(self.df_withoutBiblio['test0'] == True) & (self.df_withoutBiblio['test2'] == True), 'Criterion 2 without biblio'] = "Success"
        self.df_withoutBiblio.loc[(self.df_withoutBiblio['test0'] == True) & (self.df_withoutBiblio['test2'] == False), 'Criterion 2 without biblio'] = "Fail"


    def checkTestWithoutBiblio(self, criterion, factor):
        '''
            Retrieve and compare the successes and failures.
        '''

        # For each label, count the number of occurences according to the number of sequences
        df_CSResults_Success = self.df_withoutBiblio[self.df_withoutBiblio[criterion] == 'Success'].groupby(by=[factor]).agg({criterion : 'count'}).reset_index()
        df_CSResults_Fail = self.df_withoutBiblio[self.df_withoutBiblio[criterion] == 'Fail'].groupby(by=[factor]).agg({criterion : 'count'}).reset_index()
        df_CSResults_total = self.df_withoutBiblio.groupby(by=[factor]).agg({criterion : 'count'}).reset_index()

        # Rename the columns
        df_CSResults_Success = df_CSResults_Success.rename(columns={criterion: "Success"})
        df_CSResults_Fail = df_CSResults_Fail.rename(columns={criterion: "Fail"})

        # Merge all the tables into only one
        df_CSResults = df_CSResults_total.merge(df_CSResults_Success, how = 'outer')
        df_CSResults = df_CSResults.merge(df_CSResults_Fail, how = 'outer')

        # The max number of sequences or max number of other nominal species in the family
        max_nb_factor = max(df_CSResults[factor])

        # Cut the number of sequences into bins per 20 sequences
        bins = list(range(0, max_nb_factor, 10))
        labels = list(range(10, max_nb_factor, 10))
        df_CSResults['binned'] = pd.cut(df_CSResults[factor], bins, labels=labels)

        # Count the number of successes and failures in each bin
        df_CSResults = df_CSResults.groupby(by=['binned']).agg({'Success':'sum', 'Fail':'sum'}).reset_index()

        # Calculate the ratios Success/Success+Failures
        df_CSResults['Successes/Successes+Failures'] = df_CSResults['Success']/ (df_CSResults['Success'] + df_CSResults['Fail'])

        return df_CSResults


    def plotBubbleChart2(self, criterion, pathway):
        '''
            Plot the graph to compare the ratios.
            Bubblechart
            x: Number of sequences or number of other species in the family
            y: Ratio
            size: Number of Successes+Failures
        '''

        df_CSResults1 = self.checkTestWithoutBiblio(criterion, 'number_of_sequences')
        df_CSResults2 = self.checkTestWithoutBiblio(criterion, 'familySize')

        df_CSResults1 = df_CSResults1.dropna(subset=['Successes/Successes+Failures'])
        df_CSResults1['Successes+Failures'] = df_CSResults1['Success'] + df_CSResults1['Fail']
        fig1 = px.scatter(df_CSResults1, x='binned', y='Successes/Successes+Failures', size='Successes+Failures')
        fig1.update_traces(marker={'sizemin': 2})
        #fig1.update_traces(marker=dict(line=dict(width=1, color='black')), selector=dict(mode='markers'))
        title1 = "{} : Ratio Successes/Successes+Failures in terms of the number of sequences inside a nominal species.".format(criterion)

        df_CSResults2 = df_CSResults2.dropna(subset=['Successes/Successes+Failures'])
        df_CSResults2['Successes+Failures'] = df_CSResults2['Success'] + df_CSResults2['Fail']
        fig2 = px.scatter(df_CSResults2, x='binned', y='Successes/Successes+Failures', size='Successes+Failures')
        fig2.update_traces(marker={'sizemin': 2})
        title2 = "{} : Ratio Successes/Successes+Failures in terms of family size.".format(criterion)

        fig = make_subplots(rows=2, cols=1, shared_xaxes=False, subplot_titles=[title1, title2], y_title="Ratio")
        fig.add_trace(fig1['data'][0], row=1, col=1)
        fig.add_trace(fig2['data'][0], row=2, col=1)

        #fig.show()
        fig.write_html(pathway)


    def plotResults(self):
        '''
            Check the results
        '''

        #----------------------#
        #   Available biblio   #
        #----------------------#


        self.addTypeResultsBiblio()

        self.checkTestBiblio('Criterion 0', 'number_of_sequences', self.output_criterion0_nbSeq_biblio)
        self.checkTestBiblio('Criterion 0', 'familySize', self.output_criterion0_nbSpecies_biblio)
        self.checkTestBiblio('Criterion 1', 'number_of_sequences', self.output_criterion1_nbSeq_biblio)
        self.checkTestBiblio('Criterion 1', 'familySize', self.output_criterion1_nbSpecies_biblio)
        self.checkTestBiblio('Criterion 2', 'number_of_sequences', self.output_criterion2_nbSeq_biblio)
        self.checkTestBiblio('Criterion 2', 'familySize', self.output_criterion2_nbSpecies_biblio)

        #--------------------------#
        #   Not available biblio   #
        #--------------------------#       

        self.addTypeResultsWithoutBiblio()

        self.plotBubbleChart2('Criterion 0 without biblio', self.output_criterion0_withoutBiblio)
        self.plotBubbleChart2('Criterion 1 without biblio', self.output_criterion1_withoutBiblio)
        self.plotBubbleChart2('Criterion 2 without biblio', self.output_criterion2_withoutBiblio)  