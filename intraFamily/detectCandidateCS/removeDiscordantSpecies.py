#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np

class concordantSpecies:
    '''
        A concordant BIN is defined as a BIN not sharing by several species.
    '''

    def __init__(self):

        # Input files
        # Species their BINs and their grades
        self.input_filenameGrades = "../../../Output/corrections/detectDiscordantSpecies/grades.csv"
        # All nominal species and complete taxonomic informations
        self.input_filenameSpecies_taxonomicRank = "../../../Output/descriptiveStatistics/all_nominalSpecies.csv"

        # Structures to contain data
        self.df_filenameGrades = pd.DataFrame()
        self.df_taxonomicRanks = pd.DataFrame()
        self.df_concordance = pd.DataFrame()
        self.discordantSpecies = []
        self.list_candidateCS_criterion0 = []
        self.list_candidateCS_criterion0_update = []
        self.list_candidateCS_havingLost_criterion0 = []
        self.list_candidateCS_criterion0_removed = []

        # Output file
        self.output_filename_withoutBiblio = "../../../Output/detectCandidateCS/concordantSpecies/detectCandidateCS_withoutBiblio_concordantSpecies.csv"

        # Methods
        self.loadData()
        self.getGradeE()
        self.getDiscordantSpecies()
        self.getSpeciesLosingCriterion0()


    def loadData(self):
        '''
            Load the species and their BINs
        '''

        # Load the species, their BINs and their grade
        self.df_filenameGrades = pd.read_csv(self.input_filenameGrades, sep="\t")

        # Retrieve the taxonomic ranks of the species
        self.df_taxonomicRanks = pd.read_csv(self.input_filenameSpecies_taxonomicRank, sep="\t")


    def getGradeE(self):
        '''
            The BINs and their species, without the BIN with grade E
        '''
        self.df_gradeE = self.df_filenameGrades[self.df_filenameGrades.grade.str.contains("E")]
        self.df_withoutE = self.df_filenameGrades[~self.df_filenameGrades.grade.str.contains("E")]


    def getDiscordantSpecies(self):
        '''
            Remove the discordant BINs
        '''
        self.discordantSpecies = self.df_gradeE["bin_uri"].unique().tolist()


    def getSpeciesCriterion0(self, df):
        '''
            Removing some BINs, some criterion 0 species loose their criterion 0
            @param df: a dataframe where the criterion 0 species having several lines
        '''
        # Species with 1 BIN
        species_uniqueBIN = df['species_name'].drop_duplicates(keep=False)
        # All species without duplicata
        species_unique = df['species_name'].drop_duplicates(keep='first')
        # Species with > 1 BIN
        species_multipleBIN = pd.concat([species_unique, species_uniqueBIN]).drop_duplicates(keep=False)
        # Criterion 0 species
        return species_multipleBIN.unique().tolist()


    def getSpeciesLosingCriterion0(self):
        '''
        '''

        # All criterion 0 species
        self.list_candidateCS_criterion0 = self.getSpeciesCriterion0(self.df_filenameGrades)

        # Species the update criterion 0 species after removing the BINs with a grade E
        self.list_candidateCS_criterion0_update = self.getSpeciesCriterion0(self.df_withoutE)

        # Fetch the species having lost their criterion 0
        self.list_candidateCS_havingLost_criterion0 = list(set(self.list_candidateCS_criterion0) - set(self.list_candidateCS_criterion0_update))
        # Retrieve the column grade
        self.df_candidateCS_havingLosed_criterion0 = self.df_filenameGrades[self.df_filenameGrades["species_name"].isin(self.list_candidateCS_havingLost_criterion0)]

        # Among the species having lost their criterion 0, some species have lost all their BINs:
        # 1. Fetch the species having lost their criterion 0 but still with BINs
        self.df_candidateCS_havingLosed_criterion0_stillBINs = self.df_candidateCS_havingLosed_criterion0[~self.df_candidateCS_havingLosed_criterion0.grade.str.contains("E")]
        self.list_candidateCS_havingLost_criterion0_stillBINs = list(self.df_candidateCS_havingLosed_criterion0_stillBINs.species_name)
        # 2. Fetch the species having lost all their BINs
        self.list_candidateCS_criterion0_removed = list(set(self.list_candidateCS_havingLost_criterion0) - set(self.list_candidateCS_havingLost_criterion0_stillBINs))


    def detectCS(self):
        '''
            Concern the species having successed the criterion 0 i.e. having 2 or more BINs
            Some criterion 0 species loose their criterion 0
            And some comparisons have been loosing because of the delete of some BINs
        '''

        # Structure to contain data
        all_rows = []

        for current_candidateCS in self.list_candidateCS_criterion0_update:
        
            # Test 0 is approved, the others not yet
            test0 = True
            test1 = False
            test2 = False

            # Fetch the informations about the taxonomic rank for the current nominal species
            df_taxonomicRank = self.df_taxonomicRanks[self.df_taxonomicRanks.species_name == current_candidateCS]
            phylum_name = df_taxonomicRank.phylum_name.iloc[0]
            class_name = df_taxonomicRank.class_name.iloc[0]
            order_name = df_taxonomicRank.order_name.iloc[0]
            family_name = df_taxonomicRank.family_name.iloc[0]
            genus_name = df_taxonomicRank.genus_name.iloc[0]

            # Fetch all p-distance comparisons
            input_filenameComparisonsPdist = "../../../Output/intraFamily/{}/{}/comparisonsPdist.csv".format(phylum_name, family_name)

            if not os.path.exists(input_filenameComparisonsPdist):
                
                # The result is not already runned
                test1 = None
                test2 = None
                
                # Save the species result
                row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, test0, test1, test2]
                all_rows.append(row)

            else:

                if os.path.exists(input_filenameComparisonsPdist):

                    # Read the input file
                    df_myFamilyComparisonsPdist = pd.read_csv(input_filenameComparisonsPdist, sep='\t')

                    if df_myFamilyComparisonsPdist.empty:

                        # Not enough other species in the family to test the criteria 1 and 2
                        test1 = None
                        test2 = None

                        # Save the species result
                        row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, test0, test1, test2]
                        all_rows.append(row)

                    else:

                        # Select the result inside the family for only the current species
                        df_one_candidateCS_criterion0_compPdist = df_myFamilyComparisonsPdist[df_myFamilyComparisonsPdist.candidateCS_test0 == current_candidateCS]

                        # After fitting and cutting the sequences, some sequences have been removed and so some nominal species have lost BINs and have lost the criterion 0
                        if df_one_candidateCS_criterion0_compPdist.empty:
                            test1 = None
                            test2 = None

                        else:
                            
                            # Update the comparisons of pdistances removing the BIN with a grade E
                            bins_uri = list(set(df_one_candidateCS_criterion0_compPdist[["bin_uri1", "bin_uri2"]].values.ravel().tolist()))
                            for bin_uri in bins_uri:
                                if bin_uri in self.discordantSpecies:
                                    df_one_candidateCS_criterion0_compPdist = df_one_candidateCS_criterion0_compPdist[(df_one_candidateCS_criterion0_compPdist.bin_uri1 != bin_uri) & (df_one_candidateCS_criterion0_compPdist.bin_uri2 != bin_uri)]

                            #--------------------#
                            #     Criterion 1    #
                            #--------------------#

                            ratios_criterion1 = df_one_candidateCS_criterion0_compPdist["ratio_test1"].to_list() # Fetch all ratio results

                            for ratio in ratios_criterion1:
                                if ratio > 1:
                                    test1 = True

                            #--------------------#
                            #     Criterion 2    #
                            #--------------------#

                            ratios_criterion2 = df_one_candidateCS_criterion0_compPdist["ratio_test2"].to_list() # Fetch all ratio results

                            # Count the number of floats
                            nb_floats = 0
                            # Count the number of ratio < to 1
                            ratioInf1 = 0
                            
                            for ratio in ratios_criterion2:
                                if isinstance(ratio, float): # If all values inside the column contain a float value
                                    nb_floats+= 1
                                    if float(ratio) < 1:
                                        test2 = True
                                elif ratio.find("A") == -1 and ratio.find("B") == -1 and ratio.find("C") == -1 and ratio.find("D") == -1 and ratio.find("E") == -1 and ratio != None and ratio != "":
                                    nb_floats+= 1
                                    if float(ratio) < 1:
                                        test2 = True

                            if nb_floats == 0:
                                test2 = None # If no ratio has been calculated for a nominal species, don't consider this nominal species

                        # Save the species results
                        row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS, test0, test1, test2]
                        all_rows.append(row)


        ########################################################################################################


        for current_candidateCS_havingLost_criterion0_stillBINs in self.list_candidateCS_havingLost_criterion0_stillBINs:

            # Fetch the informations about the taxonomic rank for the current nominal species
            df_taxonomicRank = self.df_taxonomicRanks[self.df_taxonomicRanks.species_name == current_candidateCS_havingLost_criterion0_stillBINs]
            phylum_name = df_taxonomicRank.phylum_name.iloc[0]
            class_name = df_taxonomicRank.class_name.iloc[0]
            order_name = df_taxonomicRank.order_name.iloc[0]
            family_name = df_taxonomicRank.family_name.iloc[0]
            genus_name = df_taxonomicRank.genus_name.iloc[0]
            
            # Save the species result
            row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS_havingLost_criterion0_stillBINs, False, None, None]
            all_rows.append(row)


        ########################################################################################################


        for current_candidateCS_criterion0_removed in self.list_candidateCS_criterion0_removed:

            # Fetch the informations about the taxonomic rank for the current nominal species
            df_taxonomicRank = self.df_taxonomicRanks[self.df_taxonomicRanks.species_name == current_candidateCS_criterion0_removed]
            phylum_name = df_taxonomicRank.phylum_name.iloc[0]
            class_name = df_taxonomicRank.class_name.iloc[0]
            order_name = df_taxonomicRank.order_name.iloc[0]
            family_name = df_taxonomicRank.family_name.iloc[0]
            genus_name = df_taxonomicRank.genus_name.iloc[0]
            
            # Save the species result
            row = [phylum_name, class_name, order_name, family_name, genus_name, current_candidateCS_criterion0_removed, None, None, None]
            all_rows.append(row)


        #-----------------------------------#
        #          Save the data            #
        #-----------------------------------#

        # Convert to panda format
        all_rows = [x for x in all_rows if x != []]
        df_output = pd.DataFrame(all_rows, columns=['phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name', 'test0', 'test1', 'test2'])
        
        # Import into CSV file
        df_output.to_csv(self.output_filename_withoutBiblio, sep = "\t", index=False)