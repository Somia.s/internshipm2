#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
from sklearn.metrics import confusion_matrix


class CSresults_thresholds():

    def __init__(self):

        # Input
        self.input_filename = "../../../Output/detectCandidateCS/detectCandidateCS.csv"

        # Struture to contain data
        self.df_CSresults = pd.DataFrame()
        
        # Output
        self.output_filename = "../../../Output/detectCandidateCS/ratioThresholds/detectCandidateCS_thresholds.csv"
        self.output_filenameCompareThresholds = "../../../Output/detectCandidateCS/ratioThresholds/compareThresholds.txt"
        

    def getCriterion0SpeciesWithBiblio(self):
        '''
            Retrieve criterion 0 nominal species (i.e. containing several BINs) and with bibliography
        '''

        # Fetch CS results
        self.df_CSresults = pd.read_csv(self.input_filename, sep='\t')

        # Keep only results at family level
        self.df_CSresults = self.df_CSresults[self.df_CSresults.genusLevel == False]

        # Select criterion 0 nominal species with bibliography
        self.df_CSresults = self.df_CSresults[['phylum_name', 'family_name', 'genus_name', 'species_name', 'test0', 'test1', 'CS_biblio']]

        # Drop nominal species without reference
        self.df_CSresults = self.df_CSresults.dropna(subset=['CS_biblio'])

        # Select nominal species that have successed criterion 0
        self.df_CSresults = self.df_CSresults[self.df_CSresults.test0 == True]

        # Replace 'True' value by 1 and 'False' value by 0, in the CS_biblio column
        self.df_CSresults['CS_biblio'] = self.df_CSresults['CS_biblio'].replace([True], 1)
        self.df_CSresults['CS_biblio'] = self.df_CSresults['CS_biblio'].replace([False], 0)


    def calculateProportions(self):
        '''
            Calculate proportions
        '''

        for species_name in self.df_CSresults.species_name:

            phylum_name = self.df_CSresults[self.df_CSresults.species_name == species_name].phylum_name.iloc[0]
            family_name = self.df_CSresults[self.df_CSresults.species_name == species_name].family_name.iloc[0]

            filenameComparisonsPdist = "../../../Output/intraFamily/{}/{}/comparisonsPdist.csv".format(phylum_name, family_name)

            if os.path.exists(filenameComparisonsPdist):

                df_comparisonsPdist = pd.read_csv(filenameComparisonsPdist, sep='\t')

                df_comparisonsPdist = df_comparisonsPdist[df_comparisonsPdist.candidateCS_test0 == species_name]

                count_ratioSup1 = df_comparisonsPdist[df_comparisonsPdist['ratio_test1'] > 1].count().iloc[0]

                proportion = count_ratioSup1/len(df_comparisonsPdist)

                self.df_CSresults.loc[self.df_CSresults.species_name == species_name, 'proportion'] = proportion

        self.df_CSresults = self.df_CSresults.dropna(subset=['proportion'])
        

    def addThresholds(self):
        '''
            Add thresholds
        '''
    
        for species_name in self.df_CSresults.species_name:

            self.df_CSresults['threshold_1/20'] = self.df_CSresults['proportion'].apply(lambda x: 1 if x > 1/20 else 0)
            self.df_CSresults['threshold_1/10'] = self.df_CSresults['proportion'].apply(lambda x: 1 if x > 1/10 else 0)
            self.df_CSresults['threshold_1/5'] = self.df_CSresults['proportion'].apply(lambda x: 1 if x > 1/5 else 0)
            self.df_CSresults['threshold_1/2'] = self.df_CSresults['proportion'].apply(lambda x: 1 if x > 1/2 else 0)


    def compareThresholds(self):
        '''	
            Compare thresholds
        '''

        #self.getCriterion0SpeciesWithBiblio()
        #self.calculateProportions()
        #self.addThresholds()

        # Save the output in a CSV file
        #self.df_CSresults.to_csv(self.output_filename, sep='\t', index=False)

        self.df_CSresults = pd.read_csv(self.output_filename, sep='\t')

        f = open(self.output_filenameCompareThresholds, 'w')

        for threshold in ["1/20", "1/10", "1/5", "1/2"]:
            
            f.write("Threshold = {}\n\n".format(threshold))

            f.write("Confusion matrix:\n")

            y_pred = self.df_CSresults['threshold_{}'.format(threshold)].to_list() # Prediction
            y_real = self.df_CSresults['CS_biblio'].to_list() # Actual

            cmtx = pd.DataFrame(confusion_matrix(y_real, y_pred, 
                                    labels=[True, False]), 
                                    index=['Biblio:TrueCS', 'Biblio:FalseCS'], # Real data
                                    columns=['BOLD:TrueCS', 'BOLD:FalseCS']) # Prediction data
            
            cmtx["Total"] = cmtx.sum(axis=1)
            ratio_l1 = cmtx['BOLD:TrueCS']['Biblio:TrueCS']/cmtx['Total']['Biblio:TrueCS']
            ratio_l2 = cmtx['BOLD:TrueCS']['Biblio:FalseCS']/cmtx['Total']['Biblio:FalseCS']
            cmtx["Ratio1"] = [ratio_l1, ratio_l2]

            f.write(str(cmtx))

            f.write("\n\n*******************************************************************************\n\n")

        f.close()

