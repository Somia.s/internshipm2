#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from detectCandidateCS import *
from addBiblio import *
from plot import *
from addThresholds import *
from removeDiscordantSpecies import *

def main():
    
    # Create directories for results
    createDirectory("../../../Output")
    createDirectory("../../../Output/detectCandidateCS")
    createDirectory("../../../Output/detectCandidateCS/test0")
    createDirectory("../../../Output/detectCandidateCS/test1")
    createDirectory("../../../Output/detectCandidateCS/test2")
    createDirectory("../../../Output/detectCandidateCS/ratioThresholds")
    createDirectory("../../../Output/detectCandidateCS/speciesThresholds")
    createDirectory("../../../Output/detectCandidateCS/siblingSisters")
    createDirectory("../../../Output/detectCandidateCS/concordantSpecies")


    # Analyse each species
    all_species = all_Species()

    # Detect species that have failed the criterion 0
    all_species.detectSpeciesFailedCriterion0()

    #---------------------------------#
    #     All comparisons per pair    #
    #        Without thresholds       #
    #---------------------------------#

    # Detect CS without considering only the sibling sisters
    all_species.detectCS(False, None)
    
    # Add biblio
    # @Params: input filename and isSiblingSisters (True or False)
    CS_results = CSresults("../../../Output/detectCandidateCS/detectCandidateCS_withoutBiblio.csv", False)
    CS_results.add_CSbiblio()
    
    # Confusion matrix
    CS_results.confusionMatrix(False, False)

    # Parameter 1 = number of sequences and Parameter 2 = family size
    CS_results.confusionMatrix(10, 30)
    CS_results.confusionMatrix(20, 40)


    #------------------------#
    #     Sibling sisters    #
    #------------------------#

    # Detect CS considering only sibling sisters
    all_species.detectCS(True, 0.05) # Threshold 5%
    all_species.detectCS(True, 0.1) # Threshold 10%
    all_species.detectCS(True, 0.25) # Threshold 25%
    all_species.detectCS(True, 0.5) # Threshold 50%

    # Add biblio
    # @Params: input filename and isSiblingSisters (True or False)
    CS_results_005 = CSresults("../../../Output/detectCandidateCS/siblingSisters/detectCandidateCS_withoutBiblio_siblingSisters_0.05.csv", True)
    CS_results_01 = CSresults("../../../Output/detectCandidateCS/siblingSisters/detectCandidateCS_withoutBiblio_siblingSisters_0.1.csv", True)
    CS_results_025 = CSresults("../../../Output/detectCandidateCS/siblingSisters/detectCandidateCS_withoutBiblio_siblingSisters_0.25.csv", True)
    CS_results_05 = CSresults("../../../Output/detectCandidateCS/siblingSisters/detectCandidateCS_withoutBiblio_siblingSisters_0.5.csv", True)

    CS_results_005.add_CSbiblio()
    CS_results_01.add_CSbiblio()
    CS_results_025.add_CSbiblio()
    CS_results_05.add_CSbiblio()

    # Confusion matrix
    CS_results_005.confusionMatrix(False, False)
    CS_results_01.confusionMatrix(False, False)
    CS_results_025.confusionMatrix(False, False)
    CS_results_05.confusionMatrix(False, False)


    #---------------------------#
    #     Concordant Species    #
    #---------------------------#

    concordant_Species = concordantSpecies()
    concordant_Species.detectCS()
    CS_results_concordantSpecies = CSresults("../../../Output/detectCandidateCS/concordantSpecies/detectCandidateCS_withoutBiblio_concordantSpecies.csv", False)
    CS_results_concordantSpecies.add_CSbiblio()
    
    CS_results_concordantSpecies.confusionMatrix(False, False)

    #------------------------#
    #    Plot the results    #
    #------------------------#
    
    CS_results = CS_Results()
    CS_results.plotResults()

    #------------------------#
    #      Add thresholds    #
    #------------------------#
    
    CSresults_Thresholds = CSresults_thresholds()
    CSresults_Thresholds.compareThresholds()


main()
