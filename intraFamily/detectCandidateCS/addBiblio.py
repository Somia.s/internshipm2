#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import re
from sklearn.metrics import confusion_matrix

class CSresults():

    def __init__(self, input_filenameCSresults, siblingSisters):

        # Inputs
        self.input_filenameCSresults = input_filenameCSresults
        self.input_filenameCSresults_speciesFailedCriterion0 = "../../../Output/detectCandidateCS/detectSpecies_withoutBiblio_failedCriterion0.csv"
        self.input_filenameCSBiblio = "../../../Data/CS_biblio/185354woParasites_CSbiblio.csv"

        # Strutures to contain data
        self.df_finalOutput = pd.DataFrame()
        self.nb_phylum = None
        self.nb_genus = None
        self.nb_families = None
        self.nb_nominal_species = None
        self.siblingSisters = siblingSisters
        
        # Outputs
        self.output_filename = input_filenameCSresults.replace("_withoutBiblio", "")
        

    def get_testResults(self):
        '''
            Load the data: the results of tests
        '''

        df_CSresults = pd.read_csv(self.input_filenameCSresults, sep="\t")
        df_CSresults_speciesFailedCriterion0 = pd.read_csv(self.input_filenameCSresults_speciesFailedCriterion0, sep="\t")
        df_CSresults = pd.concat([df_CSresults, df_CSresults_speciesFailedCriterion0])

        return df_CSresults


    def get_CS_biblio(self):
        '''
            Retrieve the species from the Biblio
        '''

        df_CS_biblio = pd.read_csv(self.input_filenameCSBiblio, sep=";")
        return df_CS_biblio


    def add_CSbiblio(self):
        '''
            Add the informations from bibliography of cryptic species.
            In order to compare the results from the python pipeline and the results from a bibliography.
            This method creates and fills the "CS_biblio" column
        '''

        # Fetch the results from test
        df_CSresults = self.get_testResults()

        # Create the column "CS_biblio" to contain the result from the biblio, fill None value by default
        df_CSresults["CS_biblio"] = None

        # Fetch the Cryptic Species References
        df_CS_biblio = self.get_CS_biblio()

        # Merge the columns phylum and species name, in order to consider the phylum for matching the species names
        df_CSresults["Pspecies_name"] = df_CSresults["phylum_name"] + " " + df_CSresults["species_name"]
        df_CS_biblio["PSimplified.name"] = df_CS_biblio["phylum"] + " " + df_CS_biblio["Simplified.name"]
        df_CS_biblio["PscientificName"] =  df_CS_biblio["phylum"] + " " + df_CS_biblio["scientificName"]

        # Fetch all nominal species of the study
        all_nominalSpecies = df_CSresults.species_name

        # Retrieve all species names with their phylum name from biblio
        mapping1 = dict(df_CS_biblio[['PSimplified.name', 'CS_biblio']].values)
        mapping2 = dict(df_CS_biblio[['PscientificName', 'CS_biblio']].values)

        # Correction of the name for some species names
        mapping_tmp = {**mapping1, **mapping2}
        mapping = {**mapping1, **mapping2}
        for species_name in mapping_tmp.keys():
            if re.search("i$", species_name) != None:
                mapping[species_name+"i"] = mapping[species_name]
            if re.search("ii$", species_name) != None:
                mapping[species_name[0:len(species_name)-1]] = mapping[species_name]

        # Match the biblio and results from BOLD according to the species names in common
        df_CSresults['CS_biblio'] = df_CSresults.Pspecies_name.map(mapping)
        
        # Sort the dataframe according to the taxonomic rank columns
        df_CSresults = df_CSresults.sort_values(by=['phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name'])

        # Import into CSV file
        df_CSresults.to_csv(self.output_filename, columns=['phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name', 'number_of_sequences', 'number_otherSpecies_inFamily', 'test0', 'test1', 'test2', 'CS_biblio', 'genusLevel'], sep = "\t", index=False)


    def countStatistic(self, df):

        self.nb_phylum = df["phylum_name"].nunique()
        self.nb_genus = df["genus_name"].nunique()
        self.nb_families = df["family_name"].nunique()
        self.nb_nominal_species = df["species_name"].nunique()


    def confusionMatrix(self, min_seq, min_familySize):
        '''
            Construct a confusion matrix to see the number of true positives, false positives, true negatives and false negatives, 
            comparing the biblio and the result from the BIN BOLD.
            Parameter min_seq: the minimum number of sequences that the species must have
            Parameter min_familySize: the minimum family size
        '''
    
        # Load the CS results
        df_CSresults = pd.read_csv(self.output_filename, sep="\t")

        # Remove the nominal species without informations about CS biblio
        df_CSresults = df_CSresults.dropna(subset = ["CS_biblio"])

        # Filter the CS results
        if min_seq != False:
            df_CSresults = df_CSresults[df_CSresults.number_of_sequences >= min_seq]
            outputConfusionMatrix_filename = "../../../Output/detectCandidateCS/confusionMatrix.txt"

        if min_familySize != False:
            df_CSresults = df_CSresults[df_CSresults.number_otherSpecies_inFamily >= min_familySize]

        # According to the parameter values, change the filename
        outputConfusionMatrix_filename = "../../../Output/detectCandidateCS/speciesThresholds/confusionMatrix_{}seq_{}familySize.txt".format(min_seq, min_familySize)

        if min_seq == False and min_familySize == False:
            outputConfusionMatrix_filename = "../../../Output/detectCandidateCS/confusionMatrix.txt"

        if self.output_filename == "../../../Output/detectCandidateCS/concordantSpecies/detectCandidateCS_concordantSpecies.csv":
            outputConfusionMatrix_filename = "../../../Output/detectCandidateCS/concordantSpecies/confusionMatrix_concordantSpecies.txt"

        # Confusion matrix with the sibling sisters
        if self.siblingSisters == True:
            outputConfusionMatrix_filename = self.input_filenameCSresults.replace("detectCandidateCS_withoutBiblio", "confusionMatrix")
            outputConfusionMatrix_filename = outputConfusionMatrix_filename.replace("csv", "txt")

        # Open the output file
        f = open(outputConfusionMatrix_filename, "w")

        # Results from the family level
        df_CSresults_familyLevel = df_CSresults[df_CSresults.genusLevel != True]
        
        # Results from the genus level
        df_CSresults_genusLevel = df_CSresults[df_CSresults.genusLevel == True]

        # The prediction
        #for test in ["test0", "test1", "test2", "test1 Genus Level", "test2 Genus Level"]:
        for test in ["test0", "test1", "test2"]:

            f.write("Confusion matrix: {}\n\n".format(test))

            if test == "test0":
                
                df_CSresults_familyLevel = df_CSresults_familyLevel.dropna(subset = ["test0"]) # in the case of discordant BINs, some test 0 are removed

                y_pred = df_CSresults_familyLevel[test].to_list()
                y_real = df_CSresults_familyLevel["CS_biblio"].to_list() # The real CS biblio

                self.countStatistic(df_CSresults_familyLevel)

            if test == "test1":
                # Remove the real CS having failed the test 0
                df_CSresults_familyLevel.loc[(df_CSresults_familyLevel['test0'] == False), 'test0'] = np.nan
                df_CSresults_familyLevel = df_CSresults_familyLevel.dropna(subset = ["test0"])
                df_CSresults_familyLevel2 = df_CSresults_familyLevel.dropna(subset = ["test1"])
                y_real = df_CSresults_familyLevel2["CS_biblio"].to_list()
                y_pred = df_CSresults_familyLevel2[test].to_list()

                self.countStatistic(df_CSresults_familyLevel)

            if test == "test2":

                df_CSresults_familyLevel3 = df_CSresults_familyLevel.dropna(subset = ["test2"])
                y_real = df_CSresults_familyLevel3["CS_biblio"].to_list()
                y_pred = df_CSresults_familyLevel3[test].to_list()

                self.countStatistic(df_CSresults_familyLevel3)

            '''
            if test == "test1 Genus Level":
                df_CSresults_genusLevel.loc[(df_CSresults_genusLevel['test0'] == False), 'test0'] = np.nan
                df_CSresults_genusLevel = df_CSresults_genusLevel.dropna(subset = ["test0"])
                df_CSresults_genusLevel2 = df_CSresults_genusLevel.dropna(subset = ["test1"])
                y_pred = df_CSresults_genusLevel2["test1"].to_list()
                y_real = df_CSresults_genusLevel2["CS_biblio"].to_list() # The real CS biblio

                self.countStatistic(df_CSresults_genusLevel2)

            if test == "test2 Genus Level":
                df_CSresults_genusLevel3 = df_CSresults_genusLevel.dropna(subset = ["test2"])
                y_pred = df_CSresults_genusLevel3["test2"].to_list()
                y_real = df_CSresults_genusLevel3["CS_biblio"].to_list() # The real CS biblio

                self.countStatistic(df_CSresults_genusLevel3)
            '''

            cmtx = pd.DataFrame(confusion_matrix(y_real, y_pred, 
                                    labels=[True, False]), 
                                    index=['Biblio:TrueCS', 'Biblio:FalseCS'], # Real data
                                    columns=['BOLD:TrueCS', 'BOLD:FalseCS']) # Prediction data
            
            cmtx["Total"] = cmtx.sum(axis=1)
            ratio_l1 = cmtx['BOLD:TrueCS']['Biblio:TrueCS']/cmtx['Total']['Biblio:TrueCS']
            ratio_l2 = cmtx['BOLD:TrueCS']['Biblio:FalseCS']/cmtx['Total']['Biblio:FalseCS']
            cmtx["Ratio1"] = [ratio_l1, ratio_l2]

            if test == "test0":
                FN = cmtx['BOLD:FalseCS']['Biblio:TrueCS']
                TN = cmtx['BOLD:FalseCS']['Biblio:FalseCS']
            
            if test == "test1" or test == "test2":
                ratio_l3 = cmtx['BOLD:TrueCS']['Biblio:TrueCS']/(cmtx['Total']['Biblio:TrueCS'] + FN)
                ratio_l4 = cmtx['BOLD:TrueCS']['Biblio:FalseCS']/(cmtx['Total']['Biblio:FalseCS'] + TN)
                cmtx["Ratio2"] = [ratio_l3, ratio_l4]

            f.write(str(cmtx))
            f.write("\n\nNumber of phylum: {} / Number of genus: {} / Number of families: {} / Number of nominal species: {}".format(self.nb_phylum, self.nb_genus, self.nb_families, self.nb_nominal_species))
            f.write('\n\n************************************************************\n\n')
        
        f.close()
