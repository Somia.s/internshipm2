#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from MSAperFamily import *

def main():

    print("----- START MSA PER FAMILY -----\n")

    # Create directories for results
    createDirectory("../../../Output")
    createDirectory("../../../Output/intraFamily")

    # Retrieve all phylum inside the sequence folders
    list_phylum = retrieveDirectories("../../../Data/sequences/corrected_and_fitted_Sequences")

    #-----------------------#
    #    For each phylum    #
    #-----------------------# 
    
    for phylum_name in list_phylum:

        phylum_name = phylum_name.replace(".fas", "")

        folderPhylum_name = "../../../Output/intraFamily/{}".format(phylum_name)

        # Create an output folder for one phylum
        createDirectory(folderPhylum_name)
            
        #----------------------#
        #    For each family   #
        #----------------------#

        # Retrieve the families
        list_families = retrieveFilenames("../../../Data/sequences/corrected_and_fitted_Sequences/{}".format(phylum_name))

        
        for family_name in list_families:

            family_name = family_name.replace(".fas", "")

            folderFamily_name = "../../../Output/intraFamily/{}/{}".format(phylum_name, family_name)
            createDirectory(folderFamily_name)
            
            family_filename = "../../../Output/intraFamily/{}/{}/MSA.fas".format(phylum_name, family_name)

            filesize = None

            if os.path.exists(family_filename):
                filesize = os.path.getsize(family_filename)

            if not os.path.exists(family_filename) or filesize == 0:

                print("\n*********************************")
                print("\nPHYLUM:", phylum_name)
                print("FAMILY:\n", family_name)
                print(phylum_name, family_name, "Taille = ", i)

                # Create a family
                family = Family(phylum_name, family_name)

                # Name input and output filenames, according to the currently family
                family.filePathways()

                # Execute mafft for one family
                family.executeMafft()

    print("----- END MSA PER FAMILY -----\n")

main()
