#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

class Family:

    def __init__(self, phylum_name, family_name):

        self.phylum_name = phylum_name
        self.family_name = family_name

        # Input
        self.input_filenameFASTA = ""

        # Ouput
        self.output_filenameFASTA = ""


    def filePathways(self):
        '''
            Pathway of files
        '''

        self.input_filenameFASTA = "../../../Data/sequences/corrected_and_fitted_Sequences/{}/{}.fas".format(self.phylum_name, self.family_name)
        self.output_filenameFASTA = "../../../Output/intraFamily/{}/{}/MSA.fas".format(self.phylum_name, self.family_name)


    def executeMafft(self):

        command = "mafft --auto {} > {}".format(self.input_filenameFASTA, self.output_filenameFASTA)
        os.system(command)
