use warnings;
use strict;
use subprogramPerlEM;
use Data::Dumper;

# redirect stdout to save wget msg!!!!
# perl download_BOLD_API.pl > ../Log/download_BOLD_API.log 2>&1

# Takes a list of taxa
# Downloads all data if processID has COI-5P sequnece

# Drawbacks to correct by a suppmelentrary script (extract_BOLD_colums)
	#If invalid Taxon name, a download of random sequneces starts, then stop with xml format lines at the end of the file
	#If a processID has at least one COI sequence, all sequences are downloaded, not just the COI
	#Occasional errors dowload the same line more than once
	#All taxon with the same name is downloaded (Plecotera, genus, Plecoptera Order)


my $taxon_list = '../../Data/download_BOLD/input_list/BOLD_download_taxon_list.txt';
my $outdir = '../../Data/download_BOLD/Downloaded/';

open(IN, $taxon_list) or die "Cannot open $taxon_list\n";
my @taxa = <IN>;
close IN;

foreach my $taxon (@taxa)
{
	$taxon =~ s/\s*$//;
	print "START: $taxon\n";
	my $out = $taxon;
	$out =~ s/[^a-z0-9_]/_/gi;
	$out = $outdir.$out.'.csv';
	my $cmd = 'wget -O '.$out.' "http://www.boldsystems.org/index.php/API_Public/combined?taxon='.$taxon.'&marker=COI-5P&format=tsv"';
#	print $cmd, "\n";
	system $cmd;
	print "END: $taxon\n\n";
}
exit;



#http://www.boldsystems.org/index.php/API_Public/sequence?taxon=Chordata&geo=Florida&institutions=Smithsonian%20Institution
#>CDUSM032-05|Thalasseus sandvicensis|COI-5P|DQ433215
#NNNNNNNNNNNNNTTCGGCGCATGAGCTGGTATAGTAGGTACTGCCCTTAGCCTACTTATTCGTGCAGAACTAGGTCAACCAGGAACCCTTCTAGGAGACGACCAAATCTACAACGTAATCGTCACCGCCCATGCCTTTGTAATAATCTTCTTCATAGTAATACCTATCATAATTGGGGGCTTCGGAAACTGATTAGTCCCACTTATAATTGGTGCTCCCGACATGGCATTCCCACGTATGAACAACATAAGCTTCTGACTACTCCCCCCATCATTCTTACTTCTCCTAGCCTCCTCTACAGTAGAAGCTGGGGCAGGCACAGGATGAACCGTGTACCCTCCCCTAGCCGGTAATCTAGCCCATGCCGGAGCTTCAGTGGATTTAGCAATCTTCTCCCTCCATCTAGCAGGTGTATCCTCTATCCTTGGTGCTATCAACTTTATCACCACAGCTATCAACATAAAACCCCCCGCCCTTTCACAATACCAAACTCCTCTATTTGTATGATCCGTACTTATCACTGCCGTTCTACTATTACTCTCACTCCCAGTACTCGCCGCCGGTATCACTATGTTGTTAACAGACCGAAACCTAAACACAACGTTCTTTGATCCTGCTGGAGGNNNNNNNNNNNNNNNNNNNN--------------------------------------------------

#http://www.boldsystems.org/index.php/API_Public/combined?taxon=Mammalia&geo=Canada&format=tsv
#processid	sampleid	recordID	catalognum	fieldnum	institution_storing	collection_code	bin_uri	phylum_taxID	phylum_name	class_taxID	class_name	order_taxID	order_name	family_taxID	family_name	subfamily_taxID	subfamily_name	genus_taxID	genus_name	species_taxID	species_name	subspecies_taxID	subspecies_name	identification_provided_by	identification_method	identification_reference	tax_note	voucher_status	tissue_type	collection_event_id	collectors	collectiondate_start	collectiondate_end	collectiontime	collection_note	site_code	sampling_protocol	lifestage	sex	reproduction	habitat	associated_specimens	associated_taxa	extrainfo	notes	lat	lon	coord_source	coord_accuracy	elev	depth	elev_accuracy	depth_accuracy	country	province_state	region	sector	exactsite	image_ids	image_urls	media_descriptors	captions	copyright_holders	copyright_years	copyright_licenses	copyright_institutions	photographers	sequenceID	markercode	genbank_accession	nucleotides	trace_ids	trace_names	trace_links	run_dates	sequencing_centers	directions	seq_primers	marker_codes

#http://www.boldsystems.org/index.php/API_Public/combined?taxon=Lepidoptera&taxon=Plecoptera&marker=COI-5P&format=tsv
# Do not handle correctly taxon=Lepidoptera&taxon=Plecoptera => downloads Plecoptera order, Cannot take into account the intersect between taxa

#http://www.boldsystems.org/index.php/API_Public/combined?taxon=4865&marker=COI-5P&format=tsv
# Do not handle correctly taxon=4865 => downloads everything with 4865

#http://www.boldsystems.org/index.php/API_Public/combined?taxon=Ciliophora&marker=COI-5P&format=tsv
#Downloads other markers as well if processid has at least one COI seq

