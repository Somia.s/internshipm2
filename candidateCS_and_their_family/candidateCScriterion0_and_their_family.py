#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

#---------------------------------#
#     CONSIDERING ONLY A TAXON    #
#---------------------------------#

class Taxon:
    '''
        Final aim:
        Create a table with all families containing at least one candidat CS
        Retrieve the BIN(s) for each nominal species
        Count the number of sequences inside a BIN for each nominal species
    '''

    def __init__(self, taxon_name):
        '''
            A taxon contains BIN ID, species composing the BIN and other features
            Format: csv
            @params taxon_name: the name of the taxon file currently under study
        '''

        self.taxon_name = taxon_name

        # Initialization of the filenames
        self.input_filename = ""
        self.output_filename = ""

        # Structure to contain data
        self.df_data = pd.DataFrame()
        self.df_potentialCS = pd.DataFrame()



    def filePathways(self):
        '''
            Pathway of the files
        '''

        self.input_filename = "../../Data/download_BOLD/cleaned_BOLD/{}".format(self.taxon_name)
        self.output_filename = "../../Output/candidateCScriterion_and_their_family/candidateCScriterion0_and_their_family.csv"

        self.input_filenameRemainedSeq = "../../Output/corrections/coordinates_cutSequences/coordinates_cutSequences.csv"


    def importData(self):
        '''
            Import all features about the current taxa
        '''

        # Read input file
        self.df_data = pd.read_csv(self.input_filename, sep='\t')

        # Select features of interest
        self.df_data = self.df_data[["phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "subspecies_name", "bin_uri", "sequenceID"]]

        # Convert the column with the sequence IDs into string
        self.df_data['sequenceID'] = self.df_data['sequenceID'].astype(str)
        # Convert all dataframe into string
        self.df_data = self.df_data.applymap(str)

        # Remove the duplicate lines
        self.df_data = self.df_data.drop_duplicates()


    def updateNbSequences(self):
        '''
            Some sequences have been deleted, need to update the data.
        '''

        # Retrieve the sequences ID remained
        df_remained_sequences = pd.read_csv(self.input_filenameRemainedSeq, sep="\t")
        # Convert the column with the sequence IDs into string
        df_remained_sequences['sequenceID'] = df_remained_sequences['sequenceID'].astype(str)
        # Select the remained sequence ID
        df_remained_sequences = df_remained_sequences[df_remained_sequences.length >= 500]
        remained_sequences = df_remained_sequences.sequenceID

        # Remove the sequence ID removed after fitting the data
        self.df_data = self.df_data[self.df_data.sequenceID.isin(remained_sequences)]


    def selectFamilies(self):
        '''
            FIRST STEP
            SELECT ONLY FAMILY WITH A POTENTIAL CS 
        '''

        # Select species with different BINs > 1
        self.df_potentialCS = self.df_data.groupby(by='species_name') # Group by species names
        self.df_potentialCS = self.df_potentialCS.aggregate({"bin_uri": lambda x: x.nunique() > 1}) # For each group, retrieve group with different bin_uri, boleen True or False
        self.df_potentialCS = self.df_potentialCS[self.df_potentialCS.values] # Select only true values
        
        # Select the species names with several BINs (>1)
        potentialCS = self.df_potentialCS.index # list of species names

        # Select the families of those potential CS
        families = self.df_data.loc[self.df_data['species_name'].isin(potentialCS), 'family_name']

        # Retrieve all informations about those families
        self.df_potentialCS_and_their_family = self.df_data[self.df_data["family_name"].isin(families)]
        #print(self.df_potentialCS_and_their_family[["species_name", "bin_uri"]].sort_values(by=['species_name']))



    def removeSpeciesWithOnly1Seq(self):
        '''
            SECOND STEP
            REMOVE SPECIES (that are not potential cryptic species) WITH ONLY ONE SEQUENCE
        '''

        # Select the species with 1 BIN
        df_outPotentialCS = self.df_potentialCS_and_their_family.groupby(by='species_name') # Group by species names
        df_outPotentialCS = df_outPotentialCS.aggregate({"bin_uri": lambda x: x.nunique() == 1}) # For each group, retrieve group with one bin_uri, boleen True or False
        df_outPotentialCS = df_outPotentialCS[df_outPotentialCS.values] # Select only true values
        
        # Select the species names with 1 BIN
        outPotentialCS = df_outPotentialCS.index # list of species names
        
        # Retrieve all informations about those species that are not potential cryptic species
        df_outPotentialCS = self.df_data[self.df_data['species_name'].isin(outPotentialCS)]

        # Retrieve the number of sequences for those species and its BINs
        df_outPotentialCS = df_outPotentialCS.groupby(["phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "bin_uri"]).size().reset_index(name="number_of_sequences")
        
        # Species with a number of sequences = 1
        df_outPotentialCS = df_outPotentialCS[df_outPotentialCS.number_of_sequences == 1]
        outPotentialCS = df_outPotentialCS["species_name"]

        # Remove those species
        self.df_potentialCS_and_their_family = self.df_potentialCS_and_their_family[~self.df_potentialCS_and_their_family["species_name"].isin(outPotentialCS)]


    def aggregateSequences(self):
        '''
            THIRD STEP
            AGGREGATE SEQUENCES
        '''

        # Drop duplicates lines
        self.df_potentialCS_and_their_family = self.df_potentialCS_and_their_family.drop_duplicates(["phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "bin_uri", "sequenceID"])

        # Join sequences composing each BINs and count their occurences
        self.df_potentialCS_and_their_family = self.df_potentialCS_and_their_family.groupby(["phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "bin_uri"])['sequenceID'].agg([('number_of_sequences', 'count'), ('sequencesID', lambda x: ','.join(sorted(x)))]).reset_index()


    def saveToCSV(self):
        '''
            Save file with "add" mode, in order to gather all taxa in the same file
        '''

        self.df_potentialCS_and_their_family.to_csv(self.output_filename, sep = "\t", index=False, mode='a', header=False)