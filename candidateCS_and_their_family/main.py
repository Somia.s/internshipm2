#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from candidateCScriterion0_and_their_family import *

import sys
sys.path.append('../')
from commonFunctions import *

def main():

    print("--------- START RETRIEVE CANDIDATE CS AND THEIR FAMILY ---------\n")

    # Create directories for results
    createDirectory("../../Output")
    createDirectory("../../Output/candidateCScriterion0_and_their_family")

    # Clean output files
    output_filename = "../../Output/candidateCScriterion0_and_their_family/candidateCScriterion0_and_their_family.csv"
    f = open(output_filename, "w")
    #header = ["phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "number_of_BINs", "bins_uri", "number_of_sequences", "sequencesID"]
    header = ["phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "bin_uri", "number_of_sequences", "sequencesID"]
    f.write('\t'.join(header)+'\n')
    f.close()

    # Retrieve all files named as "taxon.csv" in Data/download_BOLD/cleaned_BOLD directory
    list_TaxonsCSV = retrieveFilenames("../../Data/download_BOLD/cleaned_BOLD")

    #----------------#
    # For each taxon #
    #----------------#

    #for taxon_name in ["Ismaridae.csv"]:
    
    for taxon_name in list_TaxonsCSV:
        print(taxon_name)
        # Create a taxon
        taxon = Taxon(taxon_name)
        # Name input filename, according to the currently taxon
        taxon.filePathways()
        # Import data
        taxon.importData()
        # Update the sequence IDs available after cleaning and fitting the data
        taxon.updateNbSequences()
        # Select only families of interest
        taxon.selectFamilies()
        # Remove among the nominal species with only 1 BIN, remove those with only 1 sequence
        taxon.removeSpeciesWithOnly1Seq()
        # Aggregate the sequences
        taxon.aggregateSequences()
        # Save to CSV file
        taxon.saveToCSV()
    
    # Remove the duplicate lines
    df_candidateCS_and_their_family = pd.read_csv("../../Output/candidateCScriterion0_and_their_family/candidateCScriterion0_and_their_family.csv", sep = "\t")
    df_candidateCS_and_their_family = df_candidateCS_and_their_family.drop_duplicates(subset=["phylum_name", "class_name", "order_name", "family_name", "genus_name", "species_name", "bin_uri", "number_of_sequences", "sequencesID"])
    df_candidateCS_and_their_family.to_csv("../../Output/candidateCScriterion0_and_their_family/candidateCScriterion0_and_their_family.csv", sep = "\t", index=False, header=True)

    print("--------- END RETRIEVE CANDIDATE CS AND THEIR FAMILY ---------\n")

main()