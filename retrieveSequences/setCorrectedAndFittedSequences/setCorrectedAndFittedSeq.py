#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
from Bio import SeqIO
import re
import string

#-----------------------------------------------#
#     CONSIDERING ONLY A TAXON FILE FROM BOLD   #
#-----------------------------------------------#

class Taxon:

    def __init__(self, taxon_name):
        '''
            A taxon contains BIN numbers, the species composing the BIN and other features
            Format: csv
            @params taxon_name: name of the taxon file currently under study
        '''

        # Name of the current taxon
        self.taxon_name = taxon_name 
        
        ## Initialization

        # Inputs
        self.input_filename = ""
        self.incorrectedSequences_filename = "" # To correct the sequence strands

        # To retrieve the families of interest
        self.filename_families_of_interest = ""
        self.families_of_interest = ""

        # To know how cut the sequences
        self.input_filenameCoordinates = ""

        # Dataframe to contain data
        self.df_data = pd.DataFrame()


    def filePathways(self):
        '''
            Pathway of files
        '''

        # The clean BOLD files
        self.input_filename = "../../../Data/download_BOLD/cleaned_BOLD/{}".format(self.taxon_name)

        # Families of interest
        self.filename_families_of_interest = "../../../Output/candidatCSlevel0_and_their_family/candidatCSlevel0_and_their_family.csv"

        # Sequences with incorrect strand
        self.incorrectedStrandSeq_filename = "../../../Output/corrections/blastDetectStrand/incorrectStrands.out"

        # Coordinates how cut the sequences
        self.input_filenameCoordinates = "../../../Output/corrections/coordinates_cutSequences/coordinates_cutSequences.csv"

        # Lof file
        self.output_filenameLog = "../../../Log/setCorrectedAndFittedSeq.out"


    def familiesOfInterest(self):
        '''
            Retrieve the sequences ID of interest (i.e. belonging to a family containing at least one candidat CS)
        '''

        df_families_of_interest = pd.read_csv(self.filename_families_of_interest, sep='\t')
        self.families_of_interest = df_families_of_interest["family_name"].to_list()
    

    def loadData(self):
        '''
            Load data for a taxon
        '''

        # Load data
        self.df_data = pd.read_csv(self.input_filename, sep='\t')

        # Convert ID sequences into string
        self.df_data['sequenceID'] = self.df_data['sequenceID'].astype(str)

        # Select the features of interest
        self.df_data = self.df_data[["sequenceID", "nucleotides", "phylum_name", "family_name", "genus_name", "species_name"]]

        # Creating a bool series from isin() 
        new = self.df_data["family_name"].isin(self.families_of_interest) 
            
        # Displaying data with family_name is in the families of interest only 
        # Select the families of interest
        self.df_data = self.df_data[new]

        # Drop duplicate lines
        self.df_data = self.df_data.drop_duplicates(["phylum_name", "family_name", "genus_name", "species_name", "sequenceID"])

        # Retrieve the start and the end to conserve for each sequence
        self.df_newCoordinates = pd.read_csv(self.input_filenameCoordinates, sep='\t')
        # Convert ID sequences into string
        self.df_newCoordinates['sequenceID'] = self.df_newCoordinates['sequenceID'].astype(str)


    def createDirectory(self, nameDirectory):
        '''
            Create directory if it hasn't been created yet
        '''
        if not os.path.exists(nameDirectory):
            os.mkdir(nameDirectory)
            

    def complement(self, seq):
        '''
            Correct the reversed sequences
        '''

        complement = {'A': 'T', 'C': 'G', 'G': 'C', 'T': 'A'} 
        bases = list(seq)
        letters = "" # initialization
        for element in bases:
            if element in complement:
                letters+= complement[element]
        
        return letters
        

    def correctStrand(self):
        '''
            Correction of the strand, from minus to plus
        '''

        df_incorrectedStrandSeq = pd.read_csv(self.incorrectedStrandSeq_filename, sep='\t')

        sequenceID_toCorrect = df_incorrectedStrandSeq.qseqid.unique()

        localDict = {}
        self.df_data2 = self.df_data[self.df_data.sequenceID.isin(sequenceID_toCorrect)]
        for index, row in self.df_data2.iterrows():
            localDict[row.sequenceID] = self.complement(row.nucleotides)

        for key in localDict.keys():
            self.df_data.loc[self.df_data.sequenceID == key, 'nucleotides'] = localDict[key]



    def cutSequences(self):
        '''
            Cut the sequences
        '''

        # Retrieve all ID sequences inside this family
        sequencesID = self.df_data["sequenceID"].to_list()
        
        # Select the sequences
        new = self.df_newCoordinates["sequenceID"].isin(sequencesID)
        self.df_newCoordinates = self.df_newCoordinates[new]

        # Cut the sequences
        for index, row in self.df_data.iterrows():

            #Retrieve ID of the current sequence
            ID = row["sequenceID"]
            if ID in self.df_newCoordinates["sequenceID"].to_list():
                
                # Remove the sequences < 500 nts
                if self.df_newCoordinates[self.df_newCoordinates.sequenceID == ID].length.iloc[0] < 500:

                    #self.df_newCoordinates = self.df_newCoordinates[self.df_newCoordinates.sequenceID != ID] # Remove the line
                    # Remove the sequences from the dataframe
                    self.df_data = self.df_data.drop(index)

                else:

                    # Retrieve the start in order to cut the sequence
                    df_start = self.df_newCoordinates.loc[self.df_newCoordinates.sequenceID == ID].start
                    start = df_start.iloc[0]

                    # Retrieve the end in order to cut the sequence
                    df_end = self.df_newCoordinates.loc[self.df_newCoordinates.sequenceID == ID].end
                    end = df_end.iloc[0]

                    # Retrieve the nucleotides of the current sequence
                    sequence = row["nucleotides"]
                    cut_sequence = sequence[start-1:end]

                    # Count the number of "N" nucleotid inside this cut sequence
                    number_of_N = cut_sequence.count("N")

                    # Remove the sequence with 10 or more of "N" of the analysis
                    if number_of_N <= 10:
                        
                        # Replace the original sequence by the cut sequence
                        self.df_data.at[index, 'nucleotides'] = cut_sequence
                        
                    else:
                        # Remove the sequences from the dataframe
                        self.df_data = self.df_data.drop(index)

            else:
                f = open(self.output_filenameLog, "a")
                f.write("ERROR: the {} sequence's coordinates to cut are not recorded. Probably, the BLAST against the reference DB have failed, so impossible to fit.\n".format(ID))
                f.close()

                # Remove the sequence from the dataframe
                self.df_data = self.df_data.drop(index)



    def retrieveSeqFASTAperFamily(self):
        '''
            Retrieve all sequences (their names and their nucleotides) in the same file
            format FASTA for each family
        '''
         
        # Retrieve all family names
        families = self.df_data["family_name"].unique()

        for family_name in families:
            
            # Load data
            df_family = self.df_data[self.df_data.family_name == family_name] # Select only values for a current family

            # Remove duplicated lines
            df_family = df_family.drop_duplicates(["phylum_name", "family_name", "genus_name", "species_name", "sequenceID"])
            
            
            if not df_family.empty:

                # Create a folder for each phylum
                phylum_name = df_family["phylum_name"].unique()[0] # convert a list into a string
                self.createDirectory("../../../Data/sequences/corrected_and_fitted_Sequences/{}".format(phylum_name))

                # Name of the output, one for each family
                output_one_family = "../../../Data/sequences/corrected_and_fitted_Sequences/{}/{}.fas".format(phylum_name, family_name)

                # Save file with "add" mode, in order to gather all taxa in the same file
                with open(output_one_family, "a") as f:
                    for index, row in df_family.iterrows():
                        f.write('>')
                        f.write(str(row['sequenceID']))
                        f.write('\n')
                        f.write(row['nucleotides'])
                        f.write('\n')
                f.close()

                # Remove the duplicated lines
                output_one_family_tmp = "../../../Data/sequences/corrected_and_fitted_Sequences/{}/{}_tmp.fas".format(phylum_name, family_name)
                with open(output_one_family_tmp, 'w') as outFile:
                    record_ids = list()
                    for record in SeqIO.parse(output_one_family, 'fasta'):
                        if record.id not in record_ids:
                            record_ids.append(record.id)
                            SeqIO.write(record, outFile, 'fasta')

                outFile.close()

                os.remove(output_one_family)
                os.rename(output_one_family_tmp, output_one_family)