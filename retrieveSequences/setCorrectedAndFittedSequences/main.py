#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from setCorrectedAndFittedSeq import *

def main():

    print("----- START SET CORRECTED SEQUENCES FOR THE FAMILIES OF INTEREST -----\n")

    # Create the directories for the results
    createDirectory("../../../Data/sequences")
    createDirectory("../../../Data/sequences/corrected_and_fitted_Sequences")
    createDirectory("../../../Log")

    # Create a log file
    f = open("../../../Log/setCorrectedAndFittedSeq.log", "w")
    f.close()

    # Retrieve all files named as "taxon.csv" in Data/download_BOLD/cleaned_BOLD directory
    list_TaxonsCSV = retrieveFilenames("../../../Data/download_BOLD/cleaned_BOLD")

    #----------------#
    # For each taxon #
    #----------------#
    
    for taxon_name in list_TaxonsCSV:

        print(taxon_name)

        # Create a taxon
        taxon = Taxon(taxon_name)

        # Name output filename
        taxon.filePathways()

        # Retrieve the families of interest
        taxon.familiesOfInterest()
        
        # Load data
        taxon.loadData()

        # Correct the strand
        taxon.correctStrand()

        # Cut the sequences
        taxon.cutSequences()

        # Save all sequences and their name, into a FASTA file, for each family
        taxon.retrieveSeqFASTAperFamily()

    print("\n----- END SET CORRECTED SEQUENCES FOR THE FAMILIES OF INTEREST -----\n")

main()
