#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np

#-----------------------------------------------#
#     CONSIDERING ONLY A TAXON FILE FROM BOLD   #
#-----------------------------------------------#

class Taxon:

    def __init__(self, taxon_name):
        '''
            A taxon contains BIN numbers, the species composing the BIN and other features
            Format: csv
            @params taxon_name: name of the taxon file currently under study
        '''

        # Name of the current taxon
        self.taxon_name = taxon_name 
        
        ## Initialization

        # Input
        self.input_filename = ""

        # Output
        self.output_filename = ""

        # Dataframe to contain data
        self.df_taxon = pd.DataFrame() 


    def filePathways(self):
        '''
            Pathway of files
        '''

        self.input_filename = "../../../Data/download_BOLD/cleaned_BOLD/{}".format(self.taxon_name)
        self.output_filename = "../../../Data/sequences/cleanedSequences/cleanedSequences_tmp.fas"


    def loadData(self):
        '''
            For a taxon: load data
        '''

        # Load data
        self.df_taxon = pd.read_csv(self.input_filename, sep='\t')

        # Select features of interest
        self.df_taxon = self.df_taxon[["sequenceID", "nucleotides"]]

        # Drop the duplicate lines
        self.df_taxon = self.df_taxon.drop_duplicates(["sequenceID", "nucleotides"])


    def retrieveOriginalSequences(self):
        '''
            Retrieve all sequences (their IDs and their nucleotides) in the same file
        '''

        # Save file with "add" mode, in order to gather all taxa in the same file
        with open(self.output_filename, "a") as f:
            for index, row in self.df_taxon.iterrows():
                f.write('>')
                f.write(str(row['sequenceID']))
                f.write('\n')
                f.write(row['nucleotides'])
                f.write('\n')
        
        f.close()