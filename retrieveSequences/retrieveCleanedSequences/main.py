#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from retrieveOriginalSequences import *
from Bio import SeqIO
import os

def main():

    print("----- START RETRIEVE CLEANED SEQUENCES -----\n")
    
    # Create directories for the results
    createDirectory("../../../Data/sequences")
    createDirectory("../../../Data/sequences/cleanedSequences")

    # Retrieve all files named as "taxon.csv" in Data/download_BOLD/cleaned_BOLD directory
    list_TaxonsCSV = retrieveFilenames("../../../Data/download_BOLD/cleaned_BOLD")

    #----------------#
    # For each taxon #
    #----------------#
    
    #for taxon_name in ["Coleoptera.csv"]:
    for taxon_name in list_TaxonsCSV:

        print(taxon_name)

        # Create a taxon
        taxon = Taxon(taxon_name)

        # Name output filename
        taxon.filePathways()

        # Load data
        taxon.loadData()

        # Retrieve all original sequences
        taxon.retrieveOriginalSequences()

    
    # Remove the potential duplicated lines
    
    print("\nRemove the potential duplicated lines")
    command = "awk '/^>/{f=!d[$1];d[$1]=1}f' '../../../Data/sequences/cleanedSequences/cleanedSequences_tmp.fas' > '../../../Data/sequences/cleanedSequences/cleanedSequences.fas'"
    os.system(command)
    
    # Remove the tmp file
    os.remove("../../../Data/sequences/cleanedSequences/cleanedSequences_tmp.fas")

    print("\n----- END RETRIEVE CLEANED SEQUENCES -----\n")

main()
