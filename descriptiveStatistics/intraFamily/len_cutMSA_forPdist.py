#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

import sys
sys.path.append('../..')
from commonFunctions import *


# Output filenames
createDirectory("../../../Output/descriptiveStatistics/intraFamily")

output_filename = "../../../Output/descriptiveStatistics/intraFamily/len_MSA_cutGapsExtremities.csv"
output_plot = "../../../Output/descriptiveStatistics/intraFamily/len_MSA_cutGapsExtremities.png"

# Retrieve the available phylum names
phylum_names = retrieveDirectories("../../../Output/intraFamily")

# Structure to contains data
rows = []

# Retrieve the len of MSA after cutting at the extremetites, for each families
for phylum_name in phylum_names:

    family_names = retrieveDirectories("../../../Output/intraFamily/{}".format(phylum_name))

    for family_name in family_names:
        
        filename_pdist = "../../../Output/intraFamily/{}/{}/pdistances.csv".format(phylum_name, family_name)

        if os.path.exists(filename_pdist):

            df_pdist = pd.read_csv(filename_pdist, sep = "\t")

            if not df_pdist.empty: 

                lenMSA_cutGapsExtremities = df_pdist["len_MSA_cutGapsExtremities"].iloc[0]
                
                rows.append([phylum_name, family_name, lenMSA_cutGapsExtremities])


# Convert to panda format
all_rows = [x for x in rows if x != []]
header = ['phylum_name', 'family_name', 'len_MSA_cutGapsExtremities']
df_output = pd.DataFrame(all_rows, columns=header)

# Export dataframe into CSV file
df_output.to_csv(output_filename, sep='\t', index=False)

# Create barplots
sns.set(rc={'figure.figsize':(18,8.27)})
ax = sns.histplot(x = 'len_MSA_cutGapsExtremities',
                            data = df_output, color = 'blue')
ax.set_title("The number of families in terms of the length of the MSA after cuttting the gaps at the extremities")
plt.ylabel('Number of families')
plt.xlabel('Length of the MSA after cuttting the gaps at the extremities')
#plt.xticks(drotation = 100, ha="right")
plt.xticks(ha="right")
plt.savefig(output_plot)
plt.show()