#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from eachTaxon import *
from allTaxa import *
import os
import pandas as pd

def main():

    print("----- START DESCRIPTIVE STATISTICS -----\n")
    
    # Create the directories for results
    createDirectory("../../Output")
    createDirectory("../../Output/descriptiveStatistics")
    createDirectory("../../Output/descriptiveStatistics/nb_Species_with_n_BINs_plots")

    # Retrieve all files named as "taxon.csv" in Data/download_BOLD/cleaned_BOLD directory
    list_TaxonsCSV = retrieveFilenames("../../Data/download_BOLD/cleaned_BOLD")




    #---------------------------------------------------------------------------------------------------------#
    #                                                                                                         #
    #                                       CONSIDERING ONLY A TAXON                                          #
    #                                                                                                         #
    #---------------------------------------------------------------------------------------------------------#

    #------------------#
    #  For each taxon  #
    #------------------#

    for taxon_name in list_TaxonsCSV:

        print(taxon_name)

        # Create a taxon
        taxon = Taxon(taxon_name)

        # Name the input filename
        taxon.loadInput()

        # Load data
        taxon.readInput()

        # Retrieve all nominal species
        taxon.all_nominalSpecies()

        # Length of all sequences
        taxon.lengthSequences()

        # The nominal species and their BINs
        taxon.species_and_nb_BINs()

        # Number of nominal species per family
        taxon.nb_Species_per_Family()

    #-------------------------#
    #   All nominal species   #
    #-------------------------#

    # Remove the duplicated lines
    tmp_output_filename = "../../Output/descriptiveStatistics/all_nominalSpecies_tmp.csv"
    output_filename = "../../Output/descriptiveStatistics/all_nominalSpecies.csv"    
    df_allNominalSpecies = pd.read_csv(tmp_output_filename, names = ['phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name'], sep="\t")
    df_allNominalSpecies = df_allNominalSpecies.drop_duplicates()
    df_allNominalSpecies.to_csv(output_filename, sep = "\t", index=False)

    # Remove the temporary file
    os.remove(tmp_output_filename)

    #--------------------#
    #    All sequences   #
    #--------------------#

    # Remove the duplicated lines
    tmp_output_filename = "../../Output/descriptiveStatistics/sequences_tmp.csv"
    output_filename = "../../Output/descriptiveStatistics/sequences.csv"    
    df_allSequences = pd.read_csv(tmp_output_filename, names = ["sequenceID", "phylum_name", "family_name", "genus_name", "species_name", "bin_uri", "length"], sep="\t")
    df_allSequences = df_allSequences.drop_duplicates()
    df_allSequences.to_csv(output_filename, sep = "\t", index=False)

    # Remove the temporary file
    os.remove(tmp_output_filename)


    #-----------------------------------------------#
    #     Number of nominal species with n BINs     #
    #-----------------------------------------------#

    # Remove the duplicated lines
    tmp_output_filename = "../../Output/descriptiveStatistics/species_and_nb_BINs_tmp.csv"
    output_filename = "../../Output/descriptiveStatistics/species_and_nb_BINs.csv"
    df_Species_and_nb_BINs = pd.read_csv(tmp_output_filename, names = ["phylum_name", "family_name", "species_name", "bin_uri", "sequenceID"], sep="\t")
    df_Species_and_nb_BINs = df_Species_and_nb_BINs.drop_duplicates()
    df_Species_and_nb_BINs.to_csv(output_filename, sep = "\t", index=False)

    # Remove the temporary file
    os.remove(tmp_output_filename)

    #-----------------------------------------------#
    #    Number of nominal species in each family   #
    #-----------------------------------------------#
    
    # Remove the duplicated lines
    tmp_output_filename = "../../Output/descriptiveStatistics/nb_nominalSpecies_perFamily_tmp.csv"
    output_filename = "../../Output/descriptiveStatistics/nb_nominalSpecies_perFamily.csv"
    df_nb_species_per_family = pd.read_csv(tmp_output_filename, names = ["phylum_name", "family_name", "species_name"], sep="\t")
    df_nb_species_per_family = df_nb_species_per_family.drop_duplicates()
    df_nb_species_per_family.to_csv(output_filename, sep = "\t", index=False)

    # Remove the temporary file
    os.remove(tmp_output_filename)







    #-----------------------------------------------------------------------------------------------------#
    #                                                                                                     #
    #                                      CONSIDERING ALL TAXA                                           #
    #                                                                                                     #
    #-----------------------------------------------------------------------------------------------------#

    #-----------------------------------------------#
    #    Number of nominal species in each family   #
    #-----------------------------------------------#
    all_taxa = allTaxa()
    all_taxa.nb_Species_per_Family_AllTaxa()

    #-----------------------------------#
    #  Plot histogram for all sequences #
    #-----------------------------------#
    print("\nPLOT HISTOGRAM: lengths of sequences")
    all_taxa.histLengthSeq()
    
    #-------------------------------------------------------------------------------#
    #    Plot barplots for each phylum: the number of nominal species with n BINs   #
    #-------------------------------------------------------------------------------#
    print("\nPLOT BARPLOTS: the number of nominal species with n BINs")
    all_taxa.species_and_nb_BINs()
    all_taxa.nb_Species_with_n_BINs()



    print("\n----- END DESCRIPTIVE STATISTICS -----\n")

main()
