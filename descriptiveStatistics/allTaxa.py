#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


#---------------------------------#
#      CONSIDERING ALL TAXA       #
#---------------------------------#

class allTaxa:
    '''
        Plot the lengths of all cleaned sequences
    '''

    def __init__(self):

        # Inputs
        self.input_filenameNb_Species_with_n_BINs = "../../Output/descriptiveStatistics/species_and_nb_BINs.csv"
        self.input_filenameSequences = "../../Output/descriptiveStatistics/sequences.csv"
        self.input_filenameNbSpeciesPerFamily = "../../Output/descriptiveStatistics/nb_nominalSpecies_perFamily.csv"

        # Dataframe to contain data
        self.df_output = pd.DataFrame()

        # Outputs
        self.output_filenameNb_Species_with_n_BINs = "../../Output/descriptiveStatistics/nb_Species_with_n_BINs.csv"
        self.output_hist_lengths_seq = "../../Output/descriptiveStatistics/hist_lengths_sequences.png"
        self.output_filenameNbSpeciesPerFamily = "../../Output/descriptiveStatistics/nb_nominalSpecies_perFamily.csv"


    def species_and_nb_BINs(self):
        '''
            Count the number of BINs for each nominal species
        '''

        # Load data
        df_data = pd.read_csv(self.input_filenameNb_Species_with_n_BINs, sep='\t')

        # Remove the duplicated lines
        df_data = df_data.drop_duplicates()
        l1 = df_data.species_name.unique()

        # Count the number of sequences for each species in each BIN
        df_data = df_data.groupby(by=['phylum_name', 'family_name', 'species_name', 'bin_uri'], dropna=False) # Group by species names
        #df_data = df_data.aggregate({"phylum_name": "first", "bin_uri": lambda x: x.nunique(), "sequenceID": lambda x: x.nunique()}).reset_index()
        df_data = df_data.aggregate({"sequenceID": lambda x: x.nunique()}).reset_index()

        # Count the different BINs = n, for each species
        df_data = df_data.groupby(by=['phylum_name', 'family_name', 'species_name'], dropna=False) # Group by species names
        #df_data = df_data.aggregate({"phylum_name": "first", "bin_uri": lambda x: x.nunique(), "sequenceID": "sum"}).reset_index()
        df_data = df_data.aggregate({"bin_uri": lambda x: x.nunique(), "sequenceID": "sum"}).reset_index()

        # Rename the column
        df_data.rename(columns={'bin_uri': 'number_of_BINs'}, inplace=True)
        df_data.rename(columns={'sequenceID': 'number_of_sequences'}, inplace=True)

        # Change the column places
        columns_titles = ["phylum_name", "family_name", "species_name", "number_of_BINs", "number_of_sequences"]
        df_data = df_data.reindex(columns=columns_titles)

        # Save in a output file
        df_data.to_csv(self.input_filenameNb_Species_with_n_BINs, sep = "\t", index=False)


    def nb_Species_with_n_BINs(self):
        '''
            Create csv files and barplots for each phylum: the number of nominal species with n BINs
            Plot an histogram: Number of nominal species according to the number of BINs
        '''

        # Load data
        df_data = pd.read_csv(self.input_filenameNb_Species_with_n_BINs, sep='\t')

        # Remove the duplicated lines
        df_data = df_data.drop_duplicates()

        # Update the file
        df_data.to_csv(self.input_filenameNb_Species_with_n_BINs, sep='\t')

        # Count number of nominal species having n BINs
        df_data = df_data.groupby(by='species_name') # Group by species name
        df_data = df_data.aggregate({"phylum_name": "first", "number_of_BINs": "sum"}).reset_index()

	    # Count for each phylum, the number of x occurences of n BINs numbers
        df_data = df_data.groupby(by=['phylum_name', 'number_of_BINs'])
        df_data = df_data.aggregate({"species_name": "count"}).reset_index()

        # Rename the column
        df_data.rename(columns={'species_name': 'number_of_species'}, inplace=True)

        # Change column places
        columns_titles = ["phylum_name", "number_of_species", "number_of_BINs"]
        df_data = df_data.reindex(columns=columns_titles)

        # Save in a output file
        df_data.to_csv(self.output_filenameNb_Species_with_n_BINs, sep = "\t", index=False)

        # Retrieve the name of phylum
        phylum_name = df_data["phylum_name"].unique()

        for phylum in phylum_name:

            df_phylum = df_data[df_data.phylum_name == phylum] # Select only values for a current phylum

            # Name of the output plot
            output_plot = "../../Output/descriptiveStatistics/nb_Species_with_n_BINs_plots/{}.png".format(phylum)

            # Create barplots
            sns.set(rc={'figure.figsize':(18,8.27)})
            ax = sns.barplot(x = 'number_of_BINs', y = 'number_of_species',
                                        data = df_phylum, color = 'blue')
            ax.set_title("Number of nominal species with n BINs, for the {} phylum".format(phylum))
            plt.ylabel('Number of nominal species')
            plt.xlabel('Number of BINs')
            #plt.xticks(drotation = 100, ha="right")
            plt.xticks(ha="right")
            plt.savefig(output_plot)
            plt.show()


    def histLengthSeq(self):
        '''
            Histogram: the number of sequences with n length
        '''

        # Load data
        self.df_sequences = pd.read_csv(self.input_filenameSequences, sep='\t')
        
        # Create the countplot
        sns.set(rc={'figure.figsize':(18,8.27)})
        ax = sns.distplot(self.df_sequences["length"], kde=False, bins=200)
        ax.set_title("The number of sequences in terms of the length")        
        sns.set_style("ticks", {"xtick.major.size": 1000})
        plt.ylabel('Number of sequences')
        plt.xlabel('Length (bp)')
        plt.xticks(rotation = 80, ha="right")
        plt.savefig(self.output_hist_lengths_seq)
        #plt.show()


    def nb_Species_per_Family_AllTaxa(self):
        '''
            Count the number of nominal species for each family for each taxon file
        '''

        # Read input file
        df_data = pd.read_csv(self.input_filenameNbSpeciesPerFamily, sep='\t')

        # Drop the duplicated lines
        df_data = df_data.drop_duplicates()

        # Count the number of species for each family
        df_data = df_data.groupby(by=['phylum_name', 'family_name']) # Group by family_name
        df_data = df_data.aggregate({"species_name": "count"}).reset_index()

        # Rename the column
        df_data.rename(columns={'species_name': 'number_of_species'}, inplace=True)

        # Save in a output file
        df_data.to_csv(self.output_filenameNbSpeciesPerFamily, sep = "\t", index=False)