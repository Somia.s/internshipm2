#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

#---------------------------------#
#     CONSIDERING ONLY A TAXON    #
#---------------------------------#

class Taxon:

    def __init__(self, taxon_name):
        '''
            A taxon contains BIN numbers, the species composing the BIN and other features
            Format: csv
            @params taxon_name: the name of the taxon file currently under study
        '''

        self.taxon_name = taxon_name # The name of the current taxon

        # Input
        self.input_filename = ""

        # Structure to contain data
        self.df_data = pd.DataFrame()

        # Outputs
        self.output_filename = "../../Output/descriptiveStatistics/sequences_tmp.csv"
        self.output_filenameAllSpecies = "../../Output/descriptiveStatistics/all_nominalSpecies_tmp.csv"
        self.output_filenameNbSpeciesPerFamily = "../../Output/descriptiveStatistics/nb_nominalSpecies_perFamily_tmp.csv"
        self.output_filenameSpecies_and_nb_BINs = "../../Output/descriptiveStatistics/species_and_nb_BINs_tmp.csv"


    def loadInput(self):
        '''
            Pathway of the files
        '''

        self.input_filename = "../../Data/download_BOLD/cleaned_BOLD/{}".format(self.taxon_name)


    def readInput(self):

        # Load data
        self.df_data = pd.read_csv(self.input_filename, sep='\t')

        # Drop the duplicate lines
        self.df_data = self.df_data.drop_duplicates()


    def all_nominalSpecies(self):
        '''
            Retrieve all nominal species
        '''
        
        # Save the file with "add" mode, in order to gather all taxa in the same file
        self.df_data.to_csv(self.output_filenameAllSpecies, columns = ['phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name'], sep = "\t", index=False, mode='a', header=False)


    def lengthSequences(self):
        '''
            Retrieve the length of each sequence
        '''
        # Calculte length of all sequences
        self.df_data["length"] = self.df_data["nucleotides"].str.len()

        # Save file with "add" mode, in order to gather all taxa in the same file
        self.df_data.to_csv(self.output_filename, columns = ["sequenceID", "phylum_name", "family_name", "genus_name", "species_name", "bin_uri", "length"], sep = "\t", index=False, mode='a', header=False)


    def species_and_nb_BINs(self):
        '''
            Count the number of BINs for each nominal species
        '''

        # Select features of interest
        df_data = self.df_data[["phylum_name", "family_name", "species_name", "bin_uri", "sequenceID"]]

        # Remove the duplicated lines
        df_data = df_data.drop_duplicates()

        # Save in a output file
        df_data.to_csv(self.output_filenameSpecies_and_nb_BINs, sep = "\t", index=False, mode='a', header=False)


    def nb_Species_per_Family(self):
        '''
            Count the number of nominal species for each family for each taxon file
        '''

        # Select features of interest
        df_data = self.df_data[["phylum_name", "family_name", "species_name"]]

        # Drop the duplicated lines
        df_data = df_data.drop_duplicates()

        # Save in a output file
        df_data.to_csv(self.output_filenameNbSpeciesPerFamily, sep = "\t", index=False, mode='a', header=False)
