#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

def createDirectory(nameDirectory):
    '''
        Create directory if it hasn't been created yet
    '''
    if not os.path.exists(nameDirectory):
        os.mkdir(nameDirectory)


def retrieveFilenames(directory):
    '''
        Retrieve all files in a directory
        @param directory: Directory with the files
        @return: List of filenames, directory are not considered
    '''

    list_filenames = [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]

    return list_filenames


def retrieveDirectories(directory):
    '''
        Retrieve all files in a directory
        @param directory: Directory with the files
        @return: List of filenames, directory are not considered
    '''

    list_filenames = [f for f in os.listdir(directory) if not os.path.isfile(os.path.join(directory, f))]

    return list_filenames