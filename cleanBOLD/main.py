#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from clean_BOLD import *
import argparse

import sys
sys.path.append('../')
from commonFunctions import *


program_description = ''' ************ PROGRAM DESCRIPTION ************
    Clean data from BOLD database.
   '''

def createParser(): # Retrieve arguments before start the program
    
    parser = argparse.ArgumentParser(add_help=True, 
                                     description=program_description)


    # Delete sequences that are less than the threshold length
    parser.add_argument('-minLength', '--minSequenceLength',
                        help="Delete the sequences that are less than the threshold length",
                        default=500,
                        type=int,
                        required=False)

    return parser


def parseArguments(): # Return a dictionary of arguments
    # Example: {"minLength": "500"}
    
    parser = createParser() # Creation of the parser
    args = parser.parse_args() # Parse the arguments supplied by the user
    dict_args = dict(args.__dict__)
    
    return dict_args


def main(minSequenceLength):

    print("----- START CLEANING BOLD FILES -----\n")

    # Create a folder for results
    createDirectory("../../Data/download_BOLD/cleaned_BOLD")

    # Retrieve all files named as "taxon.csv" in Data/download_BOLD/Downloaded folder
    list_TaxonsCSV = retrieveFilenames("../../Data/download_BOLD/Downloaded")

    # Clean BOLD files
    cleanBOLDFile(list_TaxonsCSV, minSequenceLength)

    print("\n----- END CLEANING BOLD FILES -----")


if __name__ == '__main__':
    arguments = parseArguments()
    main(**arguments)