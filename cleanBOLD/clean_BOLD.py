#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
import re
import string

## Clean all files
def cleanBOLDFile(list_TaxonsCSV, minSequenceLength):

    # Create log file for Error
    log_filename = "../../Log/clean_BOLD.log"
    log_file = open(log_filename, "w")
    log_file.write("Last run date: {}.\n\n".format(datetime.date(datetime.now())))
    log_file.close()

    i=0
    for taxon in list_TaxonsCSV:
        clean_one_BOLDfile(taxon, i, minSequenceLength)
        i+=1

    # Log message
    print("\nPossible errors are accessible in the log file called 'clean_BOLD.log'.")


## Clean one file
def clean_one_BOLDfile(taxon, i, minSequenceLength):

    try:
        ## Read file to clean
        filename = "../../Data/download_BOLD/Downloaded/{}".format(taxon)
        file = open(filename, "r", encoding='latin-1')
        file.readline() # remove head

        ## Create cleaned file
        #taxon = filename.rsplit('/', 1)[-1]
        cleaned_filename = "../../Data/download_BOLD/cleaned_BOLD/{}".format(taxon)
        cleaned_file = open(cleaned_filename, "w")
        # Create a header
        header = ["bin_uri", "phylum_taxID", "phylum_name", "class_taxID", "class_name", "order_taxID", "order_name", "family_taxID", "family_name", "subfamily_taxID", "subfamily_name", "genus_taxID", "genus_name", "species_taxID", "species_name", "subspecies_taxID", "subspecies_name", "sequenceID", "nucleotides", "\n"]
        cleaned_file.write('\t'.join(header))

        for line in file.readlines():

            if line != "":
                list_current_line = []

                cutting = line.strip('\n').split('\t')

                # Retrieve features of interest
                bin_uri = cutting[7] # Remove sequence without BIN identifiant
                markercode = cutting[69] # Keep only COI-5P marker
                species_name = cutting[21] # Remove undefined species
                nucleotides = cutting[71]

                ## Clean the sequences
                # Remove the "-"
                nucleotides = nucleotides.replace("-", "")
                # Replace the letters not be A, T, C or G by N
                chars_to_replace_by_N = list(string.ascii_uppercase) # the whole alphabet
                for i in ["A", "T", "C", "G"]:
                    chars_to_replace_by_N.remove(i)
                rx = '[' + re.escape(''.join(chars_to_replace_by_N)) + ']'
                nucleotides = re.sub(rx, 'N', nucleotides)
                # Remove the N only at the 5' extremity and 3' extremities
                find_N_at_firstExtremity = [m.end(0) for m in re.finditer('^N*', nucleotides)][0]
                find_N_at_secondExtremity = [m.start(0) for m in re.finditer('N*$', nucleotides)][0]
                nucleotides = nucleotides[find_N_at_firstExtremity : find_N_at_secondExtremity]

                if bin_uri != "" and markercode == "COI-5P" and species_name != "" and re.match(r"^[A-Z][a-z]+\s[a-z]+$", species_name) and len(nucleotides) >= minSequenceLength:
                    list_current_line.append(bin_uri)
                    for col in range(8,24):
                        list_current_line.append(cutting[col])

                    # sequenceID
                    list_current_line.append(cutting[68])

                    # nucleotides
                    list_current_line.append(nucleotides)

                    current_line = '\t'.join(list_current_line) + "\n"
                    cleaned_file.write(current_line)
            
        print(i, " ", taxon, " : OK.")

        # Close files
        file.close()
        cleaned_file.close()

    except Exception as e:
        error_message = "\n==================> Error with {} taxon.\nException error: {}.\n".format(taxon, e)
        print(error_message)

        # Create log file for Error
        log_filename = "../../Log/clean_BOLD.log"
        log_file = open(log_filename, "a")
        log_file.write(error_message)
        log_file.close()