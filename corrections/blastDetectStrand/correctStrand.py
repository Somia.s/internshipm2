#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

class Sequences:

    def __init__(self):
        '''
           The object contains all sequences, their ID and their strand (plus or minus)
        '''

        # Input
        self.input_filenameBLAST = "../../../Output/corrections/blastAgainstReference/blastAgainstReference.out"

        # Structures to contain data
        self.df_data = pd.DataFrame()
        self.df_incorrectSeq = pd.DataFrame()

        # Output
        self.output_filenameBLAST = "../../../Output/corrections/blastDetectStrand/incorrectStrands.out"


    def loadData(self):
        '''
            Load data from BLAST output
        '''

        # Name of the columns
        headerResultBLAST = "qseqid sseqid pident length mismatch gapopen qstart qend sstart send qcovhsp sstrand".split(" ")

        # Read the input file
        self.df_data = pd.read_csv(self.input_filenameBLAST, sep='\t', names=headerResultBLAST)

        # Sometimes BLAST returns the result for a sequence twice against its best sequence from the reference
        # We have to keep only the best result ie the first line
        self.df_data = self.df_data.drop_duplicates(subset=['qseqid', 'sseqid'], keep='first')


    def detectIncorrectStrandSeq(self):
        '''
            Detect the sequences with a incorrect strand (i.e. retrieve their ID)
        '''

        # Select the sequences where the sequence of reference is minus strand
        self.df_incorrectSeq = self.df_data[self.df_data.sstrand == "minus"]

        # Save in a output file
        self.df_incorrectSeq.to_csv(self.output_filenameBLAST, sep = "\t", index=False)