#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from correctStrand import *

def main():

    print("--------- START CORRECTION STRAND ---------\n")

    # Create directories
    createDirectory("../../../Output")
    createDirectory("../../../Output/corrections")
    createDirectory("../../../Output/corrections/blastDetectStrand")

    sequences = Sequences()
    sequences.loadData() # Load data
    sequences.detectIncorrectStrandSeq() # Detect incorrect strands

    print("--------- END CORRECTION STRAND ---------\n")

main()
