#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
from Bio import SeqIO


class Sequences:

    def __init__(self):
        '''
           This class contains all sequences and
            - their ID,
            - qstart means Start of alignment in query,
            - qend means End of alignment in query,
            - sstart means Start of alignment in subject,
            - send means End of alignment in subject.
        '''

        ## Inputs
        # Result from BLAST
        self.input_resultBLAST = "../../../Output/corrections/blastAgainstReference/blastAgainstReference.out"
        # The lengths of sequences
        self.input_lengths_sequences = "../../../Output/descriptiveStatistics/sequences.csv"
        # The DB of reference
        self.DB_183seq = "../../../Data/DB_183seq/1_one_seq_per_class.fas"

        # Structures to contain data
        self.df_resultBLAST = pd.DataFrame()
        self.df_lengths_sequences = pd.DataFrame()

        # Output
        self.output_filenameCoordinates = "../../../Output/corrections/coordinates_cutSequences/coordinates_cutSequences.csv"


    def loadData(self):
        '''
            Load data from BLAST output
        '''

        headerResultBLAST = "qseqid sseqid pident length mismatch gapopen qstart qend sstart send qcovhsp sstrand".split(" ")
        self.df_resultBLAST = pd.read_csv(self.input_resultBLAST, sep='\t', names=headerResultBLAST) # result from BLAST

        self.df_lengths_sequences = pd.read_csv(self.input_lengths_sequences, sep='\t') # lengths of the all sequences

        self.dict_DB_183seq = SeqIO.to_dict(SeqIO.parse(self.DB_183seq, "fasta")) # sequences from the reference DB (183 sequences)
    

    def removeDuplicatedID(self):
        '''
            Sometimes BLAST returns the result for a sequence twice against its best sequence from the reference
            We have to keep only the best result ie the first line
        '''

        self.df_resultBLAST = self.df_resultBLAST.drop_duplicates(subset=['qseqid', 'sseqid'], keep='first')

    
    def retrieveCoordinates(self):
        '''
            Retrieve the coordinates (start and end) of the query sequence to conserve
        '''

        # Load data
        self.loadData()
        # Remove the duplicated IDs
        self.removeDuplicatedID()

        # Create the output file with the coordinates to conserve
        f = open(self.output_filenameCoordinates, "w")
        header = ["sequenceID", "start", "end", "length"]
        f.write("\t".join(header) + "\n")

        # Parse the result from BLAST
        for index, row in self.df_resultBLAST.iterrows():

            # SUBJECT = The sequence of reference (from the reference DB) 
            # QUERY = The sequence from BOLD currently under the study

            # RETRIEVE THE ID SEQUENCE OF THE QUERY
            line = []
            line.append(str(row.qseqid))

            # RETRIEVE LENGTHS OF THE SUBJECT SEQUENCE
            seq_subject = self.dict_DB_183seq[row.sseqid].seq
            len_subject = len(seq_subject)

            # RETRIEVE LENGTHS OF THE QUERY SEQUENCE
            len_query = self.df_lengths_sequences.loc[self.df_lengths_sequences.sequenceID==row.qseqid].length
            len_query = len_query.iloc[0]

            #----------------------#
            #  START OF THE QUERY  #
            #----------------------#
            if row.qstart > row.sstart: # The query starts the alignment before the subject
                start_query = row.qstart - row.sstart + 1

            elif row.qstart < row.sstart: # The query starts the alignment after the subject
                start_query = 1
                
            elif row.qstart == row.sstart:
                start_query = 1
                
            line.append(str(start_query))                
            

            #----------------------#
            #   END OF THE QUERY   #
            #----------------------#
            len_qend_not_overlapping = len_query - row.qend
            len_send_not_overlapping = len_subject - row.send


            if len_qend_not_overlapping > len_send_not_overlapping:
                end_query = len_query - (len_qend_not_overlapping - len_send_not_overlapping)

            if len_qend_not_overlapping < len_send_not_overlapping:
                end_query = len_query

            if len_qend_not_overlapping == len_send_not_overlapping:
                end_query = len_query

            line.append(str(end_query))

            # Length of the final conserved sequence
            conserve_length = end_query - start_query + 1
            line.append(str(conserve_length))

            f.write("\t".join(line) + "\n")

        f.close()