#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from coordinates_cutSequences import *

def main():

    print("--------- START FETCH COORDINATES IN ORDER TO CUT SEQUENCES ---------\n")

    # Create directories
    createDirectory("../../../Output")
    createDirectory("../../../Output/corrections")
    createDirectory("../../../Output/corrections/coordinates_cutSequences")

    # Creation of the object
    sequences = Sequences()
    
    # Fetch the coordinates according to the DB reference (composed of 183 sequences)
    sequences.retrieveCoordinates()

    print("--------- END FETCH COORDINATES IN ORDER TO CUT SEQUENCES ---------\n")

main()
