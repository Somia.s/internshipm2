#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.metrics import confusion_matrix
import plotly.express as px

#-------------------------#
#        One taxon        #
#-------------------------#

class Taxon:

    def __init__(self, taxon_name):
        '''
            A taxon contains BIN numbers, the species composing the BIN and other features
            Format: csv
            @params taxon_name: the name of the taxon file currently under study
        '''

        # The name of the current taxon
        self.taxon_name = taxon_name 

        # Input
        self.input_filename = ""

        # Structure to contain data
        self.df_data = pd.DataFrame()

        # Output
        self.output_filename = "../../../Output/corrections/detectDiscordantSpecies/BINsAndTheirSpecies_tmp.csv"


    def loadData(self):
        '''
            Load the data from BOLD (CSV files)
        '''

        # Pathway of the input
        self.input_filename = "../../../Data/download_BOLD/cleaned_BOLD/{}".format(self.taxon_name)

        # Load data
        self.df_data = pd.read_csv(self.input_filename, sep='\t')

        # Drop the duplicate lines
        self.df_data = self.df_data.drop_duplicates()

    
    def BINs_and_their_species(self):
        '''
            Fetch the BINs and their species.
        '''

        # Select the features of interest
        df_data = self.df_data[['bin_uri', 'phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name', 'sequenceID']]

        # Remove the duplicated lines
        df_data = df_data.drop_duplicates()

        # Save in an output file
        df_data.to_csv(self.output_filename, sep = "\t", index=False, mode='a', header=False)




#-------------------------#
#        All taxa         #
#-------------------------#

class Taxa:

    def __init__(self):

        # Input files
        self.input_filename = "../../../Output/corrections/detectDiscordantSpecies/BINsAndTheirSpecies.csv"
        self.input_filename_update = "../../../Output/corrections/detectDiscordantSpecies/BINsAndTheirSpecies_nbSeqPerSpecies.csv"
        self.input_filenameCSResults = "../../../Output/detectCandidateCS/detectCandidateCS.csv"

        # Output files
        self.output_filenameBINstaxonomy = "../../../Output/corrections/detectDiscordantSpecies/taxonomyInsideBINs.csv"
        self.output_filenameGrades = "../../../Output/corrections/detectDiscordantSpecies/grades.csv"
        self.output_filenameJoinResults = "../../../Output/corrections/detectDiscordantSpecies/joinResults.csv"
        self.output_filenameConfusionMatrix = "../../../Output/corrections/detectDiscordantSpecies/confusionMatrix.txt"

        # Output figures
        self.output_pieChart = "../../../Output/corrections/detectDiscordantSpecies/grades.png"
        self.output_barChart1 = "../../../Output/corrections/detectDiscordantSpecies/gradesPerPhylum1.html"
        self.output_barChart2 = "../../../Output/corrections/detectDiscordantSpecies/gradesPerPhylum2.html"

        # Count some statistics
        self.nb_phylum = None
        self.nb_genus = None
        self.nb_families = None
        self.nb_nominal_species = None

        # Fetch the number of sequences in each nominal species independently of the BIN
        self.fetchNbSeqPerSpecies()


    def fetchNbSeqPerSpecies(self):
        '''
            Count the number of sequences in each nominal species independently of the BIN
        '''

        # The BINs and their species with their ID sequences
        df_BINs_and_theirSpecies = pd.read_csv(self.input_filename, sep='\t')

        # Count the number of sequences in each nominal species independently of the BIN
        df_nbSeqPerSpecies = df_BINs_and_theirSpecies.groupby("species_name").size().to_frame('nb_sequences_in_species').reset_index()

        # Merge:
        # 1. the BINs and their species with their ID sequences
        # with
        # 2. the number of sequences in each nominal species independently of the BIN
        df_merged = pd.merge(df_BINs_and_theirSpecies, df_nbSeqPerSpecies)

        # Remove the sequences ID column
        df_merged = df_merged.drop(['sequenceID'], axis=1)

        # Remove the potential duplicated lines
        df_merged = df_merged.drop_duplicates()

        # Save in an output file
        df_merged.to_csv(self.input_filename_update, sep = "\t", index=False)


    def taxonomy_inside_BINs(self):
        '''
            Inside each BIN, count the number of phylum, class, order, family, genus and species
        '''

        # Retrieve the BINs and their species
        df_BINs_and_theirSpecies = pd.read_csv(self.input_filename_update, sep='\t')

        # Group by BIN and count the number of uniques species (and unique other taxonomic ranks)
        df_BINstaxonomy = df_BINs_and_theirSpecies.groupby(by=["bin_uri"]).aggregate({"phylum_name": lambda x: x.nunique(),
                                                                "class_name": lambda x: x.nunique(),
                                                                "order_name": lambda x: x.nunique(),
                                                                "family_name": lambda x: x.nunique(),
                                                                "genus_name": lambda x: x.nunique(),
                                                                "species_name": lambda x: x.nunique()}).reset_index()

        # Save in a output file
        df_BINstaxonomy = df_BINstaxonomy.sort_values(by=["species_name"], ascending=False)
        df_BINstaxonomy.to_csv(self.output_filenameBINstaxonomy, sep = "\t", index=False, header=['bin_uri', 'number_of_phylum', 'number_of_class', 'number_of_order', 'number_of_family', 'number_of_genus', 'number_of_species'])


    def addGrades(self):
        '''
            Give 1 or several qualitative grades for each species
        '''

        # Load the BINs and their species
        df_data = pd.read_csv(self.input_filename_update, sep='\t')

        # Count the number of species in each BIN
        df_nbSpeciesPerBIN = df_data.groupby(by=["bin_uri"]).size().reset_index(name='nb_species_in_BIN')
        df_data = pd.merge(df_data, df_nbSpeciesPerBIN) # Merge the number of species inside a BIN

        # Add grade E
        df_data.loc[df_data['nb_species_in_BIN'] > 1, 'grade'] = 'E'

        # Add grade D
        df_data.loc[((df_data['nb_sequences_in_species'] < 4) & (df_data['nb_species_in_BIN'] == 1)), 'grade'] = "D"
        df_data.loc[((df_data['nb_sequences_in_species'] < 4) & (df_data['nb_species_in_BIN'] > 1)), 'grade'] = "DE"

        # Count the number of BIN in each species
        df_nb_BIN_in_species = df_data.groupby("species_name").size().to_frame('nb_BIN_in_species').reset_index()
        df_data = pd.merge(df_data, df_nb_BIN_in_species) # Merge with the principal dataframe

        # Add grade C
        df_data.loc[(df_data['nb_BIN_in_species'] > 1) & (df_data['nb_sequences_in_species'] >= 4) & (df_data['nb_species_in_BIN'] == 1), 'grade'] = "C"
        df_data.loc[(df_data['nb_BIN_in_species'] > 1) & (df_data['nb_sequences_in_species'] >= 4) & (df_data['nb_species_in_BIN'] > 1), 'grade'] = "CE"

        # Add grade A and B
        df_data.loc[(df_data['nb_species_in_BIN'] == 1) & (df_data['nb_BIN_in_species'] == 1) & (df_data['nb_sequences_in_species'] < 11), 'grade'] = "B"
        df_data.loc[(df_data['nb_species_in_BIN'] == 1) & (df_data['nb_BIN_in_species'] == 1) & (df_data['nb_sequences_in_species'] > 10), 'grade'] = "A"

        # Save in a output file
        df_data.to_csv(self.output_filenameGrades, sep = "\t", index=False)


    def plotGrades(self):
        '''
            Plot the proportions for all the gathered phyla, and each phylum.
            For each phylum: removing the D and B grades (bad qualities)
        '''

        # Load the grades of each species
        df_grades = pd.read_csv(self.output_filenameGrades, sep='\t')

        # Select only the grade of each species
        df_grades = df_grades[["phylum_name", "species_name", "grade"]]

        # Drop the potential duplicated lines
        df_grades = df_grades.drop_duplicates()

        # Gather the different grades for a same species
        df_grades = df_grades.groupby(by=["phylum_name", "species_name"]).agg({'grade':lambda x: ''.join(sorted(x))}).reset_index()

        # Replace CCE by CE, and DDE by DE
        df_grades["grade"] = df_grades["grade"].replace(['CCE'],'CE')
        df_grades["grade"] = df_grades["grade"].replace(['DDE'],'DE')

        # Count the number of species for each grade
        df_grades1 = df_grades.groupby(['grade']).agg({'species_name':lambda x: x.nunique()}).reset_index()

        # Count the number of species for each grade inside each phylum
        df_grades = df_grades[~(df_grades.grade.str.contains('D') | df_grades.grade.str.contains('B'))] # Remove species with insuffisant data
        df_grades2 = df_grades.groupby(['phylum_name', 'grade']).agg({'species_name':lambda x: x.nunique()}).reset_index()
        df_grades3 = df_grades2.groupby(['phylum_name']).agg({'species_name':'sum'}).reset_index()
        df_grades3.rename(columns={'species_name':'total'}, inplace = True)
        df_grades4 = pd.merge(df_grades2, df_grades3[['phylum_name', 'total']], on="phylum_name")
        df_grades4['proportion'] = df_grades4['species_name']/df_grades4['total']
        df_grades4['phylum_name_total'] = df_grades4['total'].astype(str) + " (" + df_grades4['phylum_name'] + ")"
        df_grades4 = df_grades4.sort_values(by=['total'], ascending=False)

        # Plot a pie chart, all phylum are gathered
        fig = plt.figure()
        plt.pie(df_grades1['species_name'], labels=df_grades1['grade'], colors=["green", "lightgrey", "orange", "coral", "grey", "silver", "red"], autopct='%1.1f%%')
        plt.title("Grades")
        fig.savefig(self.output_pieChart, dpi=fig.dpi)

        # Plot a bar chart, a bar for each phylum
        fig = px.bar(df_grades2, x="phylum_name", y="species_name", color="grade", title="Grades per phylum")
        fig.write_html(self.output_barChart1)

        fig = px.bar(df_grades4, y="phylum_name_total", x="proportion", color="grade", color_discrete_sequence=["orange", "green", "red", "coral"], title="Grades per phylum", orientation='h')
        fig.update_layout(yaxis=dict(title="Number of species in a phylum (and its name)"))
        fig.update_traces(textposition='outside')
        fig.update_yaxes(categoryarray=df_grades4["phylum_name_total"])
        fig.write_html(self.output_barChart2)


    def addResults(self):
        '''
            Add the biblio and the results from the bioinformatic tool (criteria 0, 1 and 2)
        '''

        # The species and their grades
        df_grades = pd.read_csv(self.output_filenameGrades, sep='\t')

        # Keep only the columns of interest in order to avoid the repetive lines with the same species
        df_grades = df_grades[["phylum_name", "family_name", "genus_name", "species_name", "grade"]]
        # Remove the duplicated lines
        df_grades = df_grades.drop_duplicates()

        # Gather the different grades for a same species
        df_grades = df_grades.groupby(by=["phylum_name", "genus_name", "family_name", "species_name"], dropna=False).agg({'grade':lambda x: ''.join(sorted(x))}).reset_index()

        # Replace CCE by CE, and DDE by DE
        df_grades["grade"] = df_grades["grade"].replace(['CCE'],'CE')
        df_grades["grade"] = df_grades["grade"].replace(['DDE'],'DE')

        # Fetch the result from the CS criteria
        df_CSresults = pd.read_csv(self.input_filenameCSResults, sep='\t')
        df_CSresults = df_CSresults[df_CSresults.genusLevel != True] # Select only the result from the family level
        # Merge the two dataframes: 
        # 1. The nominal species with grades and 
        # 2. The result of the CS detection for each nominal species
        df_merged = pd.merge(df_grades, df_CSresults)

        # Save in a output file
        df_merged.to_csv(self.output_filenameJoinResults, sep = "\t", index=False)


    def countStatistic(self, df):

        self.nb_phylum = df["phylum_name"].nunique()
        self.nb_genus = df["genus_name"].nunique()
        self.nb_families = df["family_name"].nunique()
        self.nb_nominal_species = df["species_name"].nunique()


    def proportionE(self):
        '''
            Construct a confusion matrix to see the number of true positives, false positives, true negatives and false negatives, 
            comparing the biblio and the result from the BIN BOLD.

            Inside the grades C, retrieve the grades E : for TP and FP. 
        '''
        
        # Open the output file
        f = open(self.output_filenameConfusionMatrix, "w")

        # Remove the nominal species without information about CS biblio
        df_CS_results = pd.read_csv(self.output_filenameJoinResults, sep="\t")

        # Drop the species without a bibliography
        df_CS_results = df_CS_results.dropna(subset = ["CS_biblio"])

        # The prediction
        for test in ["test0", "test1", "test2"]:

            f.write("Confusion matrix: {}\n\n".format(test))

            if test == "test0":
                y_pred = df_CS_results[test].to_list() # The prediction from bioinformatic tool
                y_real = df_CS_results["CS_biblio"].to_list() # The bibliography

                self.countStatistic(df_CS_results)
                
            if test == "test1":
                # Remove the real CS having failed the test 0
                df_CS_results.loc[(df_CS_results['test0'] == False), 'test0'] = np.nan
                df_CS_results = df_CS_results.dropna(subset = ["test0"])
                df_CS_results2 = df_CS_results.dropna(subset = ["test1"])
                y_real = df_CS_results2["CS_biblio"].to_list()
                y_pred = df_CS_results2[test].to_list()

                self.countStatistic(df_CS_results)

            if test == "test2":
                # Remove the real CS having failed the test 0
                df_CS_results.loc[(df_CS_results['test0'] == False), 'test0'] = np.nan
                df_CS_results3 = df_CS_results.dropna(subset = ["test2"])
                y_real = df_CS_results3["CS_biblio"].to_list()
                y_pred = df_CS_results3[test].to_list()

                self.countStatistic(df_CS_results)


            cmtx = pd.DataFrame(confusion_matrix(y_real, y_pred, 
                                    labels=[True, False]), 
                                    index=['Biblio:TrueCS', 'Biblio:FalseCS'], # Real data
                                    columns=['BOLD:TrueCS', 'BOLD:FalseCS']) # Prediction data
            
            cmtx["Total"] = cmtx.sum(axis=1)

            f.write(str(cmtx))
            f.write("\n\nNumber of phylum: {} / Number of genus: {} / Number of families: {} / Number of nominal species: {}".format(self.nb_phylum, self.nb_genus, self.nb_families, self.nb_nominal_species))

            # Proportion of E
            if test == "test0":
                df_TP = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True)]
                df_TP_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["grade"].str.contains('E'))]
                
                df_FP = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True)]
                df_FP_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["grade"].str.contains('E'))]
            
                df_FN = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == False)]
                df_FN_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == False) & (df_CS_results["grade"].str.contains('E'))]
                
                df_TN = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == False)]
                df_TN_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == False) & (df_CS_results["grade"].str.contains('E'))]
            

            elif test == "test1":
                df_TP = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == True)]
                df_TP_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == True) & (df_CS_results["grade"].str.contains('E'))]
                
                df_FP = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == True)]
                df_FP_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == True) & (df_CS_results["grade"].str.contains('E'))]
            
                df_FN = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == False)]
                df_FN_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == False) & (df_CS_results["grade"].str.contains('E'))]
                
                df_TN = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == False)]
                df_TN_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test1"] == False) & (df_CS_results["grade"].str.contains('E'))]
            
            elif test == "test2":
                df_TP = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == True)]
                df_TP_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == True) & (df_CS_results["grade"].str.contains('E'))]
                
                df_FP = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == True)]
                df_FP_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == True) & (df_CS_results["grade"].str.contains('E'))]

                df_FN = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == False)]
                df_FN_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == True) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == False) & (df_CS_results["grade"].str.contains('E'))]
                
                df_TN = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == False)]
                df_TN_gradeE = df_CS_results[(df_CS_results["CS_biblio"] == False) & (df_CS_results["test0"] == True) & (df_CS_results["test2"] == False) & (df_CS_results["grade"].str.contains('E'))]

            f.write("\n\nProportion of E in TP: {}/{} {}%".format(len(df_TP_gradeE), len(df_TP), len(df_TP_gradeE)/len(df_TP)*100))
            f.write("\nProportion of E in FP: {}/{} {}%".format(len(df_FP_gradeE), len(df_FP), len(df_FP_gradeE)/len(df_FP)*100))
            f.write("\n\nProportion of E in FN: {}/{} {}%".format(len(df_FN_gradeE), len(df_FN), len(df_FN_gradeE)/len(df_FN)*100))
            f.write("\nProportion of E in TN: {}/{} {}%".format(len(df_TN_gradeE), len(df_TN), len(df_TN_gradeE)/len(df_TN)*100))


            f.write('\n\n************************************************************\n\n')
        
        f.close()
