#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
sys.path.append('../..')
from commonFunctions import *

from detectDiscordantSpecies import *

def main():

    print("--------- START DETECT DISCORDANT NOMINAL SPECIES ---------\n")

    # Create directories for results
    createDirectory("../../../Output")
    createDirectory("../../../Output/corrections")
    createDirectory("../../../Output/corrections/detectDiscordantSpecies")

    # Retrieve all files named as "taxon.csv" in Data/download_BOLD/cleaned_BOLD directory
    list_TaxonsCSV = retrieveFilenames("../../../Data/download_BOLD/cleaned_BOLD")

    for taxon_name in list_TaxonsCSV:

        print(taxon_name)
        
        # Create a taxon
        taxon = Taxon(taxon_name)
        # Load data
        taxon.loadData()
        # Detect BINs and their different nominal species
        taxon.BINs_and_their_species()


    # Remove duplicated lines
    tmp_output_filename = "../../../Output/corrections/detectDiscordantSpecies/BINsAndTheirSpecies_tmp.csv"
    output_filename = "../../../Output/corrections/detectDiscordantSpecies/BINsAndTheirSpecies.csv"
    header = ['bin_uri', 'phylum_name', 'class_name', 'order_name', 'family_name', 'genus_name', 'species_name', 'sequenceID']
    df_output = pd.read_csv(tmp_output_filename, names = header, sep="\t")
    df_output = df_output.drop_duplicates()
    df_output.sort_values(by=header, ascending=True)
    df_output.to_csv(output_filename, sep = "\t", index=False)

    # Remove the temporary file
    os.remove(tmp_output_filename)

    # All taxa
    taxa = Taxa()
    taxa.taxonomy_inside_BINs()
    taxa.addGrades()
    taxa.plotGrades()
    taxa.addResults()
    taxa.proportionE()

    print("--------- END DETECT DISCORDANT NOMINAL SPECIES ---------\n")

main()
