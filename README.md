# Automatic characterization of nominal species diversities and divergences: Detection of candidate cryptic species

The morphological comparison is sometimes not sufficient to delimit species. Species where morphological characters do not distinguish biological groups are named cryptic species (CS).

### Prerequisites
What things you need to install the software and how to install them

```
conda -c install os
conda -c install pandas
conda -c install re
conda -c install string
conda -c install datetime
conda -c install pysam
conda -c install itertools
conda -c install biopython
conda -c install sklearn
conda -c install matplotlib
conda -c install plotly
conda -c install seaborn
```

Or retrieve the packages in environment.yml file.

```
conda env create --file environment.yml
conda activate detectCS
```


## Documentation

[BLAST](https://www.ncbi.nlm.nih.gov/books/NBK279684/)

[MAFFT](https://mafft.cbrc.jp/alignment/software/manual/manual.html)


## Running

### Architecture

```
appDetectCS
├── Data
│   └── download_BOLD
        └── input_list
            ├── download_bold_species_list.ods
            ├── BOLD_download_taxon_list.txt
        └── Downloaded
                └── taxons.csv
        └── clean_BOLD
                └── taxons.csv
│   └── DB_183seq
│   └── CS_biblio
│   └── sequences
        └── cleanedSequences
        └── corrected_and_fitted_Sequences
├── Scripts
├── Output
│   └── descriptiveStatistics
        └── nb_Species_with_n_BINs_plots
        └── intraFamily
│   └── candidateCScriterion0_and_their_family
│   └── corrections
        └── blastAgainstReference
        └── blastDetectStrand
        └── coordinates_cutSequences
        └── detectDiscordantSpecies
│   └── intraFamily
        └── Phyla
                └── Families
│   └── compare_pi_within_interBINs
│   └──detectCandidateCS
        └── test0
        └── test1
        └── test2
        └── ratioThresholds
        └── speciesThresholds
        └── siblingSisters
        └── concordantBINs
```

### PRE PROCESSING

#### Clean BOLD files

```
cd cleanBOLD
python3 main.py
```
- Description: Clean BOLD files
- Input: CSV files, downloads from BOLD database (2021-06-01).
- Output: CSV files, cleaned downloads.
    - Unknown species have been deleted.
    - Only keep COI-5P marker.
    - Features of interest have been selected.

Optional arguments:
- minLength: to delete sequences that are less than the threshold length, by default 500 bp.

Example:
```
python3 main.py -minLength 500
```

### ANALYSIS OF DATA

#### Descriptive statistics

##### Number of nominal species with n BINs

```
cd descriptiveStatistics
python3 main.py
```

Output files:
1. all_nominalSpecies.csv: Summary of all nominal species (taxonomy rank)
2. sequences.csv: Summary of all sequences
3. hist_lengths_all_sequences.png: a histogram showing sequence number with n length
3. species_and_nb_BINs.csv: Number of BINs for each nominal species
4. nb_nominalSpecies_perFamily: Number of species per family
5. nb_Species_with_n_BINs.csv: Number of nominal species with n BINs for each phylum
6. Barplots per phylum in nb_Species_with_n_BINs_plots folder: Nominal species number in terms of BINs number for each phylum
7. intraFamily/len_MSA_cutGapsExtremities.csv: Families number in terms of the length of MSA (Multiple Sequence Alignment) after cutting the gaps at extremities (trimming step)


#### Detect criterion 0 candidate CS and their family

```
cd candidateCScriterion0_and_their_family
python3 main.py
```

Description:
- Retrieve criterion 0 candidate CS 
- Trace back to the family
- Keep inside those families: other nominal species with a number of sequences > 1

Output: candidateCScriterion0_and_their_family.csv


### CORRECTIONS

#### Compare all sequences with a reference database

```
cd corrections/blastAgainstReference
make
```

- Description: Compare all sequences with a reference database in order to standardize all sequences
- Data: a database with 183 reference sequences 
- Tool: Nucleotide-Nucleotide BLAST 2.2.31+
- Output file: blastAgainstReference/blastAgainstReference.out

#### Detect sequences with reverse strand

```
cd corrections/blastDetectStrand
python3 main.py
```

- Description: Detect sequences with reverse strand
- Input file: blastAgainstReference/blastAgainstReference.out
- Output file: blastDetectStrand/incorrectSequences.out

#### Trimming sequences

```
cd corrections/coordinates_cutSequences
python3 main.py
```

- Description: Set coordinates to know how cutting sequences, in order to align sequences in same region.
- Input file: blastAgainstReference/blastAgainstReference.out
- Output file: coordinates_cutSequences/coordinates_cutSequences.csv

#### Remove discordant species

```
cd corrections/detectDiscordantSpecies
python3 main.py
```
- Description: Detect and remove discordand species (i.e. species sharing BINs with other nominal species)
- Input file: blastAgainstReference/blastAgainstReference.out
- Output files:
1. BINsAndTheirSpecies_nbSeqPerSpecies.csv: BINs and their species, sequence number within species
2. grades.csv: qualitative grades of each species
3. grades.png: piechart with qualitative grade proportions
4. gradesPerPhylum.html: qualitative grade proportions per phylum
5. confusionMatrix.txt: confusion matrix without discordant species
6. taxonomyInsideBINs.csv: number of different taxonomic ranks for each BIN


### RETRIEVE SEQUENCES

#### Cleaned sequences

From fastafile : Data/sequences/cleanedSequences.fas

#### Cutting sequences

- Only for families of interest
- Fetch into several folders: one folder per family (Data/sequences/corrected_and_fitted_Sequences/)

### INTRA-FAMILY DISTANCES

#### Perform multiple sequence alignments (MSA) for each family

```
cd intraFamily/MSAperFamily
python3 main.py
```

- Software: MAFFT
- Version: v7.471 
- Output: intrafamily/phylum/family/MSA.fas

#### Calculate pairwise p-distances and compare within family (for criterion 1 and 2)

```
cd intraFamily/pdistIntraFamily
python3 main.py
```

Optional arguments:
- PhylumName: only analyse a phylum name
- PhylumFileName: analyse a list of phyla (by parsing a file)
- FamilyName: only analyse a family name
- FamilyFileName: analyse a list of families (by parsing a file), the phylum can be retrieved only given family name

Example:
```
python3 main.py -FamilyName Abacionidae
```

Outputs: 
1. intrafamily/phylum/family/pdistances.csv
2. intrafamily/phylum/family/comparisonsPdist.csv


#### About comparisons between p-distances in order to detect candidate CS

| Criterion  | Description |
| ------------- | ------------- |
| 0  | MultiBINs species  |
| 1  | At least a ratio > 1  |
| 2  | At least a ratio < 1  |

ratio(criterion 1): p-distance between BINs of a candidate CS and p-distance between nominal species from same family 

ratio(criterion 2): the polymorphism is included

### COMPARISON OF POLYMORPHISM WITHIN PAIRWISE BINs OF A CANDIDATE CS

```
cd compare_pi_within_interBINs
python3 main.py
```
- Description: Compare polymorphism within pairwise BINs of each candidate CS
- Input: pdistance files of each family
- Outputs:
1. compare_pi_within_interBINs.csv: results for each test
2. compare_pi_within_interBINs.png: plot of results according to sequence numbers within the larger BIN and the smaller BIN
3. proportionsPvalue.png: pie chart with the proportion significant p-value and not significant p-value
4. proportionsNullDistSupOrInf.png: pie chart with proportions of times the larger BIN has a lower or higher polymorphism than the smaller BIN
5. CSresults_criterion3.csv: determine if criterion 3 is validated for each criterion 0 nominal species

Optional arguments:
- PhylumName: only analyse a phylum name
- PhylumFileName: analyse a list of phyla (by parsing a file)
- FamilyName: only analyse a family name
- FamilyFileName: analyse a list of families (by parsing a file), phylum can be retrieved only given family name

### DETECT CANDIDATE CS

```
cd detectCandidateCS
python3 main.py
```

## Detect candidate CS

Inputs:
1. All nominal species and their number of sequences: descriptiveStatistics/species_and_nb_BINs.csv
2. All nominal species and complete taxonomic informations: descriptiveStatistics/all_nominalSpecies.csv
3. P-distance comparisons within family: */\*/comparisonsPdist.csv

Outputs: 
1. Species with only 1 BIN: detectSpecies_withoutBiblio_failedCriterion0.csv
2. Species with more than 1 BIN: detectCandidateCS_withoutBiblio.csv

## Add CS WoRMS bibliography

Inputs:
1. CS bibliography: Data/CS_biblio/185354woParasites_CSbiblio.csv
2. detectSpecies_withoutBiblio_failedCriterion0.csv
3. detectCandidateCS_withoutBiblio.csv

Outputs: 
1. detectCandidateCS.csv
2. confusionMatrix.txt

## Determine perturbing factors

Folders test0, test1 and test2

1. Species without CS WoRMS bibliography (withoutBiblio.html)
2. Species with CS WoRMS bibliography, analyse number of sequence within species (nb_sequences_biblio.html)
3. Species with CS WoRMS bibliography, analyse family size (familySize_biblio.html)

## Comparison with a CS WoRMS bibliography according to different corrections:
1. Thresholds for ratios (folder ratioThresholds)
2. Filter data according to sequence number within species and family size (folder speciesThresholds)
3. Select sibling sisters and add thresholds for ratio (folder siblingSisters)
4. Remove discordant species (folder concordantSpecies)
